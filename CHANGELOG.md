# Medistudents Content Change log


## Semantic Versioning

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Each version should group and class changes in the following format:

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for once-stable features removed in upcoming releases.
- `Removed` for deprecated features removed in this release.
- `Fixed` for any bug fixes.
- `Security` to invite users to upgrade in case of vulnerabilities.


---

## Releases

### 1.7.0 - 2018-05-26

- `Changed` Updated *Last Updated* dates.

### 1.6.0 - 2018-02-15

- `Changed` Removing reference to 'BM'.

### 1.5.7 - 2017-11-06

- `Fixed` Incorrect image URLs on *Basic Life Support*.

### 1.5.7 - 2017-11-06

- `Fixed` Incorrect image URLs on *Basic Life Support*.

### 1.2.7 - 2017-08-28

- `Changed` Reverting common images to use `osce-skills/img` folder.

### 1.2.6 - 2017-08-25

- `Added` Placeholder thumbnails and leader images for all modules.
- `Changed` Re-organised imagery to include thumbnails and leader images within module directories, and common media in a generic folder within the skills directory.


### 1.2.5 - 2017-08-24

- `Fixed` Invalid description in previous update.


### 1.2.4 - 2017-08-24

- `Added` Begin adding meta descriptions to content.

### 1.2.3 - 2017-08-22

- `Changed` Updated MD files to use absolute URLs for media.

### 1.2.2 - 2017-08-22

- `Added` Ability to define meta description in files.

### 1.2.1 - 2017-08-21

- `Added` Test PDF download.

### 1.2 - 2017-05-14

- `Added` Title metadata to each page.

### 1.1.2 - 2017-05-12

- `Fixed` Relative URLs to media resources in content.

### 1.1.1 - 2017-05-12

- `Changed` Re-located media outside language directories.

### 1.1.0 - 2017-05-11

- `Changed` Located files under language directories.

### 1.0.3 - 2017-01-25

- `Fixed` Location of  *mental-state-assessment*.

### 1.0.2 - 2017-01-24

- `Added` Metadata example to ABG.

### 1.0.1 - 2017-01-19

- `Changed` Corrected relative media links in content files.

### 1.0 - 2017-01-12

- `Added` Existing copy from OSCE Skills Website v3.
- `Added` All imagery from OSCE Skills Website v3.
