```metadata
{
  "title":"Abdominal (GI) Examination",
  "description":"The abdominal examination aims to pick up on any gastrointestinal pathology that may be causing a patient's symptoms e.g. abdominal pain or altered bowel habit.",
  "last_updated":"2017-09-17"
}
```
# Abdominal (GI) Examination

## Foreword

This is essentially an examination of the patient's abdomen; it is also called the gastrointestinal (GI) examination. It is a complex procedure which also includes examination of other parts of the body including the hands, face and neck.

The abdominal examination aims to pick up on any gastrointestinal pathology that may be causing a patient’s symptoms, for example: abdominal pain or altered bowel habit. This examination is performed on every patient that is admitted to hospital and regularly in clinics and general practice.

Like most major examination stations this follows the usual procedure of:

* Inspection
* [Palpation](https://en.wikipedia.org/wiki/Palpation)
* Auscultation

It is an essential skill to master and is often examined in OSCE's.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.

Explain what you would like to do and obtain consent.  A chaperone should be offered for this examination.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 02

The patient should initially be laid on the bed and exposed from the waist up.

Begin by making a general inspection of the patient from the end of the bed. You should be looking for:

* Whether they are comfortable at rest.
* Do they appear to be [tachypnoeicz](https://en.wikipedia.org/wiki/Tachypnea)?
* Are there any obvious medical appliances around the bed (such as patient controlled [analgesia](http://en.wikipedia.org/wiki/Analgesia))?
* Are there any medications around the bed (although this is unlikely as all medications should be in a locked cupboard)?

Each of these should be reported to the examiner.

![Observe the patient from the end of the bed](../../../../../../media/learning/osce-skills/img/observe-the-patient-from-the-end-of-the-bed.jpg)

### Step 03

Move on to examine the patient’s hands. You are looking for the presence of:

* [Koilonychia](http://en.wikipedia.org/wiki/Koilonychia)
* [Leukonychia](http://en.wikipedia.org/wiki/Leukonychia)
* [Nail clubbing](http://en.wikipedia.org/wiki/Nail_clubbing)
* [Palmar erythema](http://en.wikipedia.org/wiki/Palmar_erythema)
* Tobacco staining
* [Dupuytren’s contracture](http://en.wikipedia.org/wiki/Dupuytren%27s_contracture)

Ask the patient to hold their hands out in front of them looking for any signs of a tremor. Them to extend their wrists up towards the ceiling keeping the fingers extended and look for a [](http://en.wikipedia.org/wiki/Liver_flap).

![Inspect the patient's hands](../../../../../../media/learning/osce-skills/img/inspect-the-patients-hands1.jpg)

![Inspect for clubbing](../../../../../../media/learning/osce-skills/img/inspect-for-clubbing.jpg)

![Look for a liver flap](../../../../../../media/learning/osce-skills/img/look-for-a-liver-flap.jpg)

![Leukonychia](../../../../../../media/learning/osce-skills/img/800px-leukonychia.jpg)

![Nail clubbing](../../../../../../media/learning/osce-skills/img/800px-acopaquia.jpg)

![Dupuytren's contracture](../../../../../../media/learning/osce-skills/img/800px-morbus_dupuytren_fcm.jpg)

### Step 04

Now is a good time to assess the [radial pulse](http://en.wikipedia.org/wiki/Radial_pulse#pulse). There is some argument as to whether this should be performed or not in an abdominal exam; however, it can be a good indication of [sepsis](http://en.wikipedia.org/wiki/Sepsis) or [thyroid disease](http://en.wikipedia.org/wiki/Thyroid_disease).

![Take the radial pulse](../../../../../../media/learning/osce-skills/img/take-the-radial-pulse.jpg)

### Step 05

Move on to examine the face.

Initially check the [conjunctiva](http://en.wikipedia.org/wiki/Conjunctiva) for [pallor](http://en.wikipedia.org/wiki/Pallor) which could be a sign of [anaemia](https://en.wikipedia.org/wiki/Anemia).

Check the [sclera](http://en.wikipedia.org/wiki/Sclera) for [jaundice](http://en.wikipedia.org/wiki/Jaundice).

Move to the mouth asking the patient to open it. Look at the [buccal mucosa](http://en.wikipedia.org/wiki/Buccal_mucosa) for any obvious ulcers which could be a sign of [Crohn’s disease](http://en.wikipedia.org/wiki/Crohn%27s_disease).

Inspect the tongue. If it is red and fat it could be another sign of  [anaemia](https://en.wikipedia.org/wiki/Anemia), as could [angular stomatitis](http://en.wikipedia.org/wiki/Angular_cheilitis).

![Inspect the eyes](../../../../../../media/learning/osce-skills/img/look-in-the-eyes-for-any-signs-of-jaundice.jpg)

![Inspect the mouth and tongue](../../../../../../media/learning/osce-skills/img/look-in-the-mouth-for-any-signs-of-anaemia.jpg)

![Angular cheilitis](../../../../../../media/learning/osce-skills/img/450px-angular_cheilitis.jpg)

### Step 06

Move down to the neck to palate the left [supraclavicular lymph node](http://en.wikipedia.org/wiki/Supraclavicular_lymph_nodes).

A palpable enlarged [supraclavicular (Virchow’s) node](https://en.wikipedia.org/wiki/Virchow%27s_node) is known as [Troisier’s Sign](http://en.wikipedia.org/wiki/Troisier%27s_sign). This is the node which drains the [thoracic duct](http://en.wikipedia.org/wiki/Thoracic_duct). This receives lymph drainage from the entire abdomen as well as the left [thorax](https://en.wikipedia.org/wiki/Thorax). Enlargement of this node may therefore suggest metastatic deposits from a malignancy in any of these areas.

![Palate the left supraclavicular lymph node](../../../../../../media/learning/osce-skills/img/inspect-the-left-supraclavicular-lymph-node.jpg)

### Step 07

Now move on to examine the chest.

You should in particular look for [gynaecomastia](http://en.wikipedia.org/wiki/Gynaecomastia) in men and the presence of 5 or more [spider naevi](http://en.wikipedia.org/wiki/Spider_naevi). These are both stigma of liver pathology.

![Spider nevus](../../../../../../media/learning/osce-skills/img/spider-nevus.jpg)

### Step 08

At this point ask the patient to lie as flat as possible with their arms straight down beside them and begin your inspection of the abdomen. Comment on any obvious abnormalities such as scars, masses and pulsations. Note if there is any [abdominal distension](http://en.wikipedia.org/wiki/Abdominal_distension).

### Step 09

Unlike other examinations, auscultation for bowel sounds may be carried out before percussion and palpation due to adverse effect that these procedures may have on the sound from the bowels.

Listen with the diaphragm next to the umbilicus for up to 30 seconds. High pitched or absent sounds may indicate bowel obstruction. Absence of sounds may be also be caused by [peritonitis](http://en.wikipedia.org/wiki/Peritonitis).

![Auscultation for bowel sound](../../../../../../media/learning/osce-skills/img/auscultation-for-bowel-sound.jpg)

### Step 10

Palpation of the abdomen should be performed in a systematic way using the 9 named segments of the abdomen:

1. Right [hypochondrium](http://en.wikipedia.org/wiki/Hypochondrium).
2. Left [hypochondrium](http://en.wikipedia.org/wiki/Hypochondrium).
3. Right [flank](https://en.wikipedia.org/wiki/Flank_(anatomy)).
4. Left [flank](https://en.wikipedia.org/wiki/Flank_(anatomy)).
5. Right [iliac fossa](http://en.wikipedia.org/wiki/Iliac_fossa).
6. Lleft [iliac fossa](http://en.wikipedia.org/wiki/Iliac_fossa).
7. The [umbilical area](http://en.wikipedia.org/wiki/Umbilical_region).
8. The [epigastrium](https://en.wikipedia.org/wiki/Epigastrium).
9. The [suprapubic region](https://en.wikipedia.org/wiki/Hypogastrium).

Where you start depends on the patient. If a patient has pain in one particular area you should start as far from that area as possible. The tender area should be examined last as they may start [guarding](http://en.wikipedia.org/wiki/Abdominal_guarding) making the examination very difficult.

![Thorax and abdomen](../../../../../../media/learning/osce-skills/img/gray1220.png)

### Step 11

Initial examination should be superficial using one hand. Place the hand flat over each area and flex at the [metacarpophalangeal joints](http://en.wikipedia.org/wiki/Metacarpophalangeal_joints). You should feel whether the abdomen is soft but you should always be looking at the patient’s face for any signs of pain. If you feel any abnormal masses you should report these to the examiner.

![Flex at the metacarpophalangeal joints](../../../../../../media/learning/osce-skills/img/flex-at-the-metacarpophalangeal-joints.jpg)

### Step 12

Once all 9 areas have been examined superficially, you should move on to examine deeper.

A deeper exam is performed with two hands, one on top of the other again flexing at the [metacarpophalangeal MCP joints](https://en.wikipedia.org/wiki/Metacarpophalangeal_joint). You should still be looking at the patient’s face for them flinching due to pain. Again, examine all 9 named segments of the abdomen.

### Step 13

Having performed a general examination of the abdomen, you should now feel for [organomegaly](https://en.wikipedia.org/wiki/Organomegaly), particularly of the liver, spleen and kidneys.

Palpation for the liver and spleen is similar, both starting in the right [iliac fossa](http://en.wikipedia.org/wiki/Iliac_fossa).

For the liver, press upwards towards the right [hypochondrium](http://en.wikipedia.org/wiki/Hypochondrium). You should try to time the palpation with the patient’s breathing-in as this presses down on the liver. If nothing is felt you should move towards the [costal margin](http://en.wikipedia.org/wiki/Costal_margin) and try again.

A distended liver feels like a light tap on the leading finger when you press down. If the liver is distended, its distance from the costal margin should be noted.

![Palpate for the liver](../../../../../../media/learning/osce-skills/img/feel-for-organomegaly.jpg)

![Front of abdomen](../../../../../../media/learning/osce-skills/img/gray1223.png)

### Step 14

Palpating for the spleen is as for the liver but in the direction of the left [hypochondrium](http://en.wikipedia.org/wiki/Hypochondrium). The edge of the spleen which may be felt if distended is more nodular than the liver.

Another way to assess for [splenomegaly](http://en.wikipedia.org/wiki/Splenomegaly) is to ask the patient to lie on their right side.  Support the rib cage with your left hand and again ask the patient to take deep breaths in moving your right hand up towards the left [hypochondrium](https://en.wikipedia.org/wiki/Hypochondrium).

![Palpating for the spleen](../../../../../../media/learning/osce-skills/img/palpating-for-the-spleen.jpg)

### Step 15

To feel for the kidneys you should place one hand under the patient in the flank region and the other hand on top. You should then try to ballot the kidney between the two hands. In the majority of people the kidneys are not palpable, but they maybe in thin patients who have no renal pathology.

![Feel for the left kidney](../../../../../../media/learning/osce-skills/img/feel-for-the-left-kidney.jpg)

![Feel for the right kidney](../../../../../../media/learning/osce-skills/img/feel-for-the-right-kidney.jpg)

### Step 16

Palpate for the bladder by starting at the umbilicus, move in steps downwards towards the pubic bone

![Palpating for the bladder](../../../../../../media/learning/osce-skills/img/palpating-for-the-bladder.jpg)

### Step 17

Next you should [percuss](http://en.wikipedia.org/wiki/Percussion_%28medicine%29). This can be also be used to check for [organomegaly](https://en.wikipedia.org/wiki/Organomegaly) if it is suspected.

Percussion over the abdomen is usually resonant, over a distended liver it will be dull. Percussion can also be used to check for [shifting dullness](https://en.wikipedia.org/wiki/Shifting_dullness) – a sign of [ascites](http://en.wikipedia.org/wiki/Ascites).

With the patient lying flat, start percussing from the midline away from you. If the percussion note changes, hold you finger in that position and ask the patient to roll towards you. Again percuss over this area and if the note has changed then it suggests presence of fluid such as in [ascites](https://en.wikipedia.org/wiki/Ascites).

A distended bladder will also be dull to percussion and this should be checked for.

![Percuss for shifting dullness 1](../../../../../../media/learning/osce-skills/img/percuss-for-shifting-dullness-1.jpg)

![Percuss for shifting dullness 2](../../../../../../media/learning/osce-skills/img/percuss-for-shifting-dullness-2.jpg)

### Step 18

Palpate for the [abdominal aorta](http://en.wikipedia.org/wiki/Abdominal_aorta) to check whether it is expansile, which could be suggestive of an [aneurysm](https://en.wikipedia.org/wiki/Aneurysm).  Note that the aortic pulsation can often be felt in thin patients, but shouldn’t be expansile.

![Palpating for the abdominal aorta](../../../../../../media/learning/osce-skills/img/palpating-for-the-abdominal-aorta.jpg)

### Step 19

At this point, you should mention to the examiner that you would like to finish the procedure with an examination of the hernial orifices, the external genitalia and also a [rectal examination](https://www.medistudents.com/en/learning/osce-skills/gastrointestinal/rectal-examination/).

It is also appropriate to perform a [urinalysis](https://www.medistudents.com/en/learning/osce-skills/genitourinary/urinalysis/) at this point including a pregnancy test in females.

### Step 20

Allow the patient to dress and thank them. Wash your hands and report any findings to your examiner.