```metadata
{
  "title":"Urethral Catheterisation (Female)",
  "description":"Urethral catheterisation is performed for many reasons e.g. occasionally patients with urinary problems need them long term, acute urinary retention or if the patient is acutely unwell to help closely measure their input and output.",
  "last_updated":"2017-09-17"
}
```
# Urethral Catheterisation (Female)

## Foreword

This skill involves you inserting a  [catheter](https://en.wikipedia.org/wiki/Urinary_catheterization) into a female patient's bladder.  It is performed for many reasons e.g. occasionally patients with urinary problems need them long term, acute urinary retention or if the patient is acutely unwell to help closely measure their input and output.

This station always involves a model, but you must remember to act as though you are talking to a patient. Catheterisation can be tested on a male or female model, but here we will discuss female catheterisation (see [Male Urethral Catheterisation](https://www.medistudents.com/en/learning/osce-skills/genitourinary/urethral-catheterisation-male/)). 

There are two main ways to perform catheterisation:

1. The 2-Glove Technique.
2. The Clean Hand / Dirty Hand Technique. 

You should use the one your medical school teaches; in this demonstration we use *the clean hand/dirty hand technique*.

## Procedure Steps

### Step 01

A chaperone is required for this procedure.

Begin by introducing yourself to the patient and clarify her identity. Explain what you would like to do and obtain her consent.

### Step 02

Prepare your equipment for this procedure. This is:

* A catheterisation pack.
* A 12 – 14 Fr female [Foley catheter](https://en.wikipedia.org/wiki/Foley_catheter).
* A catheter bag.
* Antiseptic solution.
* Sterile gloves.
* Lignocaine gel.
* A 10ml sterile water -filled syringe.

### Step 03

Position the patient on her back, lying as flat as possible. Ask her to put her ankles together and let her knees fall apart.

### Step 04

Using an aseptic technique open the catheter pack and pour antiseptic solution into the receiver. Open the rest of your equipment onto the sterile field.

### Step 05

Wash and dry your hands, then put on the sterile gloves.

### Step 06

Drape the patient and place a collecting vessel between the patients legs.

### Step 07

With your left hand, part the labia. Using saline soaked gauze balls, clean the urinary meatus with your right hand. Remember to use single downward movements with each gauze.

### Step 08

With the labia still parted, and ensuring you identify the meatus, insert the syringe of lignocaine gel and inject the whole syringe. Warn your patient that this may be slightly uncomfortable and that it will take about 5 minutes to work.

### Step 09

Using your right hand only, pick up the catheter by its sleeve and start to insert it into the meatus. Continuing to use the sleeve, insert the catheter until the end arm reaches the meatus. At this point, urine should start to flow into the collecting vessel.

### Step 10

Inflate the balloon using 10ml of sterile water, ensuring that it does not cause any pain. NB the volume used to fill the balloon may vary depending upon the size of the catheter used, check the packaging for the exact volume to use.

### Step 11

Attach the catheter bag.

### Step 12

Gently pull on the catheter until resistance is felt. This is when the balloon will be resting on the urethral opening of the bladder.

### Step 13

Dispose of your gloves and equipment in the clinical waste bin. Wash your hands.

### Step 14

Afterwards record the volume of urine collected in the catheter bag and ensure that the patient is comfortable and covered. Remember to complete the sticker on the outside of the catheter pack and put it into the patients notes.