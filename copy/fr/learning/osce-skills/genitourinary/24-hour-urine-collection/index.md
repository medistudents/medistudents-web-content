```metadata
{
  "title":"24 Hour Urine Collection",
  "description":"In order to help diagnose some conditions, or monitor others, patients are occasionally asked to take a 24 hour urine collection",
  "last_updated":"2018-05-26"
}
```
# 24 Hour Urine Collection

## Foreword

In order to help diagnose some conditions, or monitor others, patients are occasionally asked to take a 24 hour urine collection.  Essentially it is collecting all of their urine over a 24 hour period for further analysis in the laboratory.

This station not only tests your communication skills but also your knowledge base as well. The station will likely begin with being asked to explain the procedure of 24 hour collection to a patient. 

An extension to the station may be questions regarding conditions where this procedure is used, and common renal conditions.

## Procedure Steps

### Step 01

Introduce yourself to the patient and clarify their identity. Inform them you would like to explain how to collect a 24 hour urine collection.

### Step 02

Inform the patient that the intention of the process is to collect all of the urine they pass in a 24 hour period. Tell them that they will be given a large container into which all of their urine should be collected.

### Step 03

It is best to begin the collection in the morning.

Explain to the patient that as soon as they wake-up, they should initially use the toilet as they normally would and so not use the container.

From henceforth, whenever they need to pass urine, they should do so into the container. Impress on the patient, the importance that every drop of urine that they pass should go into the container.

### Step 04

The patient should be informed that they must continue this until the next morning. Again they should go to the toilet as soon as they wake up but this time the urine should go into the container.

From then on they can use the toilet as normal.

### Step 05

The patient should return the container to the hospital on the same day, and let them know that you will find out where it should be returned to.

### Step 06

At this point it is a good idea to ask the patient to repeat back to you what you have told them to check that they fully understand what has been said.

Finally, ask the patient if they have any questions.

## Conclusion

At this point, your knowledge about urine collection may be tested.

You should think of questions which may be asked about the conditions which will be tested for. Diabetes and hypertension are two particularly common areas.
