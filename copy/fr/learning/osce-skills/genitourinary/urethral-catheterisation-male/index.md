```metadata
{
  "title":"Urethral Catheterisation (Male)",
  "description":"Urethral catheterisation is performed for many reasons e.g. occasionally patients with urinary problems need them long term, acute urinary retention or if the patient is acutely unwell to help closely measure their input and output.",
  "last_updated":"2017-09-17"
}
```
# Urethral Catheterisation (Male)

## Foreword

This skill involves you inserting a [catheter](https://en.wikipedia.org/wiki/Urinary_catheterization) into a male patient's bladder.  It is performed for many reasons e.g. occasionally patients with urinary problems need them long term, acute urinary retention or if the patient is acutely unwell to help closely measure their input and output.

This station always involves a model, but you must remember to act as though you are talking to a patient. Catheterisation can be tested on a male or female model, but here we will discuss male catheterisation (see [Female Urethral Catheterisation](https://www.medistudents.com/en/learning/osce-skills/genitourinary/female-urethral-catheterisation/)). 

There are two main ways to perform catheterisation:

1. The 2-Glove Technique.
2. The Clean Hand / Dirty Hand Technique. 

You should use the one your medical school teaches; in this demonstration we use *the clean hand/dirty hand technique*.

## Procedure Steps

### Step 01

A chaperone is required for this procedure.

Begin by introducing yourself to the patient and clarify his identity. Explaining what you are going to do and obtain his consent.

### Step 02

Prepare your equipment for this procedure. This is:

* A catheterisation pack.
* A 12 – 14 Fr male [Foley catheter](https://en.wikipedia.org/wiki/Foley_catheter).
* A catheter bag.
* Antiseptic solution.
* Sterile gloves.
* Lignocaine gel.
* A 10ml sterile water -filled syringe.

![Equipment for procedure](../../../../../../media/learning/osce-skills/img/equipment-for-male-urethral-catheterisation-procedure.jpg)

### Step 03

Position the patient on his back with legs slightly apart, and lying as flat as possible.

### Step 04

Using an aseptic technique open the catheter pack and pour antiseptic solution into the receiver. Open the rest of your equipment onto the sterile field.

![Pour antiseptic solution into the receiver](../../../../../../media/learning/osce-skills/img/pour-antiseptic-solution-into-the-receiver.jpg)

### Step 05

Wash and dry your hands, then put on the sterile gloves.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-male.jpg)

### Step 06

Drape the patient and place a collecting vessel between the patients legs.

![Drape the patient and place a collecting vessel](../../../../../../media/learning/osce-skills/img/drape-the-patient-and-place-a-collecting-vessel.jpg)

### Step 07

Hold the penis with a sterile swab and clean the penis thoroughly. Remember to retract the foreskin and clean around the urethral meatus.

![Clean the penis thoroughly](../../../../../../media/learning/osce-skills/img/clean-the-penis-thoroughly.jpg)

### Step 08

Insert the lignocaine gel and hold the meatus closed with pressure from the swab. Indicate that the anaesthetic needs 5 minutes to work.

![Insert the lignocaine gel](../../../../../../media/learning/osce-skills/img/insert-the-lignocaine-gel.jpg)

### Step 09

Hold the penis vertically with one hand and with the other hold the catheter by its sleeve. Advance the catheter tip from its sleeve and insert into the urethra.

![Advance the catheter tip from its sleeve](../../../../../../media/learning/osce-skills/img/advance-the-catheter-tip-from-its-sleeve.jpg)

### Step 10

Progressively insert the catheter, ensuring that neither your hand nor the sleeve touch the penis until the end arm reaches the meatus.  At this point urine should start to flow into the collecting vessel.

![Urine should start to flow into the collecting vessel](../../../../../../media/learning/osce-skills/img/urine-should-start-to-flow-into-the-collecting-vessel.jpg)

### Step 11

Inflate the balloon using 10ml of sterile water, ensuring that it does not cause any pain. NB the volume used to fill the balloon may vary depending upon the size of the catheter used, check the packaging for the exact volume to use.

![Inflate the balloon using 10ml of saline](../../../../../../media/learning/osce-skills/img/inflate-the-balloon-using-10ml-of-saline.jpg)

### Step 12

Attach the catheter bag.

### Step 13

Gently pull on the catheter until resistance is felt. This is when the balloon will be resting on the urethral opening of the bladder. Then reposition the foreskin.

![Gently pull on the catheter until resistance is felt](../../../../../../media/learning/osce-skills/img/gently-pull-on-the-catheter-until-resistance-is-felt.jpg)

### Step 14

Dispose of your gloves and equipment in the clinical waste bin. Wash your hands.

### Step 15

Record the volume of urine collected in the catheter bag and ensure that the patient is comfortable and covered. Remember to complete the sticker on the outside of the catheter pack and put it into the patients notes.