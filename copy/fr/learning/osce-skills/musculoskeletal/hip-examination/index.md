```metadata
{
  "title":"Hip Examination",
  "description":"Hip complaints in adults are often related to pain e.g. arthritis or bursitis, however in children can occur in a well child e.g. `irritable hip` or in more serious conditions",
  "last_updated":"2018-05-26"
}
```
# Hip Examination

## Foreword

Hip complaints in adults are often related to pain, such as arthritis or bursitis. In children however, it can occur in a well child for issues such as *irritable hip*, or in more serious conditions, such as [Perthes disease](http://en.wikipedia.org/wiki/Perthes_disease) or [slipped upper femoral epiphysis](http://en.wikipedia.org/wiki/Slipped_upper_femoral_epiphysis). Hip pain can also be referred pain from another joint, most commonly knee and spine.

The hip examination, along with all other joint examinations, is commonly tested on in OSCEs. You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” and occasionally some special tests.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain what you would like to examine and gain their consent. 

Ensure the joints you wish to examine are appropriately exposed, in this case the patient will probably be wearing shorts, although in reality they should be exposed from the waist down. Therefore you should offer a chaperone.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 02

A hip examination should begin with asking the patient to walk for you which will allow assessment of muscle bulk around the hip joint. Ensure that you check both hips from behind, the side and in front.

Compare both hips for any asymmetry.

Check the gait for: an [antalgic gait](http://en.wikipedia.org/wiki/Antalgic_gait), limp or a [Trendelenburg gait](http://en.wikipedia.org/wiki/Trendelenburg_gait), and waddling due to proximal muscle wasting.

![Ask the patient to walk](../../../../../../media/learning/osce-skills/img/ask-the-patient-to-walk.jpg)

### Step 03

Whilst the patient is still standing, perform the test for [Trendelenburg’s Sign](http://en.m.wikipedia.org/wiki/Trendelenburg's_sign). Ask the patient to alternately stand on one leg. Stand behind the patient and feel the pelvis. It should remain level or rise slightly. If the pelvis drops markedly on the side of the raised leg, then it suggests abductor muscle weakness in the leg the patient is standing on.

![The Trendelenburg test](../../../../../../media/learning/osce-skills/img/the-trendelenburg-test.jpg)

### Step 04

Ask the patient to lie flat on the bed and begin with a general observation of the hip and legs. Check muscle bulk and symmetry as well as any obvious abnormalities such as scars.

Now check both the apparent and true length of the leg. True leg length discrepancy is found by measuring from the [anterior superior iliac spine](http://en.wikipedia.org/wiki/Anterior_superior_iliac_spine) to the [medial malleolus](http://en.wikipedia.org/wiki/Medial_malleolus). Apparent leg length discrepancy is measured from the [xiphisternum](http://en.wikipedia.org/wiki/Xiphisternum) or umbilicus to the [medial malleolus](http://en.wikipedia.org/wiki/Medial_malleolus).

![True leg length measurement](../../../../../../media/learning/osce-skills/img/true-length-leg-measurement.jpg)

![Apparent leg length measurement](../../../../../../media/learning/osce-skills/img/apparent-leg-length-measurement.jpg)

### Step 05

As the hip joint lies deeply there is little to palpate.  But do assess the temperature of the joint compared to surrounding tissue and palpate the greater trochanter as any tenderness here could suggest trochanteric bursitis.

![Palpate the Greater Trochanter](../../../../../../media/learning/osce-skills/img/palpate-the-greater-trochanter.jpg)

### Step 06

Movements to be assessed at the hip are:

* Flexion
* Extension
* Internal rotation.
* External rotation.

Flexion is performed by flexing the knee to 90 degrees and passively flexing the hip by pushing the knee towards the chest.

Extension is performed by placing your hand under the patient’s ankle and asking them to push your hand into the bed.

Internal and external rotation are performed with the knee flexed, inverting the knee for internal rotation and everting it for external rotation.

![Hip flexion](../../../../../../media/learning/osce-skills/img/hip-flexion.jpg)

![Hip extension](../../../../../../media/learning/osce-skills/img/hip-extension.jpg)

![Hip internal rotation](../../../../../../media/learning/osce-skills/img/hip-internal-rotation.jpg)

![Hip external rotation](../../../../../../media/learning/osce-skills/img/hip-external-rotation.jpg)

### Step 07

The final specialised test to be performed is [Thomas’ test](http://en.wikipedia.org/wiki/Thomas_test). Place your hand under the patient’s lumbar spine to stop any lumbar movements and fully flex one of the hips. Observe the other hip; if it lifts off the couch then it suggests a fixed flexion deformity of that hip.

![Thomas' test](../../../../../../media/learning/osce-skills/img/thomas-test.jpg)

![Place your hand under the patients lumbar spine](../../../../../../media/learning/osce-skills/img/place-your-hand-under-the-patients-lumbar-spine.jpg)

### Step 08

On completion, allow the patient to dress, thank them for their time and wash your hands. Report your findings to the examiner.