```metadata
{
  "title":"Knee Examination",
  "description":"Knee complaints are very common presentations to A&E, general practice as well as orthopaedic clinics. Some hospitals even have special `knee` clinics",
  "last_updated":"2018-05-26"
}
```
# Knee Examination

## Foreword

Knee complaints are very common presentations to Accident and Emergency, general practice, and orthopaedic clinics. Some hospitals will also have special *knee* clinics. Common presenting complaints are pain in the knee, the knee locking, or the knee giving way. Common conditions that cause these symptoms include arthritis, ligament, and/or cartilage injuries.

The knee examination, along with all other joint examinations, is commonly tested on in OSCEs. You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” and occasionally some special tests.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain what you would like to examine and gain their consent. 

Ensure both knees are appropriately exposed, in this case the patient will probably be wearing shorts.

### Step 02

Ask the patient to walk for you. Observe any limp or obvious deformities such as scars or muscle wasting. Check if the patient has a varus (bow-legged) or valgus (knock-knees) deformity. Observe from behind to see if there are any obvious popliteal swellings such as a [Baker’s cyst](http://en.wikipedia.org/wiki/Baker%27s_cyst).

![Ask the patient to walk](../../../../../../media/learning/osce-skills/img/ask-the-patient-to-walk.jpg)

### Step 03

Ask the patient to lie on the bed to allow a further general inspection. Look for symmetry, redness, muscle wasting, scars, rashes, or fixed flexion deformities.

![Perform a general inspection](../../../../../../media/learning/osce-skills/img/perform-a-general-inspection.jpg)

![Note the scar over the left knee of this patient](../../../../../../media/learning/osce-skills/img/knee-scar.jpg)

### Step 04

Palpate the knee joint, start by assessing the temperature using the back of your hands and comparing with the surrounding areas

![Assess knee joint temperature](../../../../../../media/learning/osce-skills/img/assess-knee-joint-temperature.jpg)

### Step 05

Palpate the border of the patella for any tenderness, behind the knee for any swellings, along all of the joint lines for tenderness and at the point of insertion of the [patellar tendon](http://en.wikipedia.org/wiki/Patellar_ligament).

Finally, tap the patella to see if there is any effusion deep to the patella.

![Palpate the border of the patella](../../../../../../media/learning/osce-skills/img/palpate-the-border-of-the-patella.jpg)

![Palpate the joint lines](../../../../../../media/learning/osce-skills/img/palpate-the-joint-lines.jpg)

![Palpate the point of insertion](../../../../../../media/learning/osce-skills/img/palpate-the-point-of-insertion.jpg)

![Tap the patella](../../../../../../media/learning/osce-skills/img/tap-the-patella.jpg)

### Step 06

The main movements which should be examined both actively and passively are:

* Flexion
* Extension

A full range of movements should be demonstrated and you should feel for any [crepitus](http://en.wikipedia.org/wiki/Crepitus).

![Knee flexion movement](../../../../../../media/learning/osce-skills/img/knee-flexion-movement.jpg)

![Knee extension movement](../../../../../../media/learning/osce-skills/img/knee-extension-movement.jpg)

### Step 07

Perform the specialist tests which assess the cruciate ligaments.

* **Anterior drawer test:** Flex the knee to 90 degrees and sit on the patient’s foot. Pull forward on the tibia just distal to the knee. There should be *no* movement. If there is movement, it suggests anterior cruciate ligament damage. Another test for ACL damage is Lachman’s test.
* **Posterior drawer test** With the knee in the same position, observe from the side for any posterior lag of the joint, this suggests posterior cruciate ligament damage.

![Draw test](../../../../../../media/learning/osce-skills/img/draw-test.jpg)

![Posterior lag test](../../../../../../media/learning/osce-skills/img/posterior-lag-test.jpg)

### Step 08

Perform the specialist tests which assess the collateral ligaments. Do this by holding the leg with the knee flexed to 15 degrees and place lateral and medial stress on the knee. Any excessive movement suggests collateral ligament damage.

![Lateral stress](../../../../../../media/learning/osce-skills/img/lateral-stress.jpg)

![Medial stress](../../../../../../media/learning/osce-skills/img/assess-medial-stress.jpg)

### Step 09

In the past, McMurrays test is used to assess for meniscal damage. Hold the knee up and fully flexed, with one hand over the knee joint itself and the other on the sole of that foot. Stress the knee joint by medially and laterally moving the foot. Pain or a click is a positive test, confirming meniscal damage.

**Note that this test is no longer recommended in the NICE guidelines due to concerns that it may exacerbate the injury and due to its low diagnostic accuracy.**

![McMurray's test](../../../../../../media/learning/osce-skills/img/mcmurrays-test.jpg)

### Step 10

On completion, allow the patient to dress, thank them for their time and wash your hands. Report your findings to the examiner.
