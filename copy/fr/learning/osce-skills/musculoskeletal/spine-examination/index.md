```metadata
{
  "title":"Spine Examination",
  "description":"Backpain is one of the most common presentations to general practice and A&E departments worldwide and is therefore one you should know well.",
  "last_updated":"2017-09-17"
}
```
# Spine Examination

## Foreword

Back pain is one of the most common presentations to Accident and Emergency and general practice. Common causes of back pain include arthritis, [prolapsed disc](http://en.wikipedia.org/wiki/Prolapsed_disc), and muscular injuries. Occasionally it can be the underlying cause of other conditions such as [sciatica](http://en.wikipedia.org/wiki/Sciatica).

The spine examination, along with all other joint examinations, is commonly tested on in OSCEs.  You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” as well as occasionally special tests.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain what you would like to examine and gain their consent. 

Ensure the joint in which you wish to examine is appropriately exposed, in this case the patient should be exposed from the waist up. You should therefore offer a chaperone.

### Step 02

The patient should be standing. Begin by examining the patient from behind, looking for any obvious abnormalities such as scars. Note the muscle bulk and any wasting. Note the symmetry of each side and look for any scoliosis.

Now look from the side to check for the normal curvatures of the spine. This is [cervical lordosis](http://en.wikipedia.org/wiki/Lordosis), [thoracic kyphosis](http://en.wikipedia.org/wiki/Kyphosis) and [lumbar lordosis](http://en.wikipedia.org/wiki/Lordosis).

![Examine patient from behind](../../../../../../media/learning/osce-skills/img/spine-examine-patient-from-behind.jpg)

![Examine patient from the side](../../../../../../media/learning/osce-skills/img/spine-examine-patient-from-behind-2.jpg)

### Step 03

Now feel the areas around the spine. Feel along the entire length of the spine, palpating each spinous process and checking with the patient for any tenderness. Palpate the sacroiliac joints and finally the paraspinal muscles. As it is difficult to observe the patient’s face for signs of discomfort, you should regularly ask them if what you are doing is painful.

![Feel areas around the spine](../../../../../../media/learning/osce-skills/img/spine-feel-areas-around-the-spine.jpg)

### Step 04

Movements of the spine are *all* performed actively. The first movements which are examined are:

* Lumbar flexion.
* Extension
* Lateral flexion.

Flexion and extension are checked by asking the patient to try and touch their toes (flexion) and then lean backwards (extension). These movements may be assessed quantitatively by placing the index and middle fingers 5 centimeters apart and noting how close and far apart they move on the movements.

Lateral flexion is examined by asking the patient to run their hand down the outside of their leg.

![Lumbar flexion movement](../../../../../../media/learning/osce-skills/img/spine-lumbar-flexion-movement-assessment.jpg)

![Lumbar extension movement](../../../../../../media/learning/osce-skills/img/spine-lumbar-extension-movement-assessment.jpg)

![Lumbar lateral flexion movement](../../../../../../media/learning/osce-skills/img/spine-lumbar-lateral-flexion-movement-assessment.jpg)

### Step 05

Cervical spine movements which are assessed are:

* Lateral flexion.
* Rotation
* Flexion
* Extension

There are some easy commands for checking these. Lateral flexion: place your ear on your shoulder; rotation: look over your shoulder; flexion: put your chin on your chest; and extension: put your head back to look at the ceiling.

![Cervical flexion movement](../../../../../../media/learning/osce-skills/img/spine-cervical-flexion-movement-assessment.jpg)

![Cervical rotation movement](../../../../../../media/learning/osce-skills/img/spine-cervical-rotation-movement-assessment.jpg)

![Cervical flexion forward movement](../../../../../../media/learning/osce-skills/img/spine-cervical-flexion-movement-assessment-2.jpg)

![Cervical extension backward movement](../../../../../../media/learning/osce-skills/img/spine-cervical-extension-movement-assessment.jpg)

### Step 06

The final movement to assess is thoracic rotation. For this the patient should be sat on the edge of the bed to fix the pelvis then ask the patient to turn to each side.

![Thoracic rotation left](../../../../../../media/learning/osce-skills/img/spine-thoracic-rotation-left.jpg)

![Thoracic rotation right](../../../../../../media/learning/osce-skills/img/spine-thoracic-rotation-right.jpg)

### Step 07

Ask the patient to lie flat on the bed. Perform a straight leg raise which assesses for nerve entrapment such as sciatica.

![Straight leg raise](../../../../../../media/learning/osce-skills/img/spine-straight-leg-raise.jpg)

### Step 08

On completion, allow the patient to dress, thank them for their time and wash your hands. Report your findings to the examiner.

## Conclusion

An extension to this station may be to perform an [Upper Limb Neurological Examination](https://www.medistudents.com/en/learning/osce-skills/neurology/upper-limb-neurological-examination/) or [Lower Limb Neurological Examination](https://www.medistudents.com/en/learning/osce-skills/neurology/lower-limb-neurological-examination/) if your findings suggest nerve entrapment from the cervical or lumbar spine.