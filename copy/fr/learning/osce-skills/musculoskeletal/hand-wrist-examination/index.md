```metadata
{
  "title":"Hand and Wrist Examination",
  "description":"Hand and wrist complaints are common presentations to A&E, general practice as well as orthopaedic and rheumatoid clinics. Some hospitals even have special `hand` clinics",
  "last_updated":"2018-05-26"
}
```
# Hand and Wrist Examination

## Foreword

Hand and wrist complaints are common presentations to Accident and Emergency, general practice, orthopaedic and rheumatoid clinics. Some hospitals may have special "hand" clinics.

Common acute problems include fractures, tendonitis and [trigger finger](http://en.wikipedia.org/wiki/Trigger_finger). 

Common chronic problems include [carpal tunnel syndrome](http://en.wikipedia.org/wiki/Carpal_tunnel_syndrome), ganglions and arthritis. 

There are three main conditions commonly examined on in this station – osteoarthritis, rheumatoid arthritis and psoriatic arthritis. This is due to the availability of patients with these conditions as well as the changes specific to each, for example: [swan neck deformity](http://en.wikipedia.org/wiki/Swan_neck_deformity), [Bouchard’s nodes](http://en.wikipedia.org/wiki/Bouchard%27s_nodes) and [Heberden’s nodes](http://en.wikipedia.org/wiki/Heberden%27s_node).  You should therefore be familiar with the changes that each of these conditions can cause.

The hand and wrist examination, along with all other joint examinations, is commonly tested on in OSCEs. You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” and occasionally some special tests.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain what you would like to examine and gain their consent. 

Ensure the hands and wrists are appropriately exposed, in this case the patient will probably be wearing a t-shirt as you will also need to inspect the elbows

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-male.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient2.jpg)

### Step 02

Place the patient’s hands on a pillow in between you and them, ensuring the patient is comfortable.

![Position the patient so they are comfortable](../../../../../../media/learning/osce-skills/img/place-the-patients-hands-on-a-pillow-in-between-you-and-them.jpg)

### Step 03

Inspect the patient’s hands. In particular look for:

* Swellings
* Deformities
* Muscle wasting.
* Scars (particularly carpal tunnel release scars).
* Skin changes.
* Rashes
* Nail pitting.
* [Onycholysis](http://en.wikipedia.org/wiki/Onycholysis)
* Nailfold vasculitis.
* [Palmar erythema](http://en.wikipedia.org/wiki/Palmar_erythema).

If there are joint swellings note which joints are involved and whether the changes are symmetrical or not. Remember to check both sides of the hands

![Inspect the patients hands](../../../../../../media/learning/osce-skills/img/inspect-the-patients-hands-male.jpg)

### Step 04

Now examine the hands, starting proximally and work towards the fingers.

Assess the temperature over the joint areas and compare these with the temperature of the forearm.

![Assess the temperature over the joint areas](../../../../../../media/learning/osce-skills/img/assess-the-temperature-over-the-joint-areas.jpg)

### Step 05

Take the radial pulses and palpate the wrist joints with your thumbs on the extensor surface and your index fingers on the flexor surface. Work your way distally to the carpal bones.

![Carpals palpation](../../../../../../media/learning/osce-skills/img/carpals-inspection.jpg)

### Step 06

Feel the muscle bulk in the thenar and hypothenar eminences.

In the palms, feel for any tendon thickening.

Assess the sensation over the relevant areas supplied by the radial, ulnar and median nerves.

![Feel the muscle bulk in the thenar eminence](../../../../../../media/learning/osce-skills/img/feel-the-muscle-bulk-in-the-thenar.jpg)

### Step 07

Squeeze over the row of metacarpophalangeal joints whilst watching the patient’s face for any discomfort.

Move onto any MCP joints which are noticeably swollen. Palpate these, gently, bimanually with your thumbs on the dorsum and index fingers on the palm. 

Move onto the interphalangeal joints and again palpate any which are swollen. This palpation is done with one of the thumbs on the top and the other on one of the sides. The index fingers will go on the vacant sides of the joint.

![Metacarpophalangeal joint palpation](../../../../../../media/learning/osce-skills/img/metacarpophalangeal-joint-inspection.jpg)

![Interphalangeal joint palpation](../../../../../../media/learning/osce-skills/img/interphalangeal-joint-inspection.jpg)

### Step 08

Check the extensor surface of the elbows for any psoriatic plaques and rheumatoid nodules. Psoriatic plaques could suggest the presence of [psoriatic arthritis](http://en.wikipedia.org/wiki/Psoriatic_arthritis).

![Inspect the elbows](../../../../../../media/learning/osce-skills/img/check-the-underside-of-the-elbows-should-be-checked.jpg)

### Step 09

The movements which should be assessed are:

* Wrist flexion.
* Wrist extension.
* Finger extension.
* Finger flexion.
* Finger abduction.
* Thumb abduction.
* Thumb opposition.

![Wrist flexion](../../../../../../media/learning/osce-skills/img/wrist-flexion.jpg)

![Wrist extension](../../../../../../media/learning/osce-skills/img/wrist-extension.jpg)

![Finger extension](../../../../../../media/learning/osce-skills/img/finger-extension.jpg)

![Finger flexion](../../../../../../media/learning/osce-skills/img/finger-flexion.jpg)

![Finger abduction](../../../../../../media/learning/osce-skills/img/finger-abduction.jpg)

![Thumb abduction](../../../../../../media/learning/osce-skills/img/thumb-abduction.jpg)

![Thumb opposition](../../../../../../media/learning/osce-skills/img/thumb-opposition.jpg)

### Step 10

One special test to employ is [Phalen’s maneuver](http://en.wikipedia.org/wiki/Phalen_maneuver) which is a diagnostic test for [carpal tunnel syndrome](http://en.wikipedia.org/wiki/Carpal_tunnel_syndrome). Forced flexion of the wrist, either against the other hand or by the examiner for 60 seconds will recreate the symptoms of carpal tunnel syndrome.

![Phalens maneuver](../../../../../../media/learning/osce-skills/img/phalens-maneuver.jpg)

[Froment’s sign](http://en.wikipedia.org/wiki/Froment%27s_sign) is a test which is performed to check Ulnar nerve function. This is carried out by asking the patient to hold a piece of paper between their thumb and index finger. This will check the function of the [adductor pollicis](http://en.wikipedia.org/wiki/Adductor_pollicis_muscle). In a patient with Ulnar nerve palsy the interphalangeal joint of the thumb will flex to compensate.

### Step 11

Finally a functional assessment of the patient should be carried out. This involves:

* Firstly, forming a power grip around your middle and index fingers.
* Then a pincer grip against your index finger.
* Llastly, asking your patient to pick up a small object such as a coin.

![Power grip around middle and index fingers](../../../../../../media/learning/osce-skills/img/power-grip-around-middle-and-index-fingers.jpg)

![Pincer grip against index finger](../../../../../../media/learning/osce-skills/img/pincer-grip-against-index-finger.jpg)

![Pick up a small object](../../../../../../media/learning/osce-skills/img/pick-up-a-small-object.jpg)

### Step 12

On completion, thank the patient for their time and wash your hands. Report your findings to the examiner.