```metadata
{
  "title":"Ankle and Foot Examination",
  "description":"Ankle and feet complaints are common presentations to A&E, general practice as well as orthopaedic clinics. The most common presentation is pain e.g. acute fractures, plantar fasciitis and tendonitis",
  "last_updated":"2018-05-26"
}
```
# Ankle and Foot Examination

## Foreword

Ankle and feet complaints are common presentations in Accident and Emergency, general practice, and orthopaedic clinics. The most common presentation is pain, such as acute fractures, [plantar fasciitis](http://en.wikipedia.org/wiki/Plantar_fasciitis) and tendonitis.

The ankle and foot examination, along with all other joint examinations, is commonly tested on in OSCEs. You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” as well as an assessment of function, in this case gait.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient.

Clarify the patient’s identity and explain what you would like to examine and gain their consent.

Ensure that both ankles and feet are appropriately exposed.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

Begin with inspecting the joint whilst the patient is standing

* Watch the patient walk, observing for a normal heel strike, toe-off gait.
* Look at the alignment of the toes for any [valgus](http://en.wikipedia.org/wiki/Valgus_deformity) or [varus](http://en.wikipedia.org/wiki/Varus_deformity) deformities.
* Examine the foot arches, checking for [pes cavus](http://en.wikipedia.org/wiki/Pes_cavus) (high arches) or [pes planus](http://en.wikipedia.org/wiki/Pes_planus) (flat feet).
* Feel the Achilles tendon for any thickening or swelling.
* Inspect the patient’s shoes and note any uneven wear on either sole and the presence of any insoles.

![Observe the patient's gait](../../../../../../media/learning/osce-skills/img/watch-the-patient-walk.jpg)

![Feel the Achilles tendon for any thickening or swelling](../../../../../../media/learning/osce-skills/img/feel-the-achilles-tendon-for-any-thickening-or-swelling.jpg)

### Step 03

Ask the patient to lie on the bed, and perform a further general inspection.

Check the following:

* Symmetry
* Nails
* Skin
* Toe alignment.
* Toe clawing.
* Joint swelling.
* Plantar and dorsal calluses.

![Perform a general inspection](../../../../../../media/learning/osce-skills/img/perform-a-general-inspection.jpg)

### Step 04

Feel the temperature of each foot, comparing it to the temperature of the rest of the leg.

![Check the ankle temperature](../../../../../../media/learning/osce-skills/img/check-the-ankle-temperature.jpg)

### Step 05

Palpate the joint, start by squeezing over the [metatarsophalangeal joints](http://en.wikipedia.org/wiki/Metatarsophalangeal) whilst observing the patient’s face.

Palpate over the midfoot, ankle and [subtalar joint](http://en.wikipedia.org/wiki/Subtalar_joint) lines for any tenderness.

Palpate the foot pulses.

![Squeeze the metatarsophalangeal joints](../../../../../../media/learning/osce-skills/img/squeeze-the-metatarsophalangeal-joints.jpg)

![Palpate the mid-foot](../../../../../../media/learning/osce-skills/img/palpate-the-mid-foot.jpg)

![Palpate the ankle](../../../../../../media/learning/osce-skills/img/palpate-the-ankle.jpg)

### Step 06

Assess all *active* movements of the foot which are:

* Inversion
* Eversion
* Dorsiflexion
* Plantarflexion (of the great toe as well as of the ankle).

Movements should then be tested *passively*.

![Foot inversion movement](../../../../../../media/learning/osce-skills/img/foot-inversion-movement-1.jpg)

![Foot eversion movement](../../../../../../media/learning/osce-skills/img/foot-inversion-movement-2.jpg)

![Dorsiflexion movement](../../../../../../media/learning/osce-skills/img/eversion-movement-back.jpg)

![Plantarflexion movement](../../../../../../media/learning/osce-skills/img/eversion-movement-forward.jpg)

![Toe dorsiflexion passively](../../../../../../media/learning/osce-skills/img/toe-dorsiflexion.jpg)

![Toe plantarflexion passively](../../../../../../media/learning/osce-skills/img/toe-plantarflexion.jpg)

### Step 07

Finally examine the [midtarsal joints](http://en.wikipedia.org/wiki/Midtarsal_joint) by fixing the ankle with one foot and inverting and everting the forefoot with the other.

![Passive inversion](../../../../../../media/learning/osce-skills/img/examine-the-midtarsal-joints-passive-inversion.jpg)

![Passive eversion](../../../../../../media/learning/osce-skills/img/examine-the-midtarsal-joints-passive-eversion.jpg)

### Step 08

Allow the patient to dress and thank them. Wash your hands and report your findings to the examiner.