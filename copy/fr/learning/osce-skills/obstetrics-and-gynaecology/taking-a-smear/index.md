```metadata
{
  "title":"Taking a Smear",
  "description":"Cervical cytology, or smear tests, form part of the UK screening programme. It is an intimate investigation to obtain cells from the cervix (neck of the womb)",
  "last_updated":"2017-09-17"
}
```
# Taking a Smear

## Foreword

Cervical cytology, or smear tests, form part of the UK screening programme. It is an intimate investigation to obtain cells from the [cervix](https://en.wikipedia.org/wiki/Cervix) (neck of the womb). It can be performed at any time, however it is best if the women is not menstruating.

It is known that [Human Papillomavirus (HPV)](https://en.wikipedia.org/wiki/Human_papillomavirus_infection) increases the risk of abnormal smear results, including [cervical carcinoma](https://en.wikipedia.org/wiki/Cervical_cancer) and as such UK guidance has recently changed to include HPV analysis. In 2008 the HPV vaccination was also introduced in the UK. For more info on this see [Paediatric Immunisation Schedule](https://www.medistudents.com/en/learning/osce-skills/paediatric/paediatric-immunisation-schedule/).

For the purpose of examinations you will be provided with a mannequin to perform this skill, however you should pretend it is a real patient and talk to it as this will form part of the marking scheme.

## Procedure Steps

### Step 01

Introduce yourself to the patient and clarify her identity. Explain what you would like to do and obtain consent. Explain she should feel little, if any, discomfort and that the examination should be over fairly quickly. 

**A chaperone is required for this examination.**

### Step 02

Ensure you have all of the necessary equipment ready. This includes:

* Gloves.
* [Speculum](https://en.wikipedia.org/wiki/Speculum_(medical)).
* Lubricant.
* A cervical brush.
* A pot of cytology preservative solution.
* A light source.

![Smear test equipment](../../../../../../media/learning/osce-skills/img/1smearkit.jpg)

### Step 03

The patient should be exposed from the waist down. Ask her to lie on the bed with her ankles together and allow her knees to spread apart as much as possible.

### Step 04

Wash you hands, put on your gloves and examine the outer vagina checking for any obvious abnormalities.

![Examine the outer vagina](../../../../../../media/learning/osce-skills/img/2smearinspect.jpg)

### Step 05

Warm the blades of the speculum with warm water. The water also acts to lubricate the [speculum](https://en.wikipedia.org/wiki/Speculum_(medical)) but you may also like to apply some lubricant. Ensure the lubricant is not placed at the end of the speculum as this may alter the result.

### Step 06

Inform the patient that you are about to start the procedure. Use your left hand to part the [labia minora](https://en.wikipedia.org/wiki/Labia_minora) and insert the speculum with the screw facing sideways.

![Insert the speculum](../../../../../../media/learning/osce-skills/img/4smearinsert.jpg)

### Step 07

As you advance the [speculum](https://en.wikipedia.org/wiki/Speculum_(medical)), turn it so that the screw faces upwards. Open the blades and fix them open with the screw. Ensure that you can see well by adjusting the light source. Check for any gross pathology and identify the transition zone.

![Advance the speculum](../../../../../../media/learning/osce-skills/img/5smearcervix.jpg)

### Step 08

Place the tip of the cervical brush into the external cervical os and rotate it three times through 360 degrees ensuring that it is always in contact with the cervix.

![Place the tip of the cervical brush into the external cervical os](../../../../../../media/learning/osce-skills/img/6smearbrush.jpg)

### Step 09

Remove the brush ensuring it does not wipe against anything. Tap the brush 10 times on the edge of the pot of cytology medium.

![Place the end of the brush into the sample pot](../../../../../../media/learning/osce-skills/img/7smearend.jpg)

### Step 10

Release the screw on the speculum and carefully remove it from the vagina, completing the examination.

![Carefully remove the speculum from the vagina](../../../../../../media/learning/osce-skills/img/8smearremove.jpg)

### Step 11

Offer the patient some tissue, cover the patient, and thank her. You should explain to the patient that her smear results will be sent to them in approximately six weeks, thus ensuring appropriate follow-up.

## Conclusion

An extension to this station would be the interpretation of cytology results. Guidance has recently changed and includes HPV analysis. You should be familiar with these.







