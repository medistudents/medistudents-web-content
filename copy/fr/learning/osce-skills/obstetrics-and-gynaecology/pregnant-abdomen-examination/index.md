```metadata
{
  "title":"Pregnant Abdomen Examination",
  "description":"Examination of the pregnant abdomen is performed routinely throughout pregnancy. Expectant mothers attend ante-natal check-ups regularly throughout their pregnancy where this is performed by both doctors and midwives",
  "last_updated":"2017-09-17"
}
```
# Pregnant Abdomen Examination

## Foreword

Examination of the pregnant abdomen is performed routinely throughout pregnancy.  Expectant mothers attend ante-natal check-ups regularly throughout their pregnancy where this is performed by both doctors and midwives. You will get the chance to practice this skill during your obstetrics and gynaecology placement in medical school, however as you will likely encounter pregnant women in whatever area you specialise in, it is an essential skill to be able to perform. As such it is commonly examined in OSCEs.

This skill demonstrates two areas:

1. Your communication skills with the mother.
2. Your examination technique. 

There will usually be real patient volunteers for this station so remember to be gentle as your patient may have had her bump examined many times before your turn. Like most stations this still follows the general rule of:

* Inspection
* [Palpation](https://en.wikipedia.org/wiki/Palpation)
* Auscultation

## Procedure Steps

### Step 01

Introduce yourself and clarify the patient’s identity. Wash your hands.  Explain what you would like to do and gain her consent.

For this station the patient should be lying on the bed, as flat as possible but in reality whatever is most comfortable for her.  She should ideally be exposed from the pubic bone to below her breasts.

### Step 02

Try and put mum at ease. A few simple but friendly questions to help her gain your trust includes:

> “How are you feeling?”

> “Do you know what you are having?”

> “Is this your first pregnancy?”

This shows the examiner that you can be caring, rather than jumping in hands first.  As you become more skilled at this station you can incorporate these types of questions into your examination technique along the way.

### Step 03

Perform a general inspection of mum and her bump. Comment whether she looks comfortable, does she have any abdominal striations or [Linea Nigra](http://en.wikipedia.org/wiki/Linea_nigra), and whether she has previous operative scars, such as previous [caesarean section](https://en.wikipedia.org/wiki/Caesarean_section).

If greater than 24 weeks you can expect some foetal movements, comment if so. This shows you really are observing her closely.

### Step 04

Measure [fundal height](https://en.wikipedia.org/wiki/Fundal_height). Do this with a tape measure (disposable if available). Measure from the [pubic symphysis](https://en.wikipedia.org/wiki/Pubic_symphysis) to the top of her bump (fundus).

The length in centimetres roughly corresponds to how far along she is in weeks;  for example 36cm roughly equals 36 weeks.

### Step 05

Check the lie of the baby by examining her bump.  Remember to be gentle and warm your hands if they are cold.  Here you are assessing which way the baby is lying – this can be longitudinal, transverse or oblique.

Use both hands, one on each side of her bump and gently press. Remember to face mum while you are doing this.

### Step 06

Check the presentation of the baby to determine which end of the baby is “presenting”. This should be cephalic but may be breech.

Place both hands at the base of her [uterus](https://en.wikipedia.org/wiki/Uterus), just above the [pubic bone](https://en.wikipedia.org/wiki/Pubis_(bone)) and apply quite firm pressure. Again face mum and warn her that this bit may be slightly uncomfortable. Hopefully the baby will be head down and may even be “engaged” in the pelvis. If you are very confident in this skill you may wish to offer the examiner how much the baby is engaged.

### Step 07

Auscultate the baby’s heart. This is best heard over the baby’s shoulder.  If you have correctly identified the lie you should roughly know where this is. Put either your [Doppler ultrasound](https://en.wikipedia.org/wiki/Medical_ultrasound#doppler) or [Pinard stethoscope](https://en.wikipedia.org/wiki/Pinard_horn) over this area and listen. The baby’s heart rate should be between 120-140bpm (ensure you are not incorrectly hearing the transmission of mum’s which will be slower).

### Step 08

Cover mum up and thank her. Then wash your hands.

### Step 09

Inform your examiner that for completeness you would like to check her [blood pressure](https://www.medistudents.com/en/learning/osce-skills/cardiovascular/blood-pressure-measurement/), and perform [urinalysis](https://www.medistudents.com/en/learning/osce-skills/genitourinary/urinalysis/).

If you have any concerns regarding the baby’s heart rate you should suggest that a [cardiotocography (CTG)](https://en.wikipedia.org/wiki/Cardiotocography) should also be performed. You will not usually be asked to perform these extra skills as part of this station.

## Conclusion

An extension to this station may be discussions regarding [glucosuria](https://en.wikipedia.org/wiki/Glycosuria) or [proteinuria](https://en.wikipedia.org/wiki/Proteinuria), and/or elevated blood pressure. You should therefore be familiar with conditions such as [gestational diabetes](https://en.wikipedia.org/wiki/Gestational_diabetes) and [pre-eclampsia](https://en.wikipedia.org/wiki/Pre-eclampsia).







