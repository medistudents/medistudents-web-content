```metadata
{
  "title":"Newborn Baby Examination",
  "description":"This examination is performed on all newborn babies, ideally within 48 hours of birth. It is also rechecked by the baby's general practitioner at the 8 week check. It is basically a top-to-toe examination of a baby and therefore has many parts to it",
  "last_updated":"2018-05-26"
}
```
# Newborn Baby Examination

## Foreword

This examination is performed on all newborn babies, ideally within 48 hours of birth.  It is also rechecked by the baby's general practitioner at the 8 week check.  It is basically a top-to-toe examination of a baby and therefore has many parts to it.

It is not often examined in OSCEs as you would not be able to use real babies as patients, however some medical schools do have dummy babies to practice these skills on.  Every medical student will have a paediatric placement and it is here that you can practice these skills. In addition, helping the doctors with their baby checks on the post-natal wards will also gain you brownie points!  All of the following steps have to be performed, but it’s up to you which order you do them in.

## Procedure Steps

### Step 01

Equipment required for this station:

* Neonatal stethoscope.
* Opthalmoscope.
* Oxygen saturation monitor/pulse oximeter.

### Step 02

Introduce yourself to mum and clarify her, and baby’s identity.  Explain that you would like to perform a full examination of her new baby(s) and gain her consent. Congratulate her on the birth as this will put her at ease and help gain your trust. New mums are protective of their babies so trust and rapport is essential.

### Step 03

Whilst washing your hands you could ask mum to strip her baby down to its nappy.  Ensure you have a changing mat to do the examination on.

### Step 04

Start by asking mum a few questions:

> How was the birth?

Good to know as forcep deliveries can cause facial bruising, c-sections can occasionally cut the baby’s skin. Baby’s born by c-section are usually more “mucusy” too.

> “Did your baby need any help after birth with breathing?”

For example, did the midwives or paediatric doctors have to give oxygen/rescue breaths.

> “How are you feeding your baby? Breast or bottle?”

If breast feeding ask her:

> “How is it going? Is the baby latching ok?”

If bottle feeding ask:

> “Which milk are you giving your baby? Is baby taking bottles ok?”

Don’t criticise if mum has not opted to breast feed, this is an individual decision.

> “Are there any conditions that run in you or dad’s family, such as congenital heart problems?”

> “Has anyone in your family (especially females) had problems with their hips at birth?”

Female babies are more likely to have clicky or dislocated hips due to the hormones that are in mums body during pregnancy, these are the hormones which help to open up mum’s pelvis prior to and during birth.

> “Has your baby passed its sticky black stool yet?”

Parents often don’t know the term '[meconium](http://en.wikipedia.org/wiki/Meconium)'.

### Step 05

Begin by observing the baby. Does it look and behave “normally”, for example: jaundice, activity, and posture.

Is there any obvious bruising or marks from birth.

Are there any other marks such as [strawberry naevus](http://en.wikipedia.org/wiki/Strawberry_naevus), stork marks or [Mongolian blue spot](http://en.wikipedia.org/wiki/Mongolian_blue_spot).

Remember to turn the baby over and inspect its back too.

### Step 06

With the baby lying on its back feel the [fontanelle](http://en.wikipedia.org/wiki/Fontanelle) gently with your hand. It should be nice and soft, a tense/bulging or sunken fontanelle can suggest the baby is unwell.

### Step 07

Using both your hands gently feel the baby’s bones checking they are symmetrical on both sides. Face, around ears, clavicles (these can be injured during birth if [shoulder dystocia](http://en.wikipedia.org/wiki/Shoulder_dystocia) occurs), both arms (for example [Erb’s palsy](http://en.wikipedia.org/wiki/Erbs_Palsy)) down to legs and feet.

Open up the baby’s hand and look at the palm for normal [palmar creases](http://en.wikipedia.org/wiki/Single_transverse_palmar_crease), count the fingers on each hand.

Look at the feet, is there any signs of a [sandal gap](http://elementsofmorphology.nih.gov/index.cgi?tid=30f338017d5cec83) or [talipes](http://en.wikipedia.org/wiki/Talipes) and count the toes on each foot.

### Step 08

If the baby has its eyes open at this point check for the [red reflex](http://en.wikipedia.org/wiki/Red_reflex) using your opthalmoscope. An absent reflex could suggest [congenital cataracts](http://en.wikipedia.org/wiki/Congenital_cataract).

### Step 09

Auscultate the baby’s heart using a neonatal/paediatric stethoscope. The normal rate is *120-150* so you will have to listen much more carefully for any murmurs as there is less time between heartbeats to hear them. If you do pick up any murmurs assess whether it radiates anywhere.

### Step 10

Ausculate the lung fields. The normal respiratory rate is 30-60 in newborns. Are there any extra sounds such as grunting or stridor.

### Step 11

Palpate the abdomen and check the umbilical stump/clamp to ensure no signs of infection.

### Step 12

Turn the baby over and check down its spine and between buttock cheeks for the sacral dimple.

### Step 13

At this point undo the baby’s nappy. Look for any obvious genital abnormalities. If it’s a male infant you should check the scrotum to see if the testicles have descended. If not you may be able to palpate them in the spermatic cord and gently bring them down yourself. Check the patency of the anus at this point too.

### Step 14

Test the baby’s hips. This is done by two techniques, [Ortolani](http://en.wikipedia.org/wiki/Ortolani_maneuver) and [Barlow](http://en.wikipedia.org/wiki/Barlow_maneuver) tests. Essentially cup the baby’s hips in the palm of your hand and gently abduct the hips, this should be smooth with no clicks.

Move your hands to the front of the baby and with their knees flexed push gently downwards into the bed, again this should be smooth with no clunks.

### Step 15

At this point redo the nappy and again wash your hands. With your hands freshly washed you now want to assess inside the baby’s mouth. Use your little finger to feel the palate of the mouth.  Look to see if there is a [tongue tie](http://en.wikipedia.org/wiki/Ankyloglossia).

### Step 16

Again wash your hands. Attach the pulse/oxygen monitor to the baby’s foot. Remember if a baby is sleeping or crying the heartrate may be higher or lower than the normal range.

### Step 17

There are a number of [primitive reflexes](http://en.wikipedia.org/wiki/Primitive_reflexes) present in newborns which you should elicit. [Moro](http://en.wikipedia.org/wiki/Moro_reflex), [grasp](http://en.wikipedia.org/wiki/Palmar_grasp) and sucking.

### Step 18

Thank mum, offer to dress the baby, although she will usually wish to do this herself. Answer any questions she may have.

### Step 19

Again wash your hands and report your findings, if any, to the examiner, or doctor if on a ward. Should you notice any abnormalities you may wish to suggest how to investigate these further.