```metadata
{
  "title":"Paediatric Immunisation Schedule",
  "description":"Immunisations (vaccinations/injections/jabs) are given to all children within the UK to prevent them catching, developing or passing on several serious diseases e.g. meningitis, measles and whooping cough, to name a few",
  "last_updated":"2018-05-26"
}
```
# Paediatric Immunisation Schedule

## Foreword

Immunisations (vaccinations/injections/jabs) are given to all children within the UK to prevent them catching, developing or passing on several serious diseases, such as meningitis, measles and whooping cough.

Prior to the introduction of the immunisation schedule, many lives were lost during childhood to preventable diseases. It has been one of the most life saving medical breakthroughs to date.  Despite this many parents choose not to vaccinate their children.

This station aims to take you through the schedule used within the **United Kingdom (UK)** including the conditions it helps protect against. Immunisation schedules will differ for other countries so you must check your local policies.

Visit the following website for the most up-to-date [UK immunisation schedule](http://www.patient.co.uk/health/immunisation-usual-uk-schedule).

Stations about childhood immunisation schedules test two aspects:

* Your ability to communicate with parents.
* Your knowledge of the schedule and any questions the parents may have about the immunisations.

## Procedure Steps

### Step 01

Begin by introducing yourself to the parent(s) and/or the child if they are present. Clarify the name of the patient as well as their age. Ensure that they are all comfortable and that the room is set up suitably for the consultation.

### Step 02

You should inform them that you have been asked to speak to them about immunisations and explain that you will take them through the schedule step-by-step. Ideally you should answer any questions they may have after you have been through the list of immunisations.

### Step 03

The current timetable for immunisations **in the UK** is shown in the table below:

#### 2 months

* **Diphtheria, Tetanus, Pertussis, Polio and Haemophilus Influenzae B (HIB)** – as a single injection (1st dose).
* **Pneumococcal vaccine**  – as a separate injection (1st dose).
* **Rotavirus vaccine** – as oral drops (1st dose). This was introduced in July 2013 for babies born on after 01 May 2013.
* **Meningococcal type B (MenB) vaccine** – as a separate injection. *Note this was introduced in September 2015 for all babies born on or after 01 July 2015.*

#### 3 months

* **Diphtheria, Tetanus, Pertussis, Polio and Haemophilus Influenzae B (HIB)** – as a single injection (2nd dose).
* **Meningitis C** – as a separate injection.
* **Rotavirus vaccine**  – as oral drops (2nd dose).

#### 4 months

* **Diphtheria, Tetanus, Pertussis, Polio and Haemophilus Influenzae B (HIB)** – as a single injection (3rd dose).
* **Pneumococcal vaccine** – as a separate injection (2nd dose).
* **Meningococcal type B (MenB) vaccine**  – as a separate injection (2nd dose).

#### Between 12 to 13 months (within 1 month of child’s 1st birthday)

* **Haemophilus Influenzae B (HIB) and Meningitis C** – as a single booster injection (4th dose of HIB, 2nd dose of Men C).
* **Measles, Mumps and Rubella (MMR)** – as a single injection (1st dose)
* **Pneumococcal booster** – as a separate injection (3rd dose).
* **Meningococcal type B (MenB) vaccine** – as a separate injection (3rd dose.

#### 2, 3 and 4 year olds

* **Seasonal Influenza** – as nasal spray, or injection due to contra-indications or reasons of faith.

#### Between 2 years to 11 years

* **Seasonal Influenza** – annually.

#### Between 3 years 4 months, to 5 years

* **Diphtheria, Tetanus, Pertussis and Polio** – as a single injection (pre-school booster).
* **Measles, Mumps and Rubella (MMR)** – as a single injection (2nd dose).

#### Between 12 to 13 years (females only)

* **Human Papillomavirus (types 16 & 18)** – as two injections, the second 6-24 months after the first. *Note that this was reduced to a 2 dose schedule from 3 dose schedule in September 2014*.

#### Between 13 to 18 years

* **Diphtheria, Tetanus and Polio** – as a single injection (booster dose). *This is the 5th dose which is sufficient to provide long term protection.*
* **Meningitis C** – as a separate injection (booster dose).
* **Meningococcal types ACWY** – as a separate injection.

### Step 04

There are non-routine vaccinations which may be given at birth. If the baby is felt to be more at risk to exposure to *tuberculosis* than the general population they may be given the BCG vaccination. If the child’s mother is *Hepatitis B positive* then the Hepatitis B vaccination is given.

### Step 05

It is **vital** that you know this schedule. You are likely to be asked questions about the various immunisations, therefore, you should be certain to know about them all. This will include whether they are live or not, any likely side effects, and any contra-indications.

### Step 06

At the end of the consultation ensure that the parent understands everything you have told them and that they have no remaining questions.

## Conclusion

Please note that the above immunisation schedule is **only relevant within the United Kingdom (UK)**.  Immunisation schedules will differ for other countries so you must check your local policies.

Visit the following website for the most up-to-date [UK immunisation schedule](http://www.patient.co.uk/health/immunisation-usual-uk-schedule).