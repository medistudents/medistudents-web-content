```metadata
{
  "title":"Cerebellar Examination",
  "description":"The cerebellar examination is performed in patients with neurological signs or symptoms of cerebellar pathology e.g. dizziness, loss of balance or poor co-ordination. As such the station involves examining the gait, balance and co-ordination",
  "last_updated":"2018-05-26"
}
```
# Cerebellar Examination

## Foreword

The cerebellar examination is performed in patients with neurological signs or symptoms of cerebellar pathology, for example: dizziness, loss of balance, or poor co-ordination.  There are many causes of cerebellar dysfunction and include vascular, for example: stroke, space-occupying lesions, [multiple sclerosis](https://en.wikipedia.org/wiki/Multiple_sclerosis), and genetic conditions such as [Friedreich’s Ataxia](http://en.wikipedia.org/wiki/Friedrich%27s_ataxia).

The cerebellar examination needs to reflect these symptoms and as such involves examining the gait, balance and co-ordination.

## Procedure Steps

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain what you would like to do and obtain consent

## Gait

### Step 01

Ask the patient to stand up. Observe the patient’s posture and whether they are steady on their feet.

Ask the patient to walk, for example: to the other side of the room, and back. If the patient normally makes use of a walking aid, allow them to do so.

![Observe the patient walking](../../../../../../media/learning/osce-skills/img/observe-the-patient-walking.jpg)

### Step 02

Observe the different gait components (heel strike, toe lift off). Is the gait shuffling/waddling/scissoring/ swinging?

Observe the patients arm swing and take note how the patient turns around as this involves good balance and co-ordination.

Ask the patient to walk heel-to-toe to assess balance.

![Walk heel-to-toe to assess balance](../../../../../../media/learning/osce-skills/img/walk-heel-to-toe-to-assess-balance.jpg)

### Step 03

Perform [Romberg’s test](http://en.wikipedia.org/wiki/Romberg%27s_test) by asking the patient to stand unaided with their eyes closed. If the patient sways or loses balance then this test is positive. Stand near the patient in case they fall.

Whilst Rombergs test does not directly test for cerebellar ataxia, it helps to differentiate cerebellar ataxia from sensory ataxia. In cerebellar ataxia the patient is likely to be unsteady on their feet even with the eyes open.

![Romberg's test by asking the patient to stand unaided with their eyes closed](../../../../../../media/learning/osce-skills/img/rombergs-test-by-asking-the-patient-to-stand-unaided-with-their-eyes-closed.jpg)

### Step 04

Check for a resting tremor in the hands by placing a piece of paper on the patient’s outstretched hands.

![Check for a resting tremor](../../../../../../media/learning/osce-skills/img/check-for-a-resting-tremor.jpg)

### Step 05

Test tone in the arms (shoulder, elbow, wrist).

![Test tone in the shoulder](../../../../../../media/learning/osce-skills/img/test-tone-in-the-arms-1.jpg)

![Test tone in the elbow and wrist](../../../../../../media/learning/osce-skills/img/test-tone-in-the-arms-2.jpg)

## Co-ordination

### Step 01

Test for [dysdiadochokinesis](http://en.wikipedia.org/wiki/Dysdiadochokinesia) by showing the patient how to clap by alternating the palmar and dorsal surfaces of the hand. Ask them to do this as fast as possible and repeat the test with the other hand.

![Test for dysdiadochokinesis 1](../../../../../../media/learning/osce-skills/img/test-for-dysdiadochokinesis-1.jpg)

![Test for dysdiadochokinesis 2](../../../../../../media/learning/osce-skills/img/test-for-dysdiadochokinesis-2.jpg)

### Step 02

Perform the *finger-to-nose test* by placing your index finger about two feet from the patients face. Ask them to touch the tip of their nose with their index finger then the tip of your finger. Ask them to do this as fast as possible while you slowly move your finger. Repeat the test with the other hand.

![Finger-to-nose test 1](../../../../../../media/learning/osce-skills/img/finger-to-nose-test-1.jpg)

![Finger-to-nose test 2](../../../../../../media/learning/osce-skills/img/finger-to-nose-test-2.jpg)

### Step 03

Perform the *heel-to-shin test*. Have the patient lying down for this and get them to run the heel of one foot down the shin of the other leg, and then to bring the heel back up to the knee and start again. Repeat the test with the other leg.

![Heel-to-shin test 1](../../../../../../media/learning/osce-skills/img/heel-to-shin-test-1.jpg)

![Heel-to-shin test 2](../../../../../../media/learning/osce-skills/img/heel-to-shin-test-2.jpg)

## Conclusion

Finish by washing your hands and thanking the patient. Summarise your findings to the examiner and offer a differential diagnosis.

Common conditions include [Parkinson’s disease](http://en.wikipedia.org/wiki/Parkinson%E2%80%99s_disease) and [cerebellar ataxia](http://en.wikipedia.org/wiki/Cerebellar_ataxia).