```metadata
{
  "title":"Cardiovascular (CVS) Examination",
  "description":"The Cardiovascular (CVS) examination aims to pick up on any cardiovascular pathology that may be causing a patient's symptoms e.g. chest pain, heart failure",
  "last_updated":"2017-09-17"
}
```
# Cardiovascular (CVS) Examination

## Foreword

The cardiovascular (CVS) examination is essentially an examination of the patient's heart; however it is a complex examination which also includes examination of other parts of the body including the hands, face and neck. The CVS examination aims to pick up on any cardiovascular pathology that may be causing a patient's symptoms, such as chest pain, breathlessness, or heart failure.  This examination is performed on every patient that is admitted to hospital and regularly in clinics and general practice.

Like most major examination stations this follows the usual procedure of:

* Inspect
* Palpate
* Auscultate

It is an essential skill to master and is often examined on.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain what you would like to do and obtain consent.  A chaperone should be offered for this examination

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 02

The patient should be on the bed with their trunk at 45 degrees, and they should be exposed from the waist up.

### Step 03

Begin by observing the patient from the end of the bed.

You should note whether the patient looks comfortable.

* Are they [cyanosed](http://en.wikipedia.org/wiki/Cyanosis) or flushed?
* Is their respiration rate normal?
* Are there any clues around the bed such as PCA machines, GTN sprays or an oxygen mask?

Comments should be provided to the examiner on each of these areas.

![Observe the patient from the end of the bed](../../../../../../media/learning/osce-skills/img/observe-the-patient-from-the-end-of-the-bed.jpg)

### Step 04

Inspect the patient’s hands. Initially note how warm they feel as this gives an indication of how well perfused they are.

Particular signs which you should be looking for are:

* [Nail clubbing](http://en.wikipedia.org/wiki/Nail_clubbing)
* [Splinter haemorrhages](http://en.wikipedia.org/wiki/Splinter_hemorrhage)
* [Palmar erythema](http://en.wikipedia.org/wiki/Palmar_erythema)
* [Janeway lesions](http://en.wikipedia.org/wiki/Janeway_lesions)
* [Osler’s nodes](http://en.wikipedia.org/wiki/Osler%27s_nodes)
* Tobaco staining

![Inspect the patient's hands](../../../../../../media/learning/osce-skills/img/inspect-the-patients-hands1.jpg)

### Step 05

Take the [radial pulse](http://en.wikipedia.org/wiki/Radial_pulse#pulse). It is not a suitable pulse for describing the character of the pulsation, but can be used to assess the rate and rhythm. At this point you should also check for a collapsing pulse – a sign of [aortic incompetence](http://en.wikipedia.org/wiki/Aortic_incompetence).

Remembering to check that the patient doesn’t have any problems with their shoulder, locate the radial pulse and place your palm over it, then raise the arm above the patient’s head. A collapsing pulse will present as a knocking on your palm.

![Take the radial pulse](../../../../../../media/learning/osce-skills/img/take-the-radial-pulse.jpg)

### Step 06

Examine the extensor aspect of the elbow for any evidence of [xanthomata](http://en.wikipedia.org/wiki/Xanthomata).

### Step 07

At this point you should say to the examiner that you would like to take the blood pressure. They will usually tell you not to and give you the value.



### Step 08

Move up to the face. Look in the eyes for any signs of:

* [Jaundice](http://en.wikipedia.org/wiki/Jaundice) (particularly in the sclera beneath the upper eyelid).
* [Anaemia](http://en.wikipedia.org/wiki/Anaemia) (in the conjunctiva beneath the lower eyelid).
* [Corneal arcus](http://en.wikipedia.org/wiki/Corneal_arcus).

You should also look around the eye for any [xanthelasma](http://en.wikipedia.org/wiki/Xanthelasma).

![Inspect the eyes](../../../../../../media/learning/osce-skills/img/look-in-the-eyes-for-any-signs-of-jaundice.jpg)

### Step 09

Whilst looking at the face, check for any malar facies:

* Look in the mouth for any signs of anaemia such as [glossitis](http://en.wikipedia.org/wiki/Glossitis).
* Check the colour of the tongue for any [cyanosis](http://en.wikipedia.org/wiki/Cyanosis).
* Check around the mouth for any [angular stomatitis](http://en.wikipedia.org/wiki/Angular_stomatitis) – another sign of anaemia.

![Inspect the mouth and tongue](../../../../../../media/learning/osce-skills/img/look-in-the-mouth-for-any-signs-of-anaemia.jpg)

### Step 10

Move to the patient’s neck to assess their [jugular venous pressure (JVP)](http://en.wikipedia.org/wiki/Jugular_venous_pressure).

Ask them to turn their head to look away from you. Look across the neck between the two heads of [sternocleidomastoid](http://en.wikipedia.org/wiki/Sternocleidomastoid) for a pulsation. If you do see a pulsation you need to determine whether it is the JVP. If it is then the pulsation is non-palpable, obliterable by compressing distal to it, and will be exaggerated by performing the [hepatojugular reflex](http://en.wikipedia.org/wiki/Abdominojugular_test).

Having warned the patient that it may cause some discomfort, press down on the liver. This will cause the JVP to rise further. If you decide the pulsation is due to the JVP, note its vertical height above the [sternal angle](http://en.wikipedia.org/wiki/Sternal_angle).

![Assess the patients jugular venous pressure](../../../../../../media/learning/osce-skills/img/assess-the-patients-jugular-venous-pressure.jpg)

### Step 11

Move the examination to the chest, or precordium (sometimes spelt "praecordium"). Begin by inspecting the area, particularly looking for any obvious pulsations, abnormalities or scars, remembering to check the [axillae](http://en.wikipedia.org/wiki/Axillae) as well.

### Step 12

Palpation of the precordium starts by trying to locate the apex beat. Start by doing this with your entire hand and gradually become more specific until it is felt under one finger and describe its location anatomically.

The normal location is in the 5th intercostal space in the [mid-clavicular line](http://en.wikipedia.org/wiki/Mid-clavicular_line). It is quite common however, to not feel the apex beat at all.

![Locate the apex beat](../../../../../../media/learning/osce-skills/img/locate-the-apex-beat.jpg)

### Step 13

Palpate for any heaves or thrills. A thrill is a palpable murmur whereas a heave is a sign of [left ventricular hypertrophy](http://en.wikipedia.org/wiki/Left_ventricular_hypertrophy). A thrill feels like a vibration and a heave feels like an abnormally large beating of the heart. Feel for these all over the precordium.

![Palpate for any heaves or thrill](../../../../../../media/learning/osce-skills/img/palpate-for-any-heaves-or-thrill.jpg)

### Step 14

Auscultation is now performed for all four valves of the heart in the following areas:

* [Mitral valve](http://en.wikipedia.org/wiki/Mitral_valve) – where the apex beat was felt.
* [Tricuspid valve](http://en.wikipedia.org/wiki/Tricuspid_valve) – on the left edge of the sternum in the 4th intercostal space.
* [Pulmonary valve](http://en.wikipedia.org/wiki/Pulmonary_valve) – on the left edge of the sternum in the 2nd intercostal space.
* [Aortic valve](http://en.wikipedia.org/wiki/Aortic_valve) – on the right edge of the sternum in the 2nd intercostal space.

You should listen initially with the diaphragm noting how many heart sounds you can hear:

* Are there any extra to the two normal sounds?
* Are there any murmurs?
* Are the heart sounds normal in character?
* Can you hear any rub?

If you hear any abnormal sounds you should describe them by when they occur and the type of sound they are producing. Feeling the radial pulse at the same time can give a good indication as to when the sound occurs as the pulse occurs at systole.

If you suspect a murmur, check if it radiates. Mitral murmurs typically radiate to the left axilla whereas aortic murmurs often radiate to the left carotid artery. You should therefore listen over here for any carotid bruits

You should also listen with the bell of your stethoscope for any low pitched murmurs.

![Mitral valve location](../../../../../../media/learning/osce-skills/img/mitral-valve-location.jpg)

![Tricuspid valve location](../../../../../../media/learning/osce-skills/img/tricuspid-valve-location.jpg)

![Pulmonary valve location](../../../../../../media/learning/osce-skills/img/pulmonary-valve-location.jpg)

![Aortic valve location](../../../../../../media/learning/osce-skills/img/aortic-valve-location.jpg)

### Step 15

To further check for mitral stenosis lay the patient on their left side, ask them to breathe in, then out, and hold it out and listen over the apex and axilla with the bell of the stethoscope.

![Further check for Mitral Stenosis](../../../../../../media/learning/osce-skills/img/further-check-for-mitral-stenosis.jpg)

### Step 16

[Aortic incompetence](http://en.wikipedia.org/wiki/Aortic_incompetence) is assessed in a similar way by asking the patient to sit forward, repeat the breathe in, then out and hold exercise and listen over the Erbs point (the third intercostal space on the left).

### Step 17

Finally assess for any [oedema](http://en.wikipedia.org/wiki/Oedema). Whilst the patient is sat forward, listen at the lung bases for pulmonary oedema, feel the sacrum for oedema. Also, assess the ankles for the same.

![Feel the sacrum for oedema](../../../../../../media/learning/osce-skills/img/feel-the-sacrum-for-oedema.jpg)

![Assess the ankles for oedema](../../../../../../media/learning/osce-skills/img/assess-the-ankles-for-oedema.jpg)

### Step 18

Thank the patient and allow them to dress. Wash your hands and report your findings to the examiner.  If you do find any abnormalities you should indicate that you would like to arrange an [ECG](https://www.medistudents.com/en/learning/osce-skills/cardiovascular/electrocardiography-ecg/) and an echocardiogram.