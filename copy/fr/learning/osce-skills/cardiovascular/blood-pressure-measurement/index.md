```metadata
{
  "title":"Mesure de la Tension Artérielle",
  "module":"Médecine Cardiovasculaire",
  "description":"La surveillance de la tension artérielle est l'un des principaux signes vitaux. Il évalue la pression requise par le cœur pour pomper",
  "last_updated":"2018-07-09"
}
```
# Mesure de la Tension Artérielle

## Préface

La surveillance de la tension artérielle est l'un des principaux signes vitaux. Il évalue la pression requise par le cœur pour pomper. Ceci est mesuré régulièrement sur tous les patients, que ce soit à l'hôpital, dans les cliniques externes ou par le médecin généraliste du patient.

L'hypotension (tension artérielle basse) peut causer des étourdissements et des chutes, surtout chez les personnes âgées. L'hypertension (TA élevée) est un facteur de risque de maladie cardiovasculaire, mais peut aussi être une cause de maux de tête et d'autres conditions. Dans les situations d'urgence, il peut également évaluer le niveau de malaise d'un patient, par exemple : l'hypotension peut indiquer une septicémie ou une diminution du volume, l'hypertension peut indiquer une douleur ou une pathologie intracrânienne.

Il s'agit d'une compétence de base mais nécessaire à l'apprentissage et qui est fréquemment examinée.

## Étapes de la procédure

### Étape 01

Assurez-vous d'avoir l'équipement nécessaire:

* Un [tensiomètre](https://fr.wikipedia.org/wiki/Tensiom%C3%A8tre).
* Un [stéthoscope](http://fr.wikipedia.org/wiki/Stethoscope).
* Gel nettoyant pour les mains.

![Appareil de mesure de la pression sanguine](../../../../../../media/learning/osce-skills/img/equipment-for-measuring-blood-pressure.jpg)

### Étape 02

Il est important, lorsque vous mesurez la tension artérielle, d'établir un rapport avec votre patient afin de prévenir [le syndrome de la blouse blanche](https://fr.wikipedia.org/wiki/Effet_%C2%AB_blouse_blanche_%C2%BB) qui peut vous donner un résultat inexactement élevé.

Par conséquent, assurez-vous de vous présenter au patient, d'expliquer la procédure en répondant à ses questions et de lui demander son consentement. Vous devriez également leur expliquer qu'ils peuvent ressentir un certain inconfort lorsque vous gonflez le brassard, mais que cela sera de courte durée. Assurez-vous qu'ils sont assis confortablement, le bras reposé.

![Se présenter au patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient2.jpg)

### Étape 03

Comme pour toutes les procédures cliniques, il est essentiel que vous vous laviez d'abord les mains avec un nettoyant alcoolisé et que vous laissiez sécher.

![Nettoyez vos mains à l'aide d'un nettoyant à base d'alcool](../../../../../../media/learning/osce-skills/img/sanitise-your-hands-using-alcohol-cleanser.jpg)

### Étape 04

Assurez-vous d'avoir choisi la bonne taille de brassard pour votre patient. Une taille de brassard différente peut être nécessaire pour les patients obèses ou les enfants.

![Sélectionnez la bonne taille de brassard pour convenir à votre patient](../../../../../../media/learning/osce-skills/img/select-the-correct-cuff-size-to-suit-your-patient.jpg)

### Étape 05

Enroulez le brassard autour du bras supérieur du patient en s'assurant que la flèche est alignée avec [l'artère brachiale](https://fr.wikipedia.org/wiki/Art%C3%A8re_brachiale). Ceci devrait être déterminé en sentant le * pouls brachial*.

![Veillez à ce que le brassard soit correctement placé](../../../../../../media/learning/osce-skills/img/ensure-correct-placement-of-the-cuff.jpg)

![L'artère brachiale](../../../../../../media/learning/osce-skills/img/gray525.png)

### Étape 06

Déterminez une valeur approximative de la tension artérielle systolique. Ceci peut être fait en palpant le pouls brachial ou radial et en gonflant le brassard jusqu'à ce que le pouls ne puisse plus être senti. La lecture à ce point doit être notée et le brassard dégonflé.

![Gonflez le brassard pour déterminer une valeur approximative de la tension artérielle systolique](../../../../../../media/learning/osce-skills/img/inflate-the-cuff-to-determine-a-rough-value-for-the-systolic-blood-pressure.jpg)


### Étape 07

Maintenant que vous avez une valeur approximative, la vraie valeur peut être mesurée. Placez le diaphragme de votre stéthoscope sur l'artère brachiale et regonflez le brassard à 20-30 mmHg de plus que la valeur estimée prise auparavant.

Dégonflez ensuite le brassard à 2-3 mmHg par seconde jusqu'à ce que vous entendiez le premier [bruits de Korotkoff](https://fr.wikipedia.org/wiki/Bruits_de_Korotkoff) – ceci est la tension artérielle systolique.

Continuez à dégonfler le brassard jusqu'à ce que les sons disparaissent, le 5e son Korotokoff - c'est la pression artérielle diastolique.

![Enregistrez la tension artérielle réelle](../../../../../../media/learning/osce-skills/img/record-the-true-blood-pressure.jpg)

### Étape 08

Si la tension artérielle est supérieure à 140/90, attendez 1 minute et vérifiez de nouveau. Notez qu'une lecture normale diffère pour les patients diabétiques.

### Étape 09

Expliquez à votre examinateur que vous voudriez vérifier la pression artérielle en position debout pour vérifier s'il y a une chute significative (>20 mmHg après 2 minutes). Cela suggère une [hypotension posturale](https://fr.wikipedia.org/wiki/Hypotension_orthostatique).

### Étape 10

Enfin, vous devez informer le patient de sa lecture et le remercier. Si, après revérification, la tension artérielle reste élevée, informez le patient qu'il aura besoin de cette répétition à l'avenir, ce qui assure un suivi approprié.
