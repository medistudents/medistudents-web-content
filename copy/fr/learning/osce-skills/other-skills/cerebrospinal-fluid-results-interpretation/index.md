```metadata
{
  "title":"Cerebrospinal Fluid Results (CSF) Interpretation",
  "description":"Cerebrospinal fluid (CSF) is collected by performing a lumbar puncture on your patient. This is performed if the doctor suspects certain conditions such as meningitis or subarachnoid haemorrhage",
  "last_updated":"2018-05-26"
}
```
# Cerebrospinal Fluid Results (CSF) Interpretation

## Foreword

[Cerebrospinal fluid (CSF)](http://en.wikipedia.org/wiki/Cerebrospinal_fluid) is collected by performing a [lumbar puncture](http://en.wikipedia.org/wiki/Lumbar_puncture) on your patient.  This is performed if the doctor suspects certain conditions such as meningitis or [subarachnoid haemorrhage](http://en.wikipedia.org/wiki/Subarachnoid_hemorrhage).

You will not be expected to know this skill for examinations, however you may be asked to interpret some CSF results.

This station tests your knowledge of both the normal values of various components of CSF, and also your ability to make a differential diagnosis of a condition purely from a set of CSF results.

## Procedure Steps

The most important knowledge to have for this station are the normal values. The most important of these are given in the below (note that these are for adults only).

### Step 01

Systematically you will now take the examiner through the results which have been given. Start at the top and comment whether each result is normal or not. If any result is abnormal, it should be commented whether the result is slightly or very deranged.

#### Normal Range

* **Appearance:** Clear & colourless.
* **White cells:** 0 – 5 x 10<sup>6</sup> per litre (all [lymphocytes](https://en.wikipedia.org/wiki/Lymphocyte) with no [neutrophils](https://en.wikipedia.org/wiki/Neutrophil)).
* **Red cells:** 0 – 10 x 10<sup>6</sup> per litre.
* **Protein:** 0.2 – 0.4 grams per litre (or less than 1% of the serum protein concentration).
* **Glucose** 3.3 – 4.4 mmol per litre (or ≥ 60% of a simultaneously derived plasma glucose concentration).
* **pH:** 7.31.
* **Pressure:** 70 – 180 mmH<sub>2</sub>O.


### Step 02

Now it will be most likely that you are asked to make a differential diagnosis based upon the results given. A number of conditions give deranged CSF readings and therefore knowledge of which conditions affect the different values is vital. The changes in various conditions are given below.

#### Bacterial Meningitis

* **Appearance:** Cloudy and turbid.
* **White cells:** Raised [neutrophils](https://en.wikipedia.org/wiki/Neutrophil).
* **Red cells:** Normal.
* **Protein:** High or very high.
* **Glucose:** Very low.

#### Viral Meningitis

* **Appearance:** Normal.
* **White cells:** Raised [lymphocytes](https://en.wikipedia.org/wiki/Lymphocyte).
* **Red Cells:** Normal.
* **Protein:** Normal or high.
* **Glucose:** Normal or low.

#### Tuberculous Meningitis

* **Appearance:** Normal or slightly cloudy.
* **White cells:** Raised [lymphocytes](https://en.wikipedia.org/wiki/Lymphocyte).
* **Red cells:** Normal.
* **Protein:** High or very high.
* **Glucose:** Very low.

#### Subarachnoid Haemorrhage

* **Appearance:** Usually blood stained.
* **White Cells:** Normal.
* **Red Cells:** Very high.
* **Protein:** Normal or high.
* **Glucose:** Normal or low.

#### Guillan-Barré Syndrome

* **Appearance:** Normal.
* **White cells:** Normal.
* **Red cells:** Normal.
* **Protein:** High (only after one week).
* **Glucose:** Normal or low.

#### Multiple Sclerosis

* **Appearance:** Normal.
* **White cells:** Raised [lymphocytes](https://en.wikipedia.org/wiki/Lymphocyte).
* **Red cells:** Normal.
* **Protein:** High.
* **Glucose:** Normal.

## Conclusion

Although initially daunting, obvious patterns will become recognisable. It is also more likely that you will be asked about the more common conditions, such as bacterial and viral [meningitis](https://en.wikipedia.org/wiki/Meningitis).

If the condition asked about is either kind of meningitis, it is most likely that a causative organism will be asked for. Therefore make sure to learn the common causes of [meningitis](https://en.wikipedia.org/wiki/Meningitis) for varying age groups as well as the treatment.







