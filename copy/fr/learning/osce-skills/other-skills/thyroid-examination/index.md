```metadata
{
  "title":"Thyroid Examination",
  "description":"The thyroid gland sits at the front of the neck and produces endocrine hormones into the bloodstream. Sometimes the gland can produce too much (hyperthyroid) or too little hormone (hypothyroid) which results in a patient needing treatment",
  "last_updated":"2018-05-26"
}
```
# Thyroid Examination

## Foreword

The [thyroid gland](https://en.wikipedia.org/wiki/Thyroid) sits at the front of the neck and produces [endocrine hormones](https://en.wikipedia.org/wiki/Endocrine_system) into the bloodstream.  Sometimes the gland can produce too much (hyperthyroid) or too little hormone (hypothyroid) which results in a patient needing treatment.  There are certain signs and symptoms that the patient may present with and it is these that you are examining the patient for in this station.

This station may start in a number of ways. You may be given a history of a [hyperthyroid](https://en.wikipedia.org/wiki/Hyperthyroidism) or [hypothyroid](https://en.wikipedia.org/wiki/Hypothyroidism) patient, or you may be asked to examine the patient’s neck or thyroid gland. Either way you should approach the situation systematically and not jump straight into feeling the neck.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain the procedure and gain the patient’s consent.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-male.jpg)

### Step 02

Perform an initial observation of the patient.

* Do they seem abnormally hyper- or hypoactive?
* Do they appear sweaty?
* What condition is their skin and hair?
* Does their voice sound normal?

Report any abnormalities to the examiner.

### Step 03

A dysfunctioning [thyroid gland](https://en.wikipedia.org/wiki/Thyroid) may give stigmata of disease in many places, so as always, it is best to start with the hands. You should first feel the hands for any sweating. Look for any tremor – placing a piece of paper on the backs of the patient’s outstretched hands may show this. Check the nails for any thyroid acropachy – similar to clubbing, or [onycholysis](http://en.wikipedia.org/wiki/Onycholysis) – where the nail comes away from the nail bed. You should also observe for any [palmar erythema](http://en.wikipedia.org/wiki/Palmar_erythema) which may occur in hyperthyroidism.

![Inspect for tremor in the hands](../../../../../../media/learning/osce-skills/img/inspect-for-tremor-in-the-hands.jpg)

### Step 04

Next you should feel the pulse. It may be [tachycardic](https://en.wikipedia.org/wiki/Tachycardia) or [bradycardic](https://en.wikipedia.org/wiki/Bradycardia) if a thyroid condition is present.

![Feel the pulse](../../../../../../media/learning/osce-skills/img/feel-the-pulse.jpg)

### Step 05

Now move onto the neck. Observe it as a whole but pay particular attention to the area of the thyroid gland. You should look from the front and the side looking for any obvious abnormalities, scars or swellings.

![Observe the neck](../../../../../../media/learning/osce-skills/img/observe-the-neck.jpg)

### Step 06

Hand the patient a glass of water and observe them as they take a drink. Watch the movement of any swellings as they drink as this can help to differentiate between different causes.

![Observe the neck as the patient drinks](../../../../../../media/learning/osce-skills/img/observe-the-neck-as-the-patient-drinks.jpg)

### Step 07

Next you should feel the gland. The approach is from behind so always tell the patient what you will be doing and that you will be behind them. Warn them again the moment before you actually touch their neck.

![Palpate the thyroid gland](../../../../../../media/learning/osce-skills/img/feel-the-thyroid-gland.jpg)

### Step 08

Palpate the entire length of both lobes of the gland as well as the isthmus. Note any swellings or abnormal lumps. You should note the shape and consistency of any lumps as well as whether they are tender or mobile. You should also examine while the patient drinks to assess whether the lump moves with swallowing.

### Step 09

Whilst still behind the patient, take the opportunity to examine the [cervical lymph nodes](http://en.wikipedia.org/wiki/Cervical_lymph_nodes). You should also examine the eyes from behind and above to look for any [exophthalmos](http://en.wikipedia.org/wiki/Exophthalmos) – another sign of hyperthyroidism.

![Examine the cervical lymph nodes](../../../../../../media/learning/osce-skills/img/examine-the-cervical-lymph-nodes.jpg)

### Step 10

Finally you should auscultate the thyroid. A bruit, a sign of increased blood flow, may be heard in [hyperthyroidism](https://en.wikipedia.org/wiki/Hyperthyroidism).

![Auscultate the thyroid](../../../../../../media/learning/osce-skills/img/auscultate-the-thyroid.jpg)

### Step 11

Thank your patient and report your findings to the examiner.

## Conclusion

An extension to this station may be *thyroid function results interpretation*. Below are the three most common diagnosis:

#### Primary Hypothyroidism

* **TSH:**  ↑
* **Free T3 and free T4:** ↓

#### Secondary Hypothyroidism

* **TSH:**  ↓ or ↔
* **Free T3 and free T4:** ↓

#### Hyperthyroidism

* **TSH:**  ↓
* **Free T3 and free T4:** ↑