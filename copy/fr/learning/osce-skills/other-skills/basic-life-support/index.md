```metadata
{
  "title":"Basic Life Support",
  "description":"Basic life support is one of the most important skills you will learn at medical school. As such, you are likely to be examined on this station regularly so make sure you know it",
  "last_updated":"2017-11-06"
}
```
# Basic Life Support

## Foreword

When a patient has a cardiac arrest (heart stops beating), Basic Life Support (BLS) can be provided to help their chance of survival.  Essentially you are providing chest compressions to pump blood from the heart and around the body, ensuring the tissues and brain maintain an oxygen supply.

Basic life support is one of the most important skills you will learn at medical school. As such, you are likely to be examined on this station regularly so **make sure you know it**. Prior to 2010 BLS used to include the assessment of circulation by palpation of the carotid pulse. This is no longer part of the guidelines as it was felt it was more often than not incorrectly assessed.

Please see [resus.org.uk](http://www.resus.org.uk) for the full up-to-date UK guidelines on basic life support procedure.


## Procedure Steps

You are likely to be presented with a resus mannequin and told that you have found the patient collapsed, either on a corridor in the hospital or somewhere outside. Either way, the algorithm to follow is fairly similar.

### Step 1 - Danger

Initially you should assess if there is any <em>danger</em> in the situation either for you or for the patient.

### Step 2 - Response

You should check if there is any <em>response</em> from the patient. Do this by gently shaking the patient's shoulders and loudly shouting into both ears, asking them "Can you hear me?"

If there is no response you should shout for help. If there is anyone present then you should also ask them to get an Automated External Defibrillator (AED) if there is one available.

![Check for a response](../../../../../../media/learning/osce-skills/img/bls-check-for-response.jpg)

### Step 03 - Airway

Check if the patient's *airway* is patent. Perform the head tilt (unless there is any chance of cervical spine injury) and jaw thrust to open the airway. Ensure that there is no physical blockage by their tongue, vomit, or anything else.

![Check the patients airway](../../../../../../media/learning/osce-skills/img/bls-check-patients-airway.jpg)

### Step 04 - Breathing

Check the patients breathing, do this by maintaining the head tilt and jaw thrust, placing your face and ear over the mouth to feel for any respiratory effort whilst observing the chest for any movement.  You should assess for normal breathing for up to 10secs.

![Check for breathing and circulation](../../../../../../media/learning/osce-skills/img/bls-check-for-breathing-and-circulation.jpg)

### Step 05

If breathing is normal, place the patient in the [recovery position](http://en.wikipedia.org/wiki/Recovery_position) and find help.

![Recovery position](../../../../../../media/learning/osce-skills/img/bls-recovery-position.jpg)

### Step 06

If breathing is absent you should call for the emergency services.

This number is 999 in the UK, or typically 911 or 112 internationally (check and know your local number). From within the hospital, call the crash call number (commonly 2222, but again check for local differences).

### Step 07

Once you have called for support, you should begin chest compressions.

Place one hand over the sternum roughly in the middle, interlock your fingers and lock your elbows positioning yourself vertically above your hands. Depress the sternum 5-6 centimeters and release the pressure. Repeat this 30 times at a rate of 100 to 120 per minute.

![Begin chest compressions](../../../../../../media/learning/osce-skills/img/bls-start-chest-compressions.jpg)


### Step 08

Start to give a combination of 2 rescue breaths and 30 chest compressions. For the rescue breaths ensure the head tilt and jaw thrust are in place, pinch the soft part of the nose so that it is closed, open the mouth, seal your lips around theirs and blow steadily for 1 second. Watch the chest to check that it rises and falls with the breath. Once the breaths are given, return to giving 30 chest compressions.

![Pinch nose and tilt head](../../../../../../media/learning/osce-skills/img/bls-rescue-breaths-1.jpg)

![Breathe into patient](../../../../../../media/learning/osce-skills/img/bls-rescue-breaths-2.jpg)

### Step 09

If there is anyone who can assist, you should share out the work. One of you should perform the breaths and the other the compressions, swapping when tired.

### Step 10

You should continue this cycle of 2 rescue breaths and 30 chest compressions until either further help arrives, the patient regains consciousness, or you can no longer physically continue.
