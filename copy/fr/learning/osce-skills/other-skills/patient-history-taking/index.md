```metadata
{
  "title":"History Taking",
  "description":"Taking history from a patient is a skill necessary for examinations and afterwards as a practicing doctor, no matter which area you specialise in",
  "last_updated":"2018-05-26"
}
```
# History Taking

## Foreword

Taking a history from a patient is a skill necessary for examinations and afterwards as a practicing doctor, no matter which area you specialise in. It tests both your communication skills as well as your knowledge about what to ask. Specific questions vary depending on what type of history you are taking but if you follow the general framework below you should gain good marks in these stations. This is also a good way to present your history.

In practice you may sometimes need to gather a collateral history from a relative, friend or carer. This may be with a child or an adult with impaired mental state.

## Procedure Steps

### Step 01

Introduce yourself, identify your patient and gain consent to speak with them.  Should you wish to take notes as you proceed, ask the patients permission to do so.

### Step 02 - Presenting Complaint (PC)

This is what the patient tells you is wrong, for example: chest pain.

### Step 03 - History of Presenting Complaint (HPC)

Gain as much information you can about the specific complaint.

Sticking with chest pain as an example you should ask:

* **Site:** Where exactly is the pain?
* **Onset:** When did it start, was it constant/intermittent, gradual/ sudden?
* **Character:** What is the pain like e.g. sharp, burning, tight?
* **Radiation:** Does it radiate/move anywhere?
* **Associations:** Is there anything else associated with the pain, e.g. sweating, vomiting.
* **Time course:** Does it follow any time pattern, how long did it last?
* **Exacerbating / relieving factors:** Does anything make it better or worse?
* **Severity:** How severe is the pain, consider using the 1-10 scale?

The **SOCRATES** acronym can be used for any type of pain history.

### Step 04 - Past Medical History (PMH)

Gather information about a patients other medical problems (if any).

### Step 05 - Drug History (DH)

Find out what medications the patient is taking, including dosage and how often they are taking them, for example: once-a-day, twice-a-day, etc.

At this point it is a good idea to find out if the patient has any allergies.

### Step 06 - Family History (FH)

Gather some information about the patients family history, e.g diabetes or cardiac history.  Find out if there are any genetic conditions within the family, for example: polycystic kidney disease.

### Step 07 - Social History (SH)

This is the opportunity to find out a bit more about the patient’s background.  Remember to ask about smoking and alcohol.  Depending on the PC it may also be pertinent to find out whether the patient drives, e.g. following an MI patient cannot drive for one month. You should also ask the patient if they use any illegal substances, for example: cannabis, cocaine, etc.

Also find out who lives with the patient. You may find that they are the carer for an elderly parent or a child and your duty would be to ensure that they are not neglected should your patient be admitted/remain in hospital.

### Step 08 - Review of Systems (ROS)

Gather a short amount of information regarding the other systems in the body that are not covered in your HPC.

The above example involves the CVS so you would focus on the others.

These are the main systems you should cover:

* CVS
* Respiratory
* GI
* Neurology
* Genitourinary/renal
* Musculoskeletal
* Psychiatry

Please note these are the main areas, however some courses will also teach the addition of other systems such as ENT/ophthalmology.

### Step 09 - Summary of History

Complete your history by reviewing what the patient has told you. Repeat back the important points so that the patient can correct you if there are any misunderstandings or errors.

You should also address what the patient thinks is wrong with them and what they are expecting/hoping for from the consultation. A useful acronym for this is **ICE** [I]deas, [C]oncerns and [E]xpectations.

### Step 10 - Patient Questions / Feedback

During or after taking their history, the patient may have questions that they want to ask you. It is very important that you don’t give them any false information. As such, unless you are absolutely sure of the answer it is best to say that you will ask your seniors about this or that you will go away and get them more information (e.g. leaflets) about what they are asking. These questions aren’t necessarily there to test your knowledge, just that you won’t try and 'blag it'.

### Step 11

When you are happy that you have all of the information you require, and the patient has asked any questions that they may have, you must thank them for their time and say that one of the doctors looking after them will be coming to see them soon.

