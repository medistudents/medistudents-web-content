```metadata
{
  "title":"Top 10 Tips for Clinical Examinations",
  "description":"Every medical school teaches clinical skills in a slightly different manner. The following list will provide a basis for which to form your own techniques around.",
  "last_updated":"2017-10-03"
}
```
# Top 10 Tips for Clinical Examinations

## Foreword

Every medical school teaches clinical skills in a slightly different manner. The following list will provide a basis from which to form your own techniques around.


## The List

### Tip 1

Wash your hands at the beginning and the end of every examination. There are usually two marks available for doing this which can make a big difference if your procedure has not gone well.

### Tip 2

Introduce yourself to the patient and clarify their identity.

### Tip 3

Be polite to your patient and always thank them for their time. These people are often real patients providing their own free time for your education. Also remember to thank your examiner.

### Tip 4

Be gentle during an examination. Note that the patient may have been examined 10 or 20 times prior to you. It will not look good for your examiner if the patient appears in pain.

### Tip 5

Look smart. Although there are no points for this, it does give the examiner a sense that you are taking the exam seriously.

### Tip 6

Tie up any long hair.

### Tip 7

Wear nothing below the elbow, including watches. Note that if an examination requires a timer, one will be provided.

### Tip 8

If you are unsure of what is being asked of you then ask your examiner for clarification. You will not lose marks for this, but you will do if you go on to perform the incorrect technique.

### Tip 9

If you remember something that you should have performed earlier then do go back and do this. You will not lose marks for performing the order incorrectly, but you will lose marks if you do not perform the procedure at all.

### Tip 10

If the examiner has left out equipment for you then use, then be sure to use them, for example gloves and an apron. These are not laid out to trick you.
