```metadata
{
  "title":"Breast Examination",
  "description":"Breast examinations are performed for a number of clinical reasons. Patients, usually female, may present with mastalgia (breast pain), nipple changes (skin or discharge) or more commonly a breast lump",
  "last_updated":"2018-05-26"
}
```
# Breast Examination

## Foreword

Breast examinations are performed for a number of clinical reasons. Patients, usually female, may present with [mastalgia (breast pain)](https://en.wikipedia.org/wiki/Breast_pain), nipple changes (skin or discharge) or more commonly a [breast lump](https://en.wikipedia.org/wiki/Breast_lump).

[Breast cancer](https://en.wikipedia.org/wiki/Breast_cancer) is a common condition, nearly 50,000 women are diagnosed with this each year in the UK, it is therefore likely that whatever area you specialise in you will encounter breast cancer. Due to this fact it is commonly examined upon, as it is an important skill to know.

As this is an intimate examination it is pertinent to gain a good rapport with your patient, maintain good communication and ensure the patient’s dignity at all times. For the purpose of examinations you will be provided with a mannequin, however you should pretend it is a real patient and talk to it as such, which will also form part of the marking scheme.

## Procedure Steps

### Step 01

Wash your hands.

Introduce yourself to the patient and clarify their identity. Explain to them what the examination will entail and gain the patients consent. In reality this station is likely to involve a female patient but could equally be a male. For the purpose of examinations there will usually be a mannequin.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

For this examination the patient should be exposed from the waist up and initially sat on the edge of the bed.

### Step 03

Begin by asking the patient to rest her hands on her thighs, with her arms relaxed.

Make a general inspection of both breasts. You should be looking for any asymmetry, scars, obvious lumps or swellings and any nipple abnormalities (e.g. inversion or discharge).

You should also comment on any skin changes (peau d’orange, eczema).

### Step 04

Ask the patient to place her hands above her head and repeat the inspection.

### Step 05

Again perform a general inspection, this time with the patient placing her hands on her hips and pushing inwards to tense the pectoralis muscles.

### Step 06

Ask the patient to lie down with her arms by her side, and again repeat the general inspection.

### Step 07

Examine each breast individually. If you have been told about any abnormality then start on the “normal” side. Ask the patient to place her hand behind her head on the side you are examining.

### Step 08

Systematically examine all areas of the breast with your hand laid flat on the breast. Start from outside and work towards the nipple. Imagine that the breast is a clock face and examine at each ‘hour’.

Don’t forget that the breast tissue extends towards the [axilla](https://en.wikipedia.org/wiki/Axilla) in the ‘axillary tail’.

Ensure you ask the patient if she experiences any pain during examination.

### Step 09

Palpate the nipple and the tissue deep to it to check for any lumps. If the patient has reported a nipple discharge you could ask her to try and reproduce this at this stage.

### Step 10

Swapping which hand is behind the patient’s head, you should now examine the other breast in the same manner.

### Step 11

Examine both [axillae](https://en.wikipedia.org/wiki/Axilla) for any enlarged [lymph nodes](https://en.wikipedia.org/wiki/Lymph_node) (you may wish to put on gloves for this part of the examination).

Whilst examining the patient’s axilla, you should fully support the weight of that arm with yours. Examine the axilla with your other hand ensuring that you feel all four walls (anterior, posterior, medial and lateral) as well as feeling into the apex of the axilla.

Repeat this on the other side.

### Step 12

Palpate the [supraclavicular fossa](https://en.wikipedia.org/wiki/Supraclavicular_fossa) on both sides to check for [lymphadenopathy](https://en.wikipedia.org/wiki/Lymphadenopathy).

### Step 13

Ensure that patient is appropriately covered and thank them.

Wash your hands and report your findings to the examiner. If you had noticed any lumps then you should be able to comment on its size, position, mobility, consistency, also noting its relationship to other structures (i.e. skin, deep muscle).

## Conclusion

An extension to this station would be the investigation of a breast abnormality such as a lump. For this you should understand what is meant by ‘triple assessment’, which is usually performed at an outpatient clinic.

* Clinical examination.
* Imaging.
* Biopsy.

Imaging modality will primarily depend upon the patient’s age. Younger women have denser breast tissue and so the use of [mammography](https://en.wikipedia.org/wiki/Mammography) is of limited use; in these patients [ultrasound](https://en.wikipedia.org/wiki/Ultrasound) is the best first form of imaging. Older patients may undergo both mammography and/or ultrasound.

If there is any abnormality detected in the examination, or imaging, then biopsies are taken. This can be in the form of [Fine Needle Aspiration Cytology (FNAC)](https://en.wikipedia.org/wiki/Fine-needle_aspiration) or a core (Tru-Cut) biopsy. If neither of these provide a reliable answer then an open surgical biopsy may be required.