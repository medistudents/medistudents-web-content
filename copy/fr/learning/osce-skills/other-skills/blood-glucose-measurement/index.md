```metadata
{
  "title":"Blood Glucose Measurement",
  "description":"Blood glucose monitoring, is the skill where you test a patient’s blood to assess the glucose (sugar) levels within the blood",
  "last_updated":"2018-02-15"
}
```
# Blood Glucose Measurement

## Foreword

Blood glucose monitoring, is the skill where you test a patient’s blood to assess the glucose (sugar) levels within the blood.

Patients with [diabetes mellitus](https://en.wikipedia.org/wiki/Diabetes_mellitus) will perform this on themselves daily to monitor their glucose levels.  It is therefore performed routinely on diabetic patients in hospital and also in all unconscious/collapsed patients that are brought into A&E to ensure the patient is not having a [hypoglycaemic](https://en.wikipedia.org/wiki/Hypoglycemia) or [hyperglycaemic](https://en.wikipedia.org/wiki/Hyperglycemia) episode.

It is an easy skill to master and should gain you good marks in an OSCE, the difficulty often lies in interpretation of the results.

## Procedure Steps


### Step 01

Ensure you have all necessary equipment for the procedure:

* Gloves.
* An alcohol wipe.
* A [glucose monitor](https://en.wikipedia.org/wiki/Blood_glucose_monitoring).
* Test strips.
* Spring loaded lancet.
* Cotton wool.

![Blood glucose measurement equipment](../../../../../../media/learning/osce-skills/img/blood-glucose-measurement-equipment1.jpg)

### Step 02

Introduce yourself and explain the procedure to the patient. The patient may be used to performing this procedure on themselves, however it is important to make sure of this and that you have their consent.

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 03

Wash your hands and put on your gloves.
Turn on the glucose monitor and ensure that it has been calibrated. If not, insert the calibration strip and allow it to calibrate.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 04

Clean the tip of one of the patient’s fingers with an alcohol wipe and allow it to dry. Note that any sugar on the patient’s finger from a sweet/candy will give a falsely elevated reading.

![Clean the tip of the patients finger](../../../../../../media/learning/osce-skills/img/clean-the-tip-of-the-patients-finger.jpg)

### Step 05

Prepare the test strip, ensuring that it is still in date. Load it into the glucose monitor.

![Ensure the test strip is still in date, the example shown is not.](../../../../../../media/learning/osce-skills/img/ensure-the-test-strip-is-still-in-date.jpg)

![Load the test strip into the glucose monitor](../../../../../../media/learning/osce-skills/img/load-the-test-strip-into-the-glucose-monitor.jpg)

### Step 06

Open the lancet carefully. Prick the side of the patient’s finger with the lancet and squeeze the finger.

Wipe away the first drop of blood and squeeze the finger again to form another drop.

Place this drop on the test strip so that it covers the strip entirely.

![Open the lancet](../../../../../../media/learning/osce-skills/img/open-the-lancet.jpg)

![Open the lancet 2](../../../../../../media/learning/osce-skills/img/open-the-lancet-2.jpg)

![Prick the side of the patients finger](../../../../../../media/learning/osce-skills/img/prick-the-side-of-the-patients-finger.jpg)

![Place the drop of blood onto the test strip](../../../../../../media/learning/osce-skills/img/place-the-drop-of-blood-onto-the-test-strip.jpg)

![Ensure the blood covers the strip entirely](../../../../../../media/learning/osce-skills/img/ensure-the-blood-covers-the-strip-entirely.jpg)

### Step 07

Give the patient a piece of cotton wool to stop the bleeding.

Safely dispose of the lancet, the test strip, and your gloves in the clinical waste bin.

Thank the patient and record the reading from the glucose monitor.

![Record the reading from the glucose monitor](../../../../../../media/learning/osce-skills/img/record-the-reading-from-the-glucose-monitor.jpg)

![Displose of the lancet in the clinical waste bin](../../../../../../media/learning/osce-skills/img/displose-of-the-lancet-in-the-clinical-waste-bin.jpg)

## Conclusion

An extension to this station may be that you are shown blood glucose results and to interpret what they mean.  It would therefore be worthwhile knowing what the values for normal, diabetes and impaired glucose tolerance are.  Follow up may include taking a venous blood glucose sample or arranging an [oral glucose tolerance test (OGTT)](https://en.wikipedia.org/wiki/Glucose_tolerance_test).
