```metadata
{
  "title":"Blood Pressure (BP) Measurement",
  "module":"Cardiovascular Medicine",
  "description":"Blood pressure (BP) monitoring is one of the principal vital signs. It assesses the pressure required by the heart to pump. It is routinely measured on all patients whether in hospital, outpatient clinics or by the patients GP",
  "last_updated":"2018-05-26"
}
```
# Blood Pressure (BP) Measurement

## Foreword

Blood pressure (BP) monitoring is one of the principal vital signs. It assesses the pressure required by the heart to pump. This is routinely measured on all patients whether in hospital, outpatient clinics or by the patient's GP. 

Hypotension (low BP) can cause dizziness and falls, especially in the elderly. Hypertension (high BP) is a risk factor for cardiovascular disease but can also be a cause of headaches and other conditions.  In emergency situations it can also assess how unwell a patient is, for example: hypotension can indicate sepsis or volume depletion, hypertension can indicate pain or intracranial pathology.

It is a basic but necessary skill to learn and one that is frequently examined on.

## Procedure Steps

### Step 01

Ensure that you have the necessary equipment:

* A [sphygmomanometer](http://en.wikipedia.org/wiki/Sphygmomanometer).
* A [stethoscope](http://en.wikipedia.org/wiki/Stethoscope).
* Hand cleansing gel.

![Equipment for measuring blood pressure](../../../../../../media/learning/osce-skills/img/equipment-for-measuring-blood-pressure.jpg)

### Step 02

It is important when measuring blood pressure to build a rapport with your patient to prevent [White Coat Syndrome ](http://en.wikipedia.org/wiki/White_coat_syndrome) which may give you an inaccurately high reading.

Therefore, ensure you introduce yourself to the patient, explain the procedure answering any questions they may have, and ask for their consent. You should also explain to them that they may feel some discomfort as you inflate the cuff, but that this will be short-lived. Make sure they are sitting comfortably, with their arm rested.

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient2.jpg)

### Step 03

As with all clinical procedures, it is vital that you first wash your hands with alcohol cleanser and allow to dry.

![Sanitise your hands using alcohol cleanser](../../../../../../media/learning/osce-skills/img/sanitise-your-hands-using-alcohol-cleanser.jpg)

### Step 04

Ensure that you have selected the correct cuff size for your patient. A different cuff size may be required for obese patients or children.

![Select the correct cuff size to suit your patient](../../../../../../media/learning/osce-skills/img/select-the-correct-cuff-size-to-suit-your-patient.jpg)

### Step 05

Wrap the cuff around the patient’s upper arm ensuring the arrow is in line with the [brachial artery](http://en.wikipedia.org/wiki/Brachial_artery). This should be determined by feeling the *brachial pulse*.

![Ensure correct placement of the cuff](../../../../../../media/learning/osce-skills/img/ensure-correct-placement-of-the-cuff.jpg)

![The brachial artery](../../../../../../media/learning/osce-skills/img/gray525.png)

### Step 06

Determine a rough value for the systolic blood pressure. This can be done by palpating the brachial or radial pulse and inflating the cuff until the pulse can no longer be felt. The reading at this point should be noted and the cuff deflated.

![Inflate the cuff to determine a rough value for the systolic blood pressure](../../../../../../media/learning/osce-skills/img/inflate-the-cuff-to-determine-a-rough-value-for-the-systolic-blood-pressure.jpg)

### Step 07

Now that you have a rough value, the true value can be measured. Place the diaphragm of your stethoscope over the brachial artery and re-inflate the cuff to 20-30 mmHg higher than the estimated value taken before.

Then deflate the cuff at 2-3 mmHg per second until you hear the first [Korotkoff sound](http://en.wikipedia.org/wiki/Korotkoff_sounds) – this is the systolic blood pressure.

Continue to deflate the cuff until the sounds disappear, the 5th Korotokoff sound – this is the diastolic blood pressure.

![Record the true blood pressure](../../../../../../media/learning/osce-skills/img/record-the-true-blood-pressure.jpg)

### Step 08

If the blood pressure is greater than 140/90, you should wait for 1 minute and re-check. Note that a normal reading differ for diabetic patients.

### Step 09

Explain to your examiner that you would want to check the blood pressure standing to check for a significant drop (>20 mmHg after 2 minutes). This would suggest a [postural hypotension](http://en.wikipedia.org/wiki/Postural_hypotension).

### Step 10

Finally, you should inform the patient of their reading, and thank them. If, after rechecking, the blood pressure remains elevated, advise the patient they will need this repeated in future which ensures appropriate follow-up.
