```metadata
{
  "title":"Electrocardiography (ECG)",
  "description":"A 12 lead electrocardiography (ECG) shows the electrical activity occurring in a patient's heart at the moment it is recorded",
  "last_updated":"2018-05-26"
}
```
# Electrocardiography (ECG)

## Foreword

A 12 lead electrocardiography (ECG) shows the electrical activity occurring in a patient's heart at the moment it is recorded. It is an important diagnostic investigation for arrhythmia's, such as atrial fibrillation, or in more serious conditions such as myocardial infarction or in cardiac arrest.  It is therefore an essential skill to be able to perform, as well as being able to interpret the ECG findings.

## Procedure Steps

### Step 01

For this station you will need:

* Hand sanitiser.
* ECG machine.
* 10 sticky pads for attaching the electrodes to.
* Occasionally need wipes and/or razor.

### Step 02

Wash your hands and introduce yourself to the patient and clarify their identity.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 03

Explain what you are going to do and gain consent to proceed.  Clarify whether the patient knows why the test is being performed, such as for chest pain.

### Step 04

Ensure the patient is comfortable, for this test the patient must have their top off so offer a chaperone.

### Step 05

For the ECG pads to stick there must be good contact with the skin, therefore ensure there is nothing which could prevent this, for example shave hair or wipe off any cream/lotion.

### Step 06

Input the patients data into the machine correctly, thus ensuring the correct details are printed onto the ECG.

### Step 07

Although this is called a 12 lead ECG you only need 10 sticky pads, 4 for the limb leads and 6 for chest leads.  Place them in the following locations:

* **Right wrist** (or shoulder).
* **Left wrist** (or shoulder).
* **Right ankle**, laterally.
* **Left ankle**, laterally.
* **V1** in 4th intercostal space at right sternal edge.
* **V2** in 4th intercostal space at left sternal edge.
* **V3** between V2 and V4.
* **V4** in 5th intercostal space in the mid-clavicular line.
* **V5** horizontal to V4 in anterior axillary line.
* **V6** horizontal to V4 and V5 in mid-axillary line.

### Step 08

Attach the leads to the pads.

**Within Europe** the limb leads are coloured: red, yellow, green and black.  One way to remember is the order of traffic lights:

* Red (R wrist).
* Yellow (L wrist).
* Green (L ankle).
* Black (R ankle).

Other countries may use a different colour scheme ([see here for more information](http://en.wikipedia.org/wiki/Electrocardiography)).

The chest leads are usually numbered V1 - V6, and are attach as shown:

![Precordial leads in ECG](../../../../../../media/learning/osce-skills/img/precordial-leads-in-ecg.jpg)

### Step 09

Once all the leads are attached ask the patient to lie as still as possible. Record the reading and print a copy. If the patient had any chest pain at the time of recording you should record this on the ECG.

### Step 10

Disconnect the leads from the pads and allow the patient to remove the pads themselves, or offer assistance if needed. Offer a tissue as the pads are sticky.

### Step 11

Allow the patient to dress and thank them.

## Conclusion

An extension to this station would be electrocardiography (ECG) interpretation; you should therefore be familiar with the most common ECG findings. 

Typically [sinus rhythm](http://en.wikipedia.org/wiki/Sinus_rhythm), [atrial fibrillation (AF)](http://en.wikipedia.org/wiki/Atrial_fibrillation), [ST elevation (STEMI)](http://en.wikipedia.org/wiki/ST_elevation) are tested, however also knowing what [ventricular fibrillation (VF)](http://en.wikipedia.org/wiki/Ventricular_fibrillation) and [ventricular tachycardia (VT)](http://en.wikipedia.org/wiki/Ventricular_tachycardia) looks like is important for advanced life support skills.

![Sinus Rhythm Labels](../../../../../../media/learning/osce-skills/img/sinus-rhythm-labels.png)

![12 lead ECG showing Atrial Fibrillation at approximately 150 beats per minute](../../../../../../media/learning/osce-skills/img/12-lead-ecg-showing-atrial-fibrillation-at-approximately-150-beats-per-minute.jpg)

![12 Lead ECG showing ST Elevation (orange)](../../../../../../media/learning/osce-skills/img/12-lead-ecg-showing-st-elevation-orange.jpg)

![12 Lead ECG showing Ventricular Fibrillation](../../../../../../media/learning/osce-skills/img/12-lead-ecg-showing-ventricular-fibrillation.png)

![12 Lead ECG showing  Ventricular Tachycardia](../../../../../../media/learning/osce-skills/img/12-lead-ecg-showing-ventricular-tachycardia.jpg)