```metadata
{
  "title":"Intravenous Cannulation (IV)",
  "description":"Insertion of an IV cannula involves putting a 'tube' into a patient's vein so that infusions can be inserted directly into the patient’s bloodstream",
  "last_updated":"2018-05-26"
}
```
# Intravenous Cannulation (IV)

## Foreword

Insertion of an intravenous (IV) cannula involves connecting a tube into a patient's vein so that infusions can be inserted directly into the patient’s bloodstream. Cannulas (also known as venflons) are available in various colours, each of which correspond to the size of the tube. The required size depends on:

* What will be infused, for example: colloid, crystalloid, blood products or medications.
* Or, at the rate the infusion is to run. 

In addition, the patients veins may dictate the size to use, for example you may only be able to insert a blue (small) cannula into an elderly patient's vein.

This is a core clinical skill to know, and is frequently examined upon in medical school. If you are currently applying to medical school, you can find more information on UCAT preparation [here](/en/ucat/ucat-2020-complete-guide).

## Procedure Steps

### Step 01

Introduce yourself to the patient and clarify the patient’s identity. Explain the procedure to the patient and gain informed consent to continue. Inform that cannulation may cause some discomfort but that it will be short lived.

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient2.jpg)

### Step 02

Ensure that you have all of your equipment ready as follows:

* Alcohol cleanser.
* Gloves.
* An alcohol wipe.
* A disposable tourniquet.
* An IV cannula.
* A suitable plaster.
* A syringe.
* Saline.
* A clinical waste bin.

![Equipment for intravenous cannulation (clinical waste bin not pictured)](../../../../../../media/learning/osce-skills/img/equipment-for-intravenous-cannulation.jpg)

### Step 03

Sanitise your hands using alcohol cleanser.

![Sanitise your hands using alcohol cleanser](../../../../../../media/learning/osce-skills/img/sanitise-your-hands-using-alcohol-cleanser.jpg)

### Step 04

Position the arm so that it is comfortable for the patient and identify a vein.

### Step 05

Apply the tourniquet and re-check the vein.

![Apply the tourniquet](../../../../../../media/learning/osce-skills/img/apply-the-tourniquet.jpg)

![Re-check the vein](../../../../../../media/learning/osce-skills/img/re-check-the-vein.jpg)

### Step 06

Put on your gloves, clean the patient’s skin with the alcohol wipe and let it dry.

![Clean the patients skin with the alcohol wipe](../../../../../../media/learning/osce-skills/img/clean-the-patients-skin-with-the-alcohol-wipe.jpg)

### Step 07

Remove the cannula from its packaging and remove the needle cover ensuring not to touch the needle.

![Remove the needle cover](../../../../../../media/learning/osce-skills/img/remove-the-needle-cover.jpg)

### Step 08

Stretch the skin distally and inform the patient that they should expect a sharp scratch.

### Step 09

Insert the needle, bevel upwards at about 30 degrees. Advance the needle until a flashback of blood is seen in the hub at the back of the cannula

![Insert the needle, bevel upwards at about 30 degrees](../../../../../../media/learning/osce-skills/img/insert-the-needle-bevel-upwards-at-about-30-degrees.jpg)

![Flashback of blood is seen in the hub](../../../../../../media/learning/osce-skills/img/flashback-of-blood-is-seen-in-the-hub-at-the-back-of-the-cannula.jpg)

### Step 10

Once the flashback of blood is seen, progress the entire cannula a further 2mm, then fix the needle, advancing the rest of the cannula into the vein.

![Advance the rest of the cannula into the vein](../../../../../../media/learning/osce-skills/img/advance-the-rest-of-the-cannula-into-the-vein.jpg)

### Step 11

Release the tourniquet, apply pressure to the vein at the tip of the cannula and remove the needle fully. Remove the cap from the needle and put this on the end of the cannula.

![Release the tourniquet](../../../../../../media/learning/osce-skills/img/release-the-tourniquet.jpg)

![Remove the needle](../../../../../../media/learning/osce-skills/img/apply-pressure-to-the-vein-at-the-tip-of-the-cannula-and-remove-the-needle.jpg)

### Step 12

Carefully dispose of the needle into the sharps bin.

### Step 13

Apply the dressing to the cannula to fix it in place and ensure that the date sticker has been completed and applied.

![Apply the plaster to the cannula](../../../../../../media/learning/osce-skills/img/apply-the-plaster-to-the-cannula-to-fix-it-in-place.jpg)

### Step 14

Check that the use-by date on the saline has not passed. If the date is ok, fill the syringe with saline and flush it through the cannula to check for patency.

If there is any resistance, or if it causes any pain, or you notice any localised tissue swelling: immediately stop flushing, remove the cannula and start again.

### Step 15

Dispose of your gloves and equipment in the clinical waste bin, ensure the patient is comfortable and thank them

## Conclusion

An extension to this procedure may to set up an IV drip.

---

This guide is designed for students and doctors. If you are applying for medical school and would like more information on the UCAT please check out our [complete guide](/en/ucat/ucat-2020-complete-guide) and our [guide on how to practice for your exam](/en/ucat/ucat-practice).