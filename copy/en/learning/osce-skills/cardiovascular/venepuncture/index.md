```metadata
{
  "title":"Venepuncture",
  "description":"Venepuncture involves taking blood from a patient's vein. This is then sent to the laboratory and tested for various conditions, depending upon which tests you request",
  "last_updated":"2017-09-18"
}
```
# Venepuncture

## Foreword

This skill involves taking blood from a patient's vein. This is then sent to the laboratory and tested for various conditions, depending upon which tests you request.

Each hospital has its own policy for blood sampling, such as some use syringes, butterflies, the Vacutainer system, or the Monovette system.  In this station we will be demonstrating the use of the Vacutainer system.  Whichever method is used however, there is little difference in the technique. 

Remember that different hospitals may use different coloured blood tubes, so if you are asked to take a specific blood sample be sure to check which colour of tube you will require.

This is a core clinical skill that you should be proficient in, and as such is frequently examined on.

## Procedure Steps

### Step 01

Introduce yourself to the patient and confirm their identity. Explain what you are going to do and obtain consent. As this is a potentially painful procedure so explain that they may feel some discomfort and answer any questions they may have.

### Step 02

Ensure that you have all of your equipment ready as follows:

* Alcohol cleanser.
* Gloves.
* An alcohol wipe.
* A disposable tourniquet.
* Needle.
* Needle holder.
* Appropriate sample tubes.
* Gauze.
* A plaster.
* A clinical waste bin.

![Venepuncture kit](../../../../../../media/learning/osce-skills/img/equipment-required-for-venepuncture.jpg)

### Step 03

Wash and sanitise your hands using alcohol cleanser.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 04

Place the patient’s arm in a comfortable position so that you can identify the veins. Select a vein, apply the tourniquet and re-check the vein making sure you are comfortable with its course.

![Applying the tourniquet](../../../../../../media/learning/osce-skills/img/apply-the-tourniquet-2.jpg)

![Re-check the vein](../../../../../../media/learning/osce-skills/img/re-check-the-vein-2.jpg)

### Step 05

Put on your gloves. This not only helps protect the patient but also yourself from any contact with blood should there be a spillage. It also reduces the risk of blood-borne virus transmission should you obtain a needle stick injury.

### Step 06

Clean the venepuncture site using the alcohol wipe in downward strokes.

![Clean the venepuncture site](../../../../../../media/learning/osce-skills/img/clean-the-venepuncture-site.jpg)

### Step 07

Remove the grey cap from the needle, attach the needle to the needle holder, and remove the other cap to reveal the needle.

![Remove the grey cap from the needle](../../../../../../media/learning/osce-skills/img/remove-the-grey-cap-from-the-needle.jpg)

![Attach the needle to the needle holder](../../../../../../media/learning/osce-skills/img/attach-the-needle-to-the-needle-holder.jpg)

![Remove the other cap to reveal the needle](../../../../../../media/learning/osce-skills/img/remove-the-other-cap-to-reveal-the-needle.jpg)

### Step 08

Retract the skin distally, prepare the patient for a 'sharp scratch', and insert the needle into the vein bevel upwards.

![Retract the skin distally](../../../../../../media/learning/osce-skills/img/retract-the-skin-distally.jpg)

![Insert the needle into the vein bevel upwards](../../../../../../media/learning/osce-skills/img/insert-the-needle-into-the-vein.jpg)

### Step 09

Once in the vein, attach a bottle and let it fill, changing if more than one bottle is required. Whilst the last bottle is filling, loosen the tourniquet and when full, remove it from the needle holder.

![Attach a bottle](../../../../../../media/learning/osce-skills/img/once-in-the-vein-attach-a-bottle-and-let-it-fill.jpg)

![Let the bottle fill](../../../../../../media/learning/osce-skills/img/let-the-bottle-fill.jpg)

![Let the bottle fill](../../../../../../media/learning/osce-skills/img/whilst-the-last-bottle-is-filling-loosen-the-tourniquet.jpg)

![When the bottle is full, remove it from the needle holder](../../../../../../media/learning/osce-skills/img/when-the-bottle-is-full-remove-it-from-the-needle-holder.jpg)

### Step 10

Withdraw the needle, applying the gauze over the puncture site. Ask the patient to apply pressure with the gauze, keeping their arm straight as bending the arm may increase any bruising.

![Withdraw the needle](../../../../../../media/learning/osce-skills/img/withdraw-the-needle-applying-the-gauze.jpg)

![Apply the gauze over the puncture site](../../../../../../media/learning/osce-skills/img/apply-the-gauze-over-the-puncture-site.jpg)

![Ask the patient to apply pressure with the gauze](../../../../../../media/learning/osce-skills/img/ask-the-patient-to-apply-pressure-with-the-gauze.jpg)

### Step 11

Carefully dispose of the needle in the sharps bin.

![Carefully dispose of the needle in the sharps bin](../../../../../../media/learning/osce-skills/img/dispose-of-the-needle-in-the-sharps-bin.jpg)

### Step 12

At this point, invert the tubes 3 times to ensure full mixing with any additives in the tubes such as heparin.

### Step 13

Whilst still at the bedside, ensure that you complete all of the patient details required on the bottles.

### Step 14

Ensure that any bleeding has stopped and if so, apply the plaster. Remove and dispose of your gloves, along with the remaining equipment in the clinical waste bin. 

Thank the patient.