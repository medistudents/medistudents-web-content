```metadata
{
  "title":"Arterial Blood Gases (ABG)",
  "description":"An arterial blood gas (ABG) is performed so that an accurate measurement of oxygen and carbon dioxide levels can be obtained, which then allows the patients oxygen to be delivered appropriately.",
  "last_updated":"2018-05-26"
}
```
# Arterial Blood Gases (ABG)

## Foreword

An [arterial blood gas (ABG)](http://en.wikipedia.org/wiki/Arterial_blood_gas) is a blood test carried out by taking blood from an artery, rather than a vein. It is performed so that an accurate measurement of oxygen and carbon dioxide levels can be obtained, which then allows the patients oxygen to be delivered appropriately.  It is performed on patients in respiratory distress, such as an asthma attack.  This skill is one you should be familiar with and can be extended to involve the interpretation of blood gas results.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain what you would like to do and obtain consent. This is a slightly uncomfortable procedure so you should let the patient know this.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 02

Gather the necessary equipment:

* A blue (23 G) needle.
* 2ml syringe with heparin.
* A cap for the syringe.
* A plastic bung.
* Local anaesthetic (plus needle and syringe for giving).
* Alcohol gel.
* Gauze.
* Gloves.
* A sharps bin.

Usually, the syringe, needle, cap and bung are all provided in one pack.

![Equipment required for measuring arterial blood gases](../../../../../../media/learning/osce-skills/img/equipment-required-for-measuring-arterial-blood-gases.jpg)

![ABG kit contents](../../../../../../media/learning/osce-skills/img/abg-kit-contents.jpg)

### Step 03

Position the patient’s arm with the wrist extended.

### Step 04

Locate the [radial artery](http://en.wikipedia.org/wiki/Radial_artery) with your index and middle fingers.

Perform [Allen’s test](http://en.wikipedia.org/wiki/Allen_test) where you compress both the radial and ulnar arteries at the same time. The hand should become white, release the ulnar artery and the colour should return to the hand. This ensures that there will still be a blood supply to the hand should the ABG cause a blockage in the radial artery.

![Locate the radial artery with your index and middle fingers](../../../../../../media/learning/osce-skills/img/locate-the-radial-artery-with-your-index-and-middle-fingers.jpg)

![Allens Test](../../../../../../media/learning/osce-skills/img/allens-test.jpg)

### Step 05

Put on your gloves and attach the needle to the heparinised syringe.

Prepare your local anaesthetic and give a small amount over the palpable radial artery.

### Step 06

Take the cap off the needle, flush the heparin through the syringe and again locate the *radial artery* using your non-dominant hand.

![Remove the cap from the needle](../../../../../../media/learning/osce-skills/img/remove-the-cap-from-the-needle.jpg)

### Step 07

Let the patient know you are about to proceed and to expect a sharp scratch.

Insert the needle at 30 degrees to the skin at the point of maximum pulsation of the *radial artery*. Advance the needle until arterial blood flushes into the syringe. The arterial pressure will cause the blood to fill the syringe.

Remove the needle/syringe placing the needle into the bung. Press firmly over the puncture site with the gauze to halt the bleeding. Remain pressed for 5 minutes.

![Prepare to insert the needle](../../../../../../media/learning/osce-skills/img/prepare-to-insert-the-needle.jpg)

![Remove the needle](../../../../../../media/learning/osce-skills/img/remove-the-needle.jpg)

![Place the needle into the bung](../../../../../../media/learning/osce-skills/img/place-the-needle-into-the-bung.jpg)

### Step 08

Remove the needle and discard safely in the sharps bin.

![Remove the needle from the syringe](../../../../../../media/learning/osce-skills/img/remove-the-needle-from-the-syringe.jpg)

![Safetly discard the needle into the sharps bin](../../../../../../media/learning/osce-skills/img/safetly-discard-the-needle-into-the-sharps-bin.jpg)

### Step 09

Cap the syringe, push out any air within it, and send immediately for analysis ensuring that the sample is packed in ice.

Remove your gloves and dispose them in the clinical waste bin.  Wash your hands and thank the patient.

![Cap the syringe](../../../../../../media/learning/osce-skills/img/cap-the-syringe.jpg)

## Conclusion

An extension to this station may be [arterial blood gas interpretation](http://en.wikipedia.org/wiki/Arterial_blood_gas). Before attempting to interpret the results you should know whether the patient was on room air or on oxygen when the sample was taken, and if on oxygen, what concentration.

It is also useful to know whether the patient has a temperature or not, and this should be clearly written on the sample.

### Results

The following information shows the changes in pH, CO<sub>2</sub> and bicarbonate concentrations in different situations:

#### Metabolic Acidosis

* **pH:** ↓
* **pCO<sub>2</sub>:** ↔
* **Bicarbonate:** ↓

#### Respiratory Acidosis

* **pH:** ↓
* **pCO<sub>2</sub>:** ↑
* **Bicarbonate:** ↔

#### Metabolic Alkalosis

* **pH:** ↑
* **pCO<sub>2</sub>:** ↔
* **Bicarbonate:** ↑

#### Respiratory Alkalosis

* **pH:** ↑
* **pCO<sub>2</sub>:** ↓
* **Bicarbonate:** ↔

