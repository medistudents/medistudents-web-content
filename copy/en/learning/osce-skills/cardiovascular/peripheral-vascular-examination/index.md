```metadata
{
  "title":"Peripheral Vascular (PVS) Examination",
  "description":"The peripheral vascular examination is performed to elicit signs of peripheral vascular pathology i.e. examining the blood vessels in the extremities",
  "last_updated":"2018-05-26"
}
```
# Peripheral Vascular (PVS) Examination

## Foreword

The peripheral vascular (PVS) examination is performed to elicit signs of peripheral vascular pathology, such as examining the blood vessels in the extremities. 

[Peripheral vascular disease](http://en.wikipedia.org/wiki/Peripheral_vascular_disease) (PVD) is a common reason for referral to vascular clinics, conditions of which include [intermittent claudication](http://en.wikipedia.org/wiki/Intermittent_claudication) and in emergency situations [ischaemia](http://en.wikipedia.org/wiki/Ischaemia) of the limbs.

Like most examination stations this follows the usual procedure of:

* Inspect,
* Palpate
* Auscultate

## Procedure Steps

### Step 01

Begin by washing your hands.

Introduce yourself to the patient and clarify their identity. Explain what you would like to do and gain their consent.

For this station the patient should by lying on the bed and ideally exposed from the waist down, however for the purposes of the exam the patient will likely be wearing shorts.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

Perform a general observation of the patient, noting whether they are comfortable at rest as well as their general wellbeing. Comment on the general appearance of the legs, including any obvious abnormalities such as muscle wasting or scars.

Furthermore, note any appliances or medications which may be positioned around the bed.

### Step 03

Focus the observation towards the patient’s legs, feet and toes. Signs to note include:

* Any signs of [gangrene](http://en.wikipedia.org/wiki/Gangrene) or pre-gangrene such as missing toes or blackening of the extremities.
* The presence of any ulcers. Ensure you check all around the feet, including behind the ankle and between the toes. These may be venous or arterial – one defining factor is that venous ulcers tend to be painless whereas arterial are painful.
* Any skin changes such as pallor, varicose eczema, sites of previous ulcers, or change in colour, for example: purple/black from [haemostasis](http://en.wikipedia.org/wiki/Hemostasis) or brown from [haemosiderin](http://en.wikipedia.org/wiki/Hemosiderin) deposition.
* Presence of any [varicose veins](http://en.wikipedia.org/wiki/Varicose_veins) – often seen best with the patient standing.

![Observation of the legs](../../../../../../media/learning/osce-skills/img/observation-of-the-legs.jpg)

### Step 04

Now move onto palpating the legs. This will include an assessment of the temperature of each leg. Starting distally, feel with the back of your hand and compare the legs to each other noting any difference.

![Assessment of the temperature of each leg](../../../../../../media/learning/osce-skills/img/assessment-of-the-temperature-of-each-leg.jpg)

### Step 05

Check capillary return by compressing the nail bed and then releasing it. Normal colour should return within 2 seconds.

If this result is abnormal, you should suggest to the examiner that you would like to perform [Buerger’s Test](http://en.wikipedia.org/wiki/Buerger%27s_test). This involves raising the patient’s feet to 45 degrees. In the presence of poor arterial supply, pallor rapidly develops.

Following this, place the feet over the side of the bed, [cyanosis](http://en.wikipedia.org/wiki/Cyanosis) may then develop.

![Capillary return - compress the nail bed](../../../../../../media/learning/osce-skills/img/capillary-return-compress-the-nail-bed.jpg)

![Capillary return - release the nail bed](../../../../../../media/learning/osce-skills/img/capillary-return-release-the-nail-bed.jpg)

![Buergers Test 1](../../../../../../media/learning/osce-skills/img/perform-buergers-test.jpg)

![Buergers Test 2](../../../../../../media/learning/osce-skills/img/place-the-feet-over-the-side-of-the-bed-to-check-for-cyanosis.jpg)

### Step 06

Any varicosities which you noted in the observation should now be palpated. If these are hard to the touch, or painful when touched, it may suggest [thrombophlebitis](http://en.wikipedia.org/wiki/Thrombophlebitis).

### Step 07

Finally for palpation, you should feel for the abdominal aorta and each of the peripheral pulses. These are:

* **Aorta:** this should be palpated just to the left of the midline in the epigastrium, note whether the pulsation in expansile as in an aneurysm.
* **Femoral:** feel at the mid inguinal point, below the inguinal ligament.
* **Popliteal:** ask the patient to flex their knee to roughly 45 degrees keeping their foot on the bed, place both hands on the front of the knee and place your fingers in the popliteal space.
* **Posterior tibial:** felt posterior to the medial malleolus of the tibia.
* **Dorsalis pedis:** feel on the dorsum of the foot, lateral to the extensor tendon of the great toe.

You should feel these on both sides and comment on their strength, comparing one side relative to the other.

![Feel for the Abdominal Aorta pulse](../../../../../../media/learning/osce-skills/img/feel-for-the-abdominal-aorta.jpg)

![Feel for the Popliteal pulse](../../../../../../media/learning/osce-skills/img/feel-for-the-popliteal-pulse.jpg)

![Feel for the Posterior Tibial pulse](../../../../../../media/learning/osce-skills/img/feel-for-the-posterior-tibial-pulse.jpg)

![Feel for the Dorsalis Pedis pulse](../../../../../../media/learning/osce-skills/img/feel-for-the-dorsalis-pedis-pulse.jpg)

### Step 08

Check for radio-femoral delay by palpating both the radial and femoral pulses on one side of the body at the same time. The pulsation should occur at the same time. Any delay may suggest [coarctation of the aorta](http://en.wikipedia.org/wiki/Coarctation_of_the_aorta).

### Step 09

There is little to auscultate in a peripheral vascular examination. However, you should listen for femoral and abdominal aortic bruits.

![Listen for femoral and abdominal aortic bruits](../../../../../../media/learning/osce-skills/img/listen-for-femoral-and-abdominal-aortic-bruits.jpg)

### Step 10

On completion, thank the patient for their time and ensure they are comfortable and well-covered. Remember to wash your hands and report your findings to the examiner.