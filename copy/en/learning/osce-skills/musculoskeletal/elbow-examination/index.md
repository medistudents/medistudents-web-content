```metadata
{
  "title":"Elbow Examination",
  "description":"Elbow complaints are often related to pain e.g. epicondylitis (tennis and golfer’s elbow), fractures and bursitis, although can also be skin related with regards to other medical conditions e.g. psoriasis and rheumatoid arthritis",
  "last_updated":"2018-05-26"
}
```
# Elbow Examination

## Foreword

Elbow complaints are often related to pain, such as [epicondylitis](http://en.wikipedia.org/wiki/Epicondylitis) (tennis and golfer’s elbow), fractures, and [bursitis](http://en.wikipedia.org/wiki/Bursitis). Complaints can also be skin related with regards to other medical conditions, such as psoriasis and rheumatoid arthritis. Occasionally elbow problems can also cause [ulnar nerve entrapment](http://en.wikipedia.org/wiki/Ulnar_nerve_entrapment).

The elbow examination, along with all other joint examinations, is commonly tested on in OSCEs.  You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” and occasionally some special tests.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity and explain what you would like to examine, gain their consent.

Ensure the elbows are appropriately exposed, in this case the patient will probably be wearing a t-shirt.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-male.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient2.jpg)

### Step 02

Begin with observation of the joint. Inspect:

* The front to check the carrying angle.
* From the side to check for a fixed flexion deformity.
* From behind and on the inside to check for scars, swellings, rashes, [rheumatoid nodules](http://en.wikipedia.org/wiki/Rheumatoid_nodule) and [psoriatic plaques](http://en.wikipedia.org/wiki/Psoriasis).

![Inspect the front](../../../../../../media/learning/osce-skills/img/inspect-the-front-to-check-the-carrying-angle.jpg)

![Inspect the side](../../../../../../media/learning/osce-skills/img/inspect-the-side-to-check-for-a-fixed-flexion-deformity.jpg)

![Inspect behind and on the inside for scars](../../../../../../media/learning/osce-skills/img/inspect-behind-and-on-the-inside.jpg)

### Step 03

Feel the elbow, assessing the joint temperature relative to the rest of the arm.

![Assess the joint temperature](../../../../../../media/learning/osce-skills/img/assess-the-joint-temperature-relative-to-the-rest-of-the-arm.jpg)

### Step 04

Palpate the [olecranon](http://en.wikipedia.org/wiki/Olecranon) process as well as the lateral and medial [epicondyles](http://en.wikipedia.org/wiki/Medial_epicondyle_of_the_humerus) for tenderness.

![Palpate the medial epicondyle](../../../../../../media/learning/osce-skills/img/palpate-the-olecranon-process.jpg)

### Step 05

The movements at the elbow joint are all fairly easy to describe and assess, which are:

* Flexion
* Extension
* Pronation
* Supination

Once these have been assessed actively they should be checked passively feeling for [crepitus](http://en.wikipedia.org/wiki/Crepitus).

![Flexion joint movement](../../../../../../media/learning/osce-skills/img/flexion-joint-movement.jpg)

![Extension joint movement](../../../../../../media/learning/osce-skills/img/extension-joint-movement.jpg)

![Pronation joint movement](../../../../../../media/learning/osce-skills/img/pronation-joint-movement.jpg)

![Supination joint movement](../../../../../../media/learning/osce-skills/img/supination-joint-movement.jpg)

### Step 06

Finally check for [tennis elbow](http://en.wikipedia.org/wiki/Tennis_elbow) and [golfer’s elbow](http://en.wikipedia.org/wiki/Golfer%27s_elbow). Check each of these individually to eliminate them.

Tennis elbow localises pain over the lateral epicondyle, particularly on active extension of the wrist with the elbow bent.

Golfer’s elbow pain localises over the medial epicondyle and is made worse by flexing the wrist.

![Check for tennis elbow](../../../../../../media/learning/osce-skills/img/check-for-tennis-elbow.jpg)

![Check for golfers elbow](../../../../../../media/learning/osce-skills/img/check-for-golfers-elbow.jpg)

### Step 07

On completion, thank the patient for their time and wash your hands. Report your findings to the examiner.