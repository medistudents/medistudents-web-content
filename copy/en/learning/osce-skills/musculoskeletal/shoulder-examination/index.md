```metadata
{
  "title":"Shoulder Examination",
  "description":"Shoulder complaints are fairly common presentations to A&E, general practice as well as orthopaedic clinics. Common acute problems include fractures and dislocation of the shoulder joint and rotator cuff injuries. Common chronic problems include frozen shoulder and arthritis",
  "last_updated":"2018-05-26"
}
```
# Shoulder Examination

## Foreword

Shoulder complaints are fairly common presentations to Accident and Emergency, general practice, and orthopaedic clinics. Common acute problems include fractures, dislocation of the shoulder joint, and rotator cuff injuries. Common chronic problems include [frozen shoulder](http://en.wikipedia.org/wiki/Frozen_shoulder) and arthritis.

The shoulder examination, along with all other joint examinations, is commonly tested on in OSCEs.  You should ensure you are able to perform this confidently.

The examination of all joints follows the general pattern of “look, feel, move” as well as occasionally special tests, in which this station has many.

## Procedure Steps

### Step 01

Wash your hands and introduce yourself to the patient. Clarify the patient’s identity. Explain what you would like to examine and gain their consent. 

Ensure the joint in which you wish to examine is appropriately exposed, in this case the patient should be exposed from the waist up. You should therefore offer a chaperone

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

The patient should be standing for this examination.  Begin with a general inspection of the shoulder joint from the front, side and back.  Look for symmetry, wasting, scars or obvious bony deformity.

![Inspection from the front](../../../../../../media/learning/osce-skills/img/shoulder-inspection-from-the-front.jpg)

![Inspection from the side](../../../../../../media/learning/osce-skills/img/shoulder-inspection-from-the-side.jpg)

![Inspection from the back](../../../../../../media/learning/osce-skills/img/shoulder-inspection-from-the-back.jpg)

![Leaning against the wall](../../../../../../media/learning/osce-skills/img/shoulder-inspection-leaning-against-the-wall.jpg)

### Step 03

Feel over the joint and its surrounding areas for the temperature of the joint. A raised temperature may suggest inflammation or infection in the joint.

![Assess the joint temperature](../../../../../../media/learning/osce-skills/img/assess-the-joint-temperature.jpg)

### Step 04

Systematically feel along both sides of the bony shoulder girdle. Start at the [sternoclavicular joint](http://en.wikipedia.org/wiki/Sternoclavicular_joint), work along the clavicle to the [acromioclavicular joint](http://en.wikipedia.org/wiki/Acromioclavicular_joint), feel the [acromion](http://en.wikipedia.org/wiki/Acromion), and then around the spine of the [scapula](http://en.wikipedia.org/wiki/Scapula). Feel the anterior and posterior joint lines of the [glenohumeral joint](http://en.wikipedia.org/wiki/Glenohumeral_joint) and finally the muscles around the joint for any tenderness.

![Feel along the sternoclavicular joint](../../../../../../media/learning/osce-skills/img/feel-along-the-sternoclavicular-joint.jpg)

![Feel the acromioclavicular joint](../../../../../../media/learning/osce-skills/img/feel-the-acromioclavicular-joint.jpg)

![Feel around the spine of the scapula](../../../../../../media/learning/osce-skills/img/feel-around-the-spine-of-the-scapula.jpg)

![The glenohumeral joint](../../../../../../media/learning/osce-skills/img/the-glenohumeral-joint.jpg)

### Step 05

The movements of the joint should begin by being performed actively. Ask the patient to bring their arm forward (flexion), bend their arm at the elbow and push backwards (extension). Bring their arm out to the side and up above their head (abduction). Flex the elbow and tuck it into the side and move the hand outwards (external rotation). And finally see how far they can place their hand up their back (internal rotation).

![Flexion movement](../../../../../../media/learning/osce-skills/img/shoulder-flexion-movement.jpg)

![Extension movement](../../../../../../media/learning/osce-skills/img/shoulder-extension-movement.jpg)

![Abduction movement](../../../../../../media/learning/osce-skills/img/shoulder-abduction-movement.jpg)

![Abduction movement 2](../../../../../../media/learning/osce-skills/img/shoulder-abduction-movement-2.jpg)

![External rotation movement](../../../../../../media/learning/osce-skills/img/shoulder-external-rotation-movement.jpg)

![Internal rotation movement](../../../../../../media/learning/osce-skills/img/shoulder-internal-rotation-movement.jpg)

### Step 06

Once all of these movements have been performed actively, you should perform them passively and feel for any crepitus whilst moving the joints

### Step 07

There are three special tests which can be performed on the shoulder:

* The impingement test.
* The apprehension test.
* Tthe scarf test.

The *impingement test* is performed by placing the shoulder out at 90 degrees with the arm hanging down, press back on the arm and check for any pain.

The *apprehension test* is similar but the arm is facing upwards and push back on the arm; the patient may be apprehensive about the movement as the joint feels unstable.

The *scarf test* is performed with the elbow flexed to 90 degrees, placing the patient’s hand on their opposite shoulder and pushing back. Again look for any discomfort.

![Impingement test](../../../../../../media/learning/osce-skills/img/shoulder-impingement-test.jpg)

![Apprehension test](../../../../../../media/learning/osce-skills/img/shoulder-apprehension-test.jpg)

![Scarf test](../../../../../../media/learning/osce-skills/img/shoulder-scarf-test.jpg)

### Step 08

Finally, perform two quick and easy function tests. This involves the patient placing their hands behind their head and behind their back. This checks that they can perform everyday tasks.

![Arms behind head](../../../../../../media/learning/osce-skills/img/shoulder-inspection-arms-behind-head.jpg)

![Arms behind back](../../../../../../media/learning/osce-skills/img/shoulder-inspection-from-the-back-arms-crossed.jpg)

### Step 09

On completion, allow the patient to dress, thank them for their time, and wash your hands. Report your findings to the examiner.

## Conclusion

An extension to this station may be to assess the rotator cuff muscles individually, you should therefore be aware how to do this.