```metadata
{
  "title":"Nasogastric Tube (NGT) Insertion",
  "description":"Nasogastric (NG) tubes may be used for feeding or for drainage – read your instructions thoroughly as this will dictate the type of tube you need to use. Essentially you are inserting a tube from the patients nose into their stomach.",
  "last_updated":"2018-05-26"
}
```
# Nasogastric Tube (NGT) Insertion

## Foreword

Nasogastric (NG) tubes may be used for feeding or for drainage – read your instructions thoroughly as this will dictate the type of tube you need to use. Essentially you are inserting a tube from the patients nose into their stomach.

There are several absolute contraindications for insertion so you should be aware of these. For full guidance, visit the [NPSA](http://www.nrls.npsa.nhs.uk/EasySiteWeb/getresource.axd?AssetID=129697&) website.

Most medical schools now have mannequins to practice this on, although it's not frequently examined upon. However, it is very useful skill to know.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain the procedure to the patient and gain their consent to proceed.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

Gather your equipment:

* Gloves.
* Fine bore nasogastric tube (feeding only) or nasogastric “Ryles” tube 16Fr (all other indications).
* Water based lubricant.
* Syringe.
* Bile bag.
* Securing device or tape.
* Cup of water.
* pH indicator paper.
* Anaesthetic throat spray*

*\*As there is limited evidence to suggest that a local anaesthetic spray may reduce the gag reflex during NG tube insertion, it is not used by all health boards. You should check your local guidelines for whether this is recommended or not. If so use it prior to Step 5.*

### Step 03

For this procedure the patient should be positioned on the bed upright and facing forward.  Put on your gloves.

### Step 04

Estimate the length of the tube to be inserted.  Do this by measuring the nasogastric tube from the tip of the nose, to the earlobe and then to the [xiphisternum](http://en.wikipedia.org/wiki/Xiphoid_process).

![Xiphisternum position](../../../../../../media/learning/osce-skills/img/xiphoid-process-frontal.jpg)

### Step 05

Lubricate the tip of the tube and begin to insert through one of the nostrils. If any resistance is encountered change to the other nostril.

Your patient will usually be awake during this procedure so ensure they are not experiencing too much discomfort.

### Step 06

Ask the patient to indicate when the tube is at the back of the throat, or if they have had anaesthetic spray ask the patient to open their mouth and look for the end of the tube.

### Step 07

Ask the patient to take a mouthful of water and as they swallow advance the tube to the desired length.

### Step 08

Aspirate from the tube using a syringe. Test the aspirate using pH indicator paper. The pH should be between 1 and 5.5.

Ensure you document the result in the patient’s notes.

### Step 09

If satisfied that the pH is correct, and the tube is draining gastric fluid, secure the tube with tape and attach a bile bag to allow drainage.

### Step 10

Remove gloves and dispose in a clinical waste bin.

Ensure your patient is comfortable, thank them and wash hands.

## Conclusion

At the end of this skill you should indicate to the examiner that you would now arrange a chest x-ray to ensure correct positioning of the nasogastric tube.