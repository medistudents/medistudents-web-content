```metadata
{
  "title":"Rectal (PR) Examination",
  "description":"Rectal (PR) examinations are performed for a number of clinical reasons e.g. altered bowel habit, rectal bleeding, urinary symptoms and is a skill surgeons perform on all patients",
  "last_updated":"2018-05-26"
}
```
# Rectal (PR) Examination

## Foreword

Rectal (PR) examinations are performed for a number of clinical reasons, such as altered bowel habit, rectal bleeding, or urinary symptoms. It is a skill surgeons perform on all patients and as such it is commonly examined as it is an important skill to know. 

For the purpose of examinations you will be provided with a mannequin, however you should pretend it is a real patient and talk to it as such as this forms part of the marking scheme.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain what you would like to do and obtain consent.  This is a slightly uncomfortable procedure so you should warn the patient of this.

**A chaperone is required for this examination.**

### Step 02

Ensure you have all of the necessary equipment for the station:

* Gloves.
* Lubricant.
* Tissues.

![Rectal Kit](../../../../../../media/learning/osce-skills/img/1rectalkit.jpg)

### Step 03

Positioning of the patient in this procedure is very important. Ask them to lie on their left hand side with their knees drawn up towards their chest, their feet pushed forward and their anus exposed.

### Step 04

Having washed your hands and put on your gloves, separate the buttocks and inspect the area around the anus. Look for any abnormalities including skin tags, [haemorrhoids](http://en.wikipedia.org/wiki/Haemorrhoids) and fissures.

![External inspection](../../../../../../media/learning/osce-skills/img/2rectalinspection.jpg)

### Step 05

After inspecting, lubricate your right index finger.

![Lubricate your right index finger](../../../../../../media/learning/osce-skills/img/3rectallubrication.jpg)

### Step 06

Tell the patient you are about to start the procedure. Place your finger on the anus so that it points anteriorly and apply pressure to the midline of the anus.

![Apply pressure](../../../../../../media/learning/osce-skills/img/4rectalpressure.jpg)

### Step 07

Maintain the pressure so that your finger enters the rectum. Initially you need to assess anal tone by asking the patient to squeeze your finger.

![Maintain the pressure to insert](../../../../../../media/learning/osce-skills/img/5rectalinsert.jpg)

### Step 08

Systematically examine each part of the rectum by sweeping the finger both clockwise and anti-clockwise around the entire circumference. You should be feeling for any abnormalities such as impacted faeces, masses or ulcers.

![Sweep clockwise](../../../../../../media/learning/osce-skills/img/6-1rectalright.jpg)

![Sweep anti-clockwise](../../../../../../media/learning/osce-skills/img/6-2rectalleft.jpg)

### Step 09

One of the main reasons for performing a rectal examination in males is to assess the [prostate gland](http://en.wikipedia.org/wiki/Prostate_gland). This lies anteriorly and should always be felt. You should check the size, consistency and presence of the midline groove.

### Step 10

Remove your finger and examine the glove for the colour of any faeces as well as the presence of any mucus or blood.

![Examine the glove](../../../../../../media/learning/osce-skills/img/7rectallook.jpg)

### Step 11

Clean off any lubricant left around the anus and remove and dispose of your gloves in the clinical waste bin.

![Clean off any lubricant](../../../../../../media/learning/osce-skills/img/8rectalclean.jpg)

### Step 12

Allow the patient to dress and thank them. Wash your hands and report your findings to the examiner.