```metadata
{
  "title":"Respiratory Examination",
  "description":"The respiratory examination aims to pick up on any respiratory (breathing) pathology that may be causing a patient’s symptoms e.g. shortness of breath, coughing, wheezing",
  "last_updated":"2018-05-26"
}
```
# Respiratory Examination

## Foreword

This is essentially an examination of the patient's lungs; however it is a complex examination which also includes examination of other parts of the body including the hands, face and neck.

The respiratory examination aims to pick up on any respiratory (breathing) pathology that may be causing a patient’s symptoms, such as shortness of breath, coughing, and wheezing.  Common conditions include chest infections, asthma and [chronic obstructive pulmonary disease (COPD)](https://en.wikipedia.org/wiki/Chronic_obstructive_pulmonary_disease).  This examination is performed on every patient that is admitted to hospital and regularly in clinics and general practice.

Like most major examination stations this follows the usual procedure of inspect, palpate, percuss, auscultate (look, feel, tap, listen).  It is an essential skill to master and is often examined in OSCEs.

## Procedure Steps

### Step 01

Begin by washing your hands, introduce yourself and clarify the patient’s identity.  Explain what you would like to do and gain the patient’s consent.

Offer a chaperone for this examination

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 02

The patient should be sitting up and exposed from the waist up. Make a general observation of the patient. Check whether they are comfortable at rest, do they look [tachypnoeic](http://en.wikipedia.org/wiki/Tachypnea), are they using accessory muscles, are there any obvious abnormalities of the chest. Also check for any clues around the bed such as inhalers, oxygen masks, or cigarettes.

![Observe the patient from the end of the bed](../../../../../../media/learning/osce-skills/img/observe-the-patient-from-the-end-of-the-bed.jpg)

### Step 03

Move to the hands. Hot, pink peripheries may be a sign of carbon dioxide retention. Look for any signs of clubbing or nicotine staining. Ask the patient to extend their arms and cock their wrists to 90 degrees. Observe the hands in this position for 30 seconds; a coarse flap may also be a sign of carbon dioxide retention.

![Inspect the patient's hands](../../../../../../media/learning/osce-skills/img/inspect-the-patients-hands1.jpg)

![Look for CO2 flap](../../../../../../media/learning/osce-skills/img/look-for-co2-flap.jpg)

### Step 04

At the wrist, take the patient’s pulse. A bounding pulse may indicate carbon dioxide retention. After you have taken the pulse it is advisable to keep your hands in the same position and subtly count the patient’s respiration rate. This helps to keep it as natural as possible.

![Take the radial pulse](../../../../../../media/learning/osce-skills/img/take-the-radial-pulse.jpg)

### Step 05

Move up to the face. Ask the patient to stick out their tongue and note its colour checking for anaemia or central cyanosis. Remember to ask them to raise their tongue up and check underneath.

![Inspect the mouth and tongue](../../../../../../media/learning/osce-skills/img/look-in-the-mouth-for-any-signs-of-anaemia.jpg)

### Step 06

Look for any use of accessory muscles such as the [sternocleidomastoid muscle](http://en.wikipedia.org/wiki/Sternocleidomastoid_muscle). Also palpate for the left supraclavicular node ([Virchow’s Node](http://en.wikipedia.org/wiki/Virchow%27s_Node)).  This drains the thoracic duct so an enlarged node (Troisier’s Sign) may suggest metastatic cancer e.g. lung or abdominal.

![Palpate for the left supraclavicular node](../../../../../../media/learning/osce-skills/img/palpate-for-the-left-supraclavicular-node.jpg)

### Step 07

The examination now moves onto the chest. Take time to observe the chest looking for any abnormalities such as changes in rib cage shape, or scars. Remember these may be in the axillae or on the back.

### Step 08

Now palpate the chest. Firstly feel between the heads of the two clavicles for the trachea. If it is deviated, it may suggest a tumour or [pneumothorax](http://en.wikipedia.org/wiki/Pneumothorax).

![Palpate the trachea](../../../../../../media/learning/osce-skills/img/feel-between-the-heads-of-the-two-clavicles-for-the-trachea.jpg)

### Step 09

Feel for chest expansion. Place your hands firmly on the chest wall with your thumbs meeting in the midline. Ask the patient to take a deep breath in and note the distance your thumbs move apart. Normally this should be at least 5 centimetres. You should measure this at the top and bottom of the lungs as well as on the back.

![Assess chest expansion](../../../../../../media/learning/osce-skills/img/observe-the-chest.jpg)

### Step 10

Perform percussion on both sides, comparing similar areas on both sides. You should start by tapping on the [clavicle](http://en.wikipedia.org/wiki/Clavicle) which gives an indication of the resonance in the apex. Then percuss normally for the entire lung fields. Hyper-resonance may suggest a collapsed lung where as hypo-resonance or dullness suggests consolidation such as in infection, effusion or a tumour. Be sure to perform this on the back as well.

![Percuss the lung fields](../../../../../../media/learning/osce-skills/img/percuss-normally-for-the-entire-lung-fields.jpg)

### Step 11

Check for tactile vocal fremitus. Place the medial edge of your hand on the chest and ask the patient to say “99” or “blue balloon”. Do this with your hand in the upper, middle and lower areas of both lungs. This again gives a suggestion of the constitution of the tissue deep to your hand.

![Check for tactile vocal fremitus](../../../../../../media/learning/osce-skills/img/check-for-tactile-vocal-fremitus.jpg)

### Step 12

Finally, auscultate. Do this in all areas of both lungs and on front and back comparing the sides to each other. Listen for any reduced breathe sounds, or added sounds such as crackles, wheeze, pleural rub or rhonchi.

![Auscultate left lung](../../../../../../media/learning/osce-skills/img/auscultate-left-lung.jpg)

![Auscultate right lung](../../../../../../media/learning/osce-skills/img/auscultate-right-lung.jpg)

### Step 13

Whilst using the stethoscope, ask the patient to again say “99” or “blue balloon” whilst listening in all areas – this is a more reliable test than the one described earlier.

### Step 14

Finish by examining the [lymph nodes](http://en.wikipedia.org/wiki/Lymph_nodes) in the head and neck. Start under the chin with the submental nodes, move along to the submandibular then to the back of the head at the occipital nodes. Next palpate the pre- and post- auricular nodes. Move down the cervical chain and onto the [supraclavicular nodes](https://en.wikipedia.org/wiki/Supraclavicular_lymph_nodes).

![Palpate the submental lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-submental-lymph-node.jpg)

![Palpate the submandibular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-submandibular-lymph-node.jpg)

![Palpate the occipital lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-occipital-lymph-node.jpg)

![Palpate the pre-auricular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-pre-auricular-lymph-node.jpg)

![Palpate the post-auricular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-post-auricular-lymph-node.jpg)

![Palpate the cervical lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-cervical-lymph-node.jpg)

### Step 15

Thank your patient and allow them to dress.  Wash your hands and report your findings to your examiner.

## Conclusion

**Tactile Vocal Fremitus**

UK students are still typically taught to use the phrase “99” to check for [tactile vocal fremitus](https://en.wikipedia.org/wiki/Fremitus). There are however thoughts that phrases such as “toy boat” or “blue balloon” which may be more appropriate.