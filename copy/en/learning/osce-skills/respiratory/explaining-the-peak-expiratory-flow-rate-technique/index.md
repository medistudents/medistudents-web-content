```metadata
{
  "title":"Peak Expiratory Flow Rate (PEFR) Technique",
  "description":"Peak expiratory flow rate is the measurement of how much the patient can blow out of their lungs in one breath. It is a crude measurement and not as accurate as Spirometry",
  "last_updated":"2018-05-26"
}
```
# Peak Expiratory Flow Rate (PEFR) Technique

## Foreword

Peak expiratory flow rate is the measurement of how much the patient can blow out of their lungs in one breath. It is a crude measurement and not as accurate as [Spirometry](http://en.wikipedia.org/wiki/Spirometry). However it is useful for patients to perform themselves, especially when they are having a flare up of their respiratory disease such as asthma.

There are *[UK guidelines](http://www.peakflow.com/top_nav/normal_values/PEFNorms.html)* for the “normal” values which you can use to compare against the values your patient is able to achieve.

This station therefore tests several examination skills, firstly your knowledge of the underlying condition being monitored, how to use a peak flow meter and your ability to communicate both of these areas to the patient.

## Procedure Steps

### Step 01

It is benefitial to check the patient’s understanding of their condition. If they do not fully understand then you should explain what is happening and that when they have an exacerbation they will find breathing more difficult. Furthermore, you should explain why measuring their PEFR is important as a guide to how well-controlled their asthma is at this time.

### Step 02

Explain to the patient that they should be checking their PEFR regularly, particularly if their asthma is worse than usual.

### Step 03

Explain the different steps in PEFR measurement to the patient. These are:

1. Connect a clean mouthpiece.
2. Ensure the marker is set to zero.
3. Stand up or sit upright.
4. Take as deep a breath in as you can and hold it.
5. Place the mouthpiece in your mouth and form as tight a seal as possible around it with your lips.
6. Breathe out as hard as you can.
7. Observe and record the reading.
8. Repeat the process 3-4 times and record the highest reading.
9. Note down the reading in a diary to allow comparison with readings on other days.

![Connect a clean mouthpiece](../../../../../../media/learning/osce-skills/img/connect-a-clean-mouthpiece.jpg)

![Ensure the marker is set to zero](../../../../../../media/learning/osce-skills/img/ensure-the-marker-is-set-to-zero.jpg)

![Breathe out as hard as you can](../../../../../../media/learning/osce-skills/img/breathe-out-as-hard-as-you-can.jpg)

![Observe and record the reading](../../../../../../media/learning/osce-skills/img/observe-and-record-the-reading.jpg)

### Step 04

Once you have discussed the process with the patient, show the patient how to perform the measurement by measuring your own PEFR.

### Step 05

Once the technique has been demonstrated, ask the patient to show you how they would perform the measurement themselves. Make sure they are doing it correctly, and resolve any mistakes which they might be making.

### Step 06

Finish by asking the patient if they have any questions or concerns about either their asthma or taking their PEFR measurement.