```metadata
{
  "title":"The Inhaler Technique",
  "description":"Patients with respiratory disease (breathing problems) such as asthma or chronic obstructive pulmonary disease (COPD) often require medication in the form of inhalers",
  "last_updated":"2018-05-26"
}
```
# The Inhaler Technique

## Foreword

Patients with respiratory disease (breathing problems) such as [asthma](https://en.wikipedia.org/wiki/Asthma) or [chronic obstructive pulmonary disease (COPD)](https://en.wikipedia.org/wiki/Chronic_obstructive_pulmonary_disease) often require medication in the form of inhalers.  Basically they need to inhale, hence the term inhalers, the medication via their mouth into their lungs.

This station therefore tests several examination skills:

1. Your knowledge of the underlying condition that they require the medication for.
2. How to use an inhaler.
3. Your ability to communicate both of these areas to the patient.

There are several types of inhalers available, such as: aerosol inhalation or breath activated. You should be familiar with how to use each of these.  In this station we will demonstrate the use of *[aerosol inhalers](https://en.wikipedia.org/wiki/Inhaler)*.

## Procedure Steps

### Step 01

Introduce yourself to the patient and clarify their identity.

Initially it is worth checking the patient’s understanding of their condition. If they do not understand fully, explain what is happening and why they find breathing more difficult when they have an exacerbation.

### Step 02

Discuss the inhaler with the patient; inform them that it contains medication and explain to them how it works.

![Types of inhaler](../../../../../../media/learning/osce-skills/img/types-of-inhaler.jpg)

### Step 03

Describe the following steps for using the inhaler:

1. Remove the inhaler cap.
2. Shake the inhaler.
3. Hold the inhaler upright with your index finger on the top.
4. Hold the inhaler close to your mouth and breathe out completely.
5. Put the mouthpiece in your mouth, breathe in deeply and firmly press on the canister simultaneously.
6. Inhale fully so that the dose goes deep into the lungs.
7. Hold your breath for 10 seconds.
8. Breathe out and, if necessary, repeat the procedure.

Once you have described the steps, show the patient how to do it yourself.

![Remove the inhaler cap](../../../../../../media/learning/osce-skills/img/remove-the-inhaler-cap.jpg)

![Position the inhaler](../../../../../../media/learning/osce-skills/img/position-the-inhaler.jpg)

![Inhale fully](../../../../../../media/learning/osce-skills/img/inhale-fully.jpg)

### Step 04

After you have demonstrated the technique, ask the patient to show you how they would do it. Check that they are doing it correctly and resolve any mistakes they are making.

### Step 05

End the consultation by asking the patient if they have any questions or concerns about the process.