```metadata
{
  "title":"Reticulo-Endothelial Examination",
  "description":"Reticulo-Endothelial is medical term for examining the lymph nodes, liver and spleen",
  "last_updated":"2018-05-26"
}
```
# Reticulo-Endothelial Examination

## Foreword

Reticulo-Endothelial is medical term for *examining the  [lymph nodes](http://en.wikipedia.org/wiki/Lymph_nodes), liver and spleen*.

Conditions which cause enlarged lymph nodes or hepatosplenomegaly (enlarged liver and spleen) are usually the [hematological malignancies](https://en.wikipedia.org/wiki/Tumors_of_the_hematopoietic_and_lymphoid_tissues) such as lymphoid and myeloid leukemias.

Whilst the name of the station may sound daunting, it is actually a fairly basic station, and one in which you should aim to achieve good marks.

## Procedure Steps

### Step 01

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain what you would like to do and obtain consent. The patient should be exposed from the waist up for this examination, therefore you may wish to offer a chaperone.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

![Introduce yourself to the patient](../../../../../../media/learning/osce-skills/img/introduce-yourself-to-the-patient1.jpg)

### Step 02

As you have been given a fairly directed instruction, there is little observation to be done. As the station however revolves around examination of parts of the immune system, it may look good to comment on the patient’s general appearance and well-being initially.

![Perform a general inspection](../../../../../../media/learning/osce-skills/img/perform-a-general-inspection.jpg)

### Step 03

Begin with an examination of the [lymph nodes](http://en.wikipedia.org/wiki/Lymph_nodes) which are arranged in various groups. This should be done with the patient sitting and you standing behind them. Feel both sides together for comparison.

Start under the chin to feel the submental node, then move along the lower jaw line to feel the submandibular nodes. Then feel along the anterior border of the [sternocleidomastoid muscle](http://en.wikipedia.org/wiki/Sternocleidomastoid_muscle) for the anterior chain and around to the posterior border for the posterior chain. Feel at the occiput for the occipital nodes and then behind and in front of the ear for the post- and pre-auricular nodes.

![Palpate the submental lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-submental-lymph-node.jpg)

![Palpate the submandibular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-submandibular-lymph-node.jpg)

![Palpate the occipital lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-occipital-lymph-node.jpg)

![Palpate the pre-auricular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-pre-auricular-lymph-node.jpg)

![Palpate the post-auricular lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-post-auricular-lymph-node.jpg)

![Palpate the cervical lymph nodes](../../../../../../media/learning/osce-skills/img/palpate-the-cervical-lymph-node.jpg)

### Step 04

Feel above and below the clavicle for the supra- and infra-clavicular nodes, and in the axilla for the axillary nodes. For the axillary nodes it is best to have the patient lying down with the back of the bed raised to 30 – 45 degrees. When examining the right axilla take the weight of their right arm with your right arm and examine with your left hand. You may wish to put gloves on for this inspection.

### Step 05

If you notice any enlarged nodes, nodes with strange consistency, or fixed or tender nodes; you should report this to the examiner.  Typically rubbery, non-tender nodes tend to be suspicious.  You should mention to the examiner at this point you would also like to check the inguinal lymph nodes for completion, although you usually won’t be expected to perform this.

### Step 06

Next you should move onto examine the liver and spleen. Palpation for the liver and spleen is similar, both starting in the right [iliac fossa](https://en.wikipedia.org/wiki/Iliac_fossa).

For the liver, press upwards towards the [right hypochondrium](http://en.wikipedia.org/wiki/Hypochondrium). You should try to time the palpation with the patient breathing in as this presses down on the liver. If nothing is felt you should move towards the costal margin and try again. A distended liver feels like a light tap on the leading finger when you press down. If the liver is distended, its distance from the costal margin should be noted.

![Palpate for the liver](../../../../../../media/learning/osce-skills/img/feel-for-organomegaly.jpg)

![Front of abdomen](../../../../../../media/learning/osce-skills/img/gray1223.png)

### Step 07

Palpating for the spleen is as for the liver but in the direction of the [left hypochondrium](https://en.wikipedia.org/wiki/Hypochondrium). The edge of the spleen which may be felt if distended, is more nodular than the liver. Another way to assess for splenomegaly is to ask the patient to lie on their right side. Support the rib cage with your left hand and again ask the patient to take deep breaths in moving your right hand up towards the left hypochondrium.

![Palpating for the spleen](../../../../../../media/learning/osce-skills/img/palpating-for-the-spleen.jpg)

### Step 08

Thank your patient and allow them to dress. Report any findings to your examiner.