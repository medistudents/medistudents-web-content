```metadata
{
  "title":"Upper Limb Neurological Examination",
  "description":"The upper limb examination is another skill to elicit neurological signs i.e. nerve problems that supply the arms and hands. Patients may present with a number of complaints including altered sensation",
  "last_updated":"2017-09-17"
}
```
# Upper Limb Neurological Examination

## Foreword

The upper limb examination is another skill to elicit neurological signs, such as nerve problems that supply the arms and hands. Patients may present with a number of complaints including altered sensation, for example: pins and needles or numbness or loss of power of a limb, it may be intermittent such as [multiple sclerosis](http://en.wikipedia.org/wiki/Multiple_sclerosis) or permanent such as in [motor neurone disease](http://en.wikipedia.org/wiki/MND). 

Neuropathies can also occur and can be mono such as [wrist drop](http://en.wikipedia.org/wiki/Wrist_drop) or [Erb’s palsy](http://en.wikipedia.org/wiki/Erb%27s_palsy) or poly such as “[glove and stocking](http://en.wikipedia.org/wiki/Glove_and_stocking_anesthesia)” which can occur in [diabetes mellitus](https://en.wikipedia.org/wiki/Diabetes_mellitus).

A full neurological examination therefore includes assessment of both the motor and sensory systems of the legs. In exams you may be asked to focus on one part, such as the sensory or the motor. For reference, this guide, will include both.

See [Lower Limb Neurological Examination](https://www.medistudents.com/en/learning/osce-skills/neurology/lower-limb-neurological-examination/) for how to perform this on the lower limbs.

## Procedure Steps


### Step 01

As with all examinations, the best method is your own – one with which you are comfortable and familiar. The one explained here takes the following format:

* Tone.
* Power.
* Reflexes.
* Function.
* Sensation.

### Step 02

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain the procedure you about to perform and and obtain consent.

### Step 03

The upper body should be exposed for this examination. Initially, observe the patient’s arms, look for any muscle wasting, fasciculations or asymmetry.

![Observe the patients arms, look for any muscle wasting, fasciculations or asymmetry](../../../../../../media/learning/osce-skills/img/observe-the-patients-arms-look-for-any-muscle-wasting-fasciculations-or-asymmetry.jpg)


### Step 04 - Tone

Start proximally at the shoulder, feeling how easy the joint is to move passively. Then move down to the elbow, wrist and hand joints, again assessing each one’s tone in turn.

![Assess arm tone](../../../../../../media/learning/osce-skills/img/assess-arm-tone.jpg)

![Assess elbow, wrist and hand joints](../../../../../../media/learning/osce-skills/img/assess-elbow-wrist-and-hand-joints.jpg)

### Step 05 - Power

Start at the shoulder asking the patient to abduct and adduct against your hand so you can assess how much force they can overcome. Do the same for flexion and extension at the elbow and wrist, as well as the fingers; also checking abduction and adduction of the thumb.

![Assess power of the shoulders 1](../../../../../../media/learning/osce-skills/img/assess-power-of-the-shoulders-1.jpg)

![Assess power of the shoulders 2](../../../../../../media/learning/osce-skills/img/assess-power-of-the-shoulders-2.jpg)

![Elbow flexion](../../../../../../media/learning/osce-skills/img/elbow-flexion.jpg)

![Elbow extension](../../../../../../media/learning/osce-skills/img/elbow-extension.jpg)

![Wrist flexion](../../../../../../media/learning/osce-skills/img/wrist-flexion1.jpg)

![Wrist extension](../../../../../../media/learning/osce-skills/img/wrist-extension1.jpg)

![Finger flexion](../../../../../../media/learning/osce-skills/img/finger-flexion1.jpg)

![Finger extension](../../../../../../media/learning/osce-skills/img/finger-extension1.jpg)

![Finger abduction](../../../../../../media/learning/osce-skills/img/finger-abduction1.jpg)

![Abduction of the thumb](../../../../../../media/learning/osce-skills/img/abduction-of-the-thumb.jpg)

### Step 06 - Reflexes

There are three reflexes in the upper limb:

1. [The biceps reflex](https://en.wikipedia.org/wiki/Biceps_reflex)
2. [The triceps reflex](https://en.wikipedia.org/wiki/Triceps_reflex)
3. [The supinator reflexes](https://en.wikipedia.org/wiki/Brachioradialis_reflex)

#### Biceps Reflex

The [biceps reflex](https://en.wikipedia.org/wiki/Biceps_reflex) is tested by supporting the patient’s arm, with it flexed at roughly 60 degrees, placing your thumb over the biceps tendon and hitting your thumb with the tendon hammer. It is vital to get your patient to relax as much as possible and for you to take the entire weight of their arm.

![Bicep reflex test right](../../../../../../media/learning/osce-skills/img/bicep-reflex-test-right.jpg)

![Bicep reflex test left](../../../../../../media/learning/osce-skills/img/bicep-reflex-test-left.jpg)

#### Triceps Reflex

The [triceps reflex](https://en.wikipedia.org/wiki/Triceps_reflex) is elicited by resting the patient’s arm across their chest and hitting the triceps tendon just proximal to the elbow.

![Triceps reflex test right](../../../../../../media/learning/osce-skills/img/triceps-reflex-test-right.jpg)

![Triceps reflex test left](../../../../../../media/learning/osce-skills/img/triceps-reflex-test-left.jpg)

#### Supinator Reflex

Finally, with their arm rested on their abdomen, locate the supinator tendon as it crosses the radius, place three fingers on it and hit the fingers. This should give the [supinator reflex](https://en.wikipedia.org/wiki/Brachioradialis_reflex). If you struggle with any of these reflexes, asking the patient to clench their teeth should exaggerate the reflex.

![Supinator reflex test right](../../../../../../media/learning/osce-skills/img/supinator-reflex-test-right.jpg)

![Supinator reflex test left](../../../../../../media/learning/osce-skills/img/supinator-reflex-test-left.jpg)

### Step 10 - Function

To assess the upper limb, you should ask the patient to touch their head with both hands and then ask them to pick up a small object such as a coin which each hand.

### Step 11 - Sensation

You should test light touch, pin prick, vibration, and joint position sense, or [proprioception](http://en.wikipedia.org/wiki/Proprioception).

#### Light Touch

Ask the patient to place their arms by their sides with their palms facing forwards. Lightly touch the patient’s sternum with a piece of cotton wool so that they know how it feels.

Then, with the patient’s eyes shut, lightly touch their arm with the cotton wool. The places to touch them should test each of the [dermatomes](http://en.wikipedia.org/wiki/Dermatomes) – make sure you know these! Tell the patient to say “yes” every time they feel the cotton wool as it felt before.

![Touch sensation with cotton wool](../../../../../../media/learning/osce-skills/img/touch-sensation-with-cotton-wool.jpg)

#### Pin Prick

Repeat the last step using a light pin prick.

![Touch sensation with pin prick](../../../../../../media/learning/osce-skills/img/touch-sensation-with-pin-prick.jpg)


#### Vibration

To assess vibration you should use a sounding tuning fork. Place the fork on the patient’s sternum to show them how it should feel.

Now place it on the bony prominence at the base of their thumb and ask them if it feels the same. If it does, there is no need to check any higher. If it feels different you should move to the radial stylus and then to the [olecranon](http://en.wikipedia.org/wiki/Olecranon) until it feels normal.

![Vibration test using a sounding tuning fork](../../../../../../media/learning/osce-skills/img/vibration-test-using-a-sounding-tuning-fork.jpg)

#### Proprioception

Finally assess [proprioception](http://en.wikipedia.org/wiki/Proprioception). Hold the [distal phalanx](https://en.wikipedia.org/wiki/Phalanx_bone) of the thumb on either side so that you can flex the interphalangeal joint.

Show the patient that when you hold the joint extended, that represents *Up* whereas when you hold it flexed that represents *Down*. Ask the patient to close their eyes and, having moved the joint a few times hold it in one position – up or down. Ask the patient which position the joint is in.

![Thumb position up](../../../../../../media/learning/osce-skills/img/thumb-position-up.jpg)

![Thumb position down](../../../../../../media/learning/osce-skills/img/thumb-position-down.jpg)

## Conclusion

Allow the patient to dress and thank them. 

Wash your hands and report your findings to the examiner.