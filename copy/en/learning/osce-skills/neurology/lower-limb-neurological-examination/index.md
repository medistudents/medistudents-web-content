```metadata
{
  "title":"Lower Limb Neurological Examination",
  "description":"The lower limb examination is another skill to elicit neurological signs i.e nerve problems that supply the legs and feet",
  "last_updated":"2017-09-17"
}
```
# Lower Limb Neurological Examination

## Foreword

The lower limb examination is a skill to elicit neurological signs, such as nerve problems that supply the legs and feet.  Patients may present with a number of complaints including altered sensation, for example: pins and needles or numbness or loss of power of a limb. It may be intermittent such as [multiple sclerosis](http://en.wikipedia.org/wiki/Multiple_sclerosis) or permanent such as in [motor neurone disease](http://en.wikipedia.org/wiki/MND). 

[Neuropathies](https://en.wikipedia.org/wiki/Peripheral_neuropathy) can also occur and can be mono such as [foot drop](http://en.wikipedia.org/wiki/Foot_drop) or poly such as “[glove and stocking](http://en.wikipedia.org/wiki/Glove_and_stocking_anesthesia)” which can occur in [diabetes mellitus](https://en.wikipedia.org/wiki/Diabetes_mellitus).

A full neurological examination therefore includes assessment of both the motor and sensory systems of the legs. In exams you may be asked to focus on one part, such as the sensory or the motor. For reference, this guide, will include both.

See [Upper Limb Neurological Examination](https://www.medistudents.com/en/learning/osce-skills/neurology/upper-limb-neurological-examination/) for how to perform this on the upper limbs.

## Procedure Steps

### Step 01

As with all examinations, the best method is your own – one with which you are comfortable and familiar. The one explained here takes the following format:

* Tone.
* Power.
* Reflexes.
* Function.
* Sensation.

### Step 02

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain the procedure you about to perform and and obtain consent.

### Step 03

Ideally the patient should have their lower body exposed, although for the purpose of the exam the patient will likely be in shorts.  Begin by observing the patients legs, looking for any muscle wasting, fasciculations or asymmetry.

### Step 04 - Tone

Roll the leg on the bed to see if it moves easily, then pull up on the knee to check its tone. Check for [ankle clonus](http://en.wikipedia.org/wiki/Clonus) by placing the patients leg turned outwards on the bed, moving the ankle joint a few times to relax it and then sharply dorsiflexing it. Any further movement of the joint may suggest clonus.

![Assess leg tone](../../../../../../media/learning/osce-skills/img/lower-limb-examine-tone-of-the-muscles-1.jpg)

![Assess knee tone](../../../../../../media/learning/osce-skills/img/lower-limb-examine-tone-of-the-muscles-2.jpg)

![Check for ankle clonus](../../../../../../media/learning/osce-skills/img/lower-limb-examine-tone-of-the-muscles-3.jpg)

### Step 05 - Power

Start at the hip asking the patient to abduct, adduct, flex and extend against your hand so you can assess how much force they can overcome. Do the same for flexion and extension at the knee and ankle as well as the toes.

![Assess hip flexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-1.jpg)

![Assess hip extension](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-2.jpg)

![Assess knee flexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-3.jpg)

![Assess knee extension](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-4.jpg)

![Dorsiflexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-5.jpg)

![Plantarflexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-6.jpg)

![Toe dorsiflexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-7.jpg)

![Toe plantarflexion](../../../../../../media/learning/osce-skills/img/lower-limb-assess-the-power-of-each-of-the-muscle-groups-8.jpg)

### Step 06 - Reflexes

There are three reflexes in the lower limb:

1. [Patellar reflex](http://en.wikipedia.org/wiki/Patellar_reflex)
2. [Ankle jerk reflex](http://en.wikipedia.org/wiki/Ankle_jerk_reflex)
3. [Plantar reflex](http://en.wikipedia.org/wiki/Plantar_reflex) (elicited by stroking up the lateral aspect of the plantar surface)

#### Patellar Reflex

The patellar reflex is tested by placing the patient’s leg flexed at roughly 60 degrees, taking the entire weight of their leg with your arm, and hitting the patellar tendon with the tendon hammer. It is vital to get your patient to relax as much as possible and for you to take the entire weight of their leg.

![The patellar reflex test](../../../../../../media/learning/osce-skills/img/lower-limb-the-patellar-reflex-test.jpg)

#### Ankle Jerk

The ankle jerk is elicited by resting the patient’s leg on the bed with their hip laterally rotated. Pull the foot into dorsiflexion and hit the [calcaneal tendon](http://en.wikipedia.org/wiki/Calcaneal_tendon).

![The ankle jerk test](../../../../../../media/learning/osce-skills/img/lower-limb-the-ankle-jerk-test.jpg)

#### Plantar Reflex

Finally, with their leg out straight and resting on the bed, run the end of the handle of the tendon hammer along the outside of the foot. This gives the plantar reflex. An abnormal reflex would see the great toe extending.

If you struggle with any of these reflexes, asking the patient to clench their teeth should exaggerate the reflex.

### Step 08 - Function

For the lower limb you should assess the patient’s walking. Observe their gait and check for any abnormalities. Whilst they are standing you should perform [Romberg’s test](http://en.wikipedia.org/wiki/Romberg%27s_test). Ask the patient to stand with their feet apart and then close their eyes. Any swaying may be suggestive of a posterior column pathology.

![Assess patient walking](../../../../../../media/learning/osce-skills/img/lower-limb-lower-limb-function-examination-1.jpg)

![Romberg's test](../../../../../../media/learning/osce-skills/img/lower-limb-lower-limb-function-examination-2-e1318789087578.jpg)

### Step 09 - Sensation

You should test light touch, pin prick, vibration, and joint position sense, or [proprioception](http://en.wikipedia.org/wiki/Proprioception).

#### Light Touch

Ask the patient to place their legs out straight on the bed. Lightly touch the patient’s sternum with a piece of cotton wool so that they know how it feels.

With the patient’s eyes shut, lightly touch their leg with the cotton wool. The places to touch the patient should test each of the [dermatomes](http://en.wikipedia.org/wiki/Dermatomes) – make sure you know these! Tell the patient to say yes every time they feel the cotton wool as it felt before.

![Sensation test with cotton wool](../../../../../../media/learning/osce-skills/img/lower-limb-sensation-test-1.jpg)

#### Pin Prick

Repeat the last step using a light pin prick.

![Sensation test with a pin prick](../../../../../../media/learning/osce-skills/img/lower-limb-sensation-test-2.jpg)

#### Vibration

To assess vibration you should use a sounding tuning fork. Place the fork on the patient’s sternum to show them how it should feel.

Now place the tuning fork on their great toe and ask them if it feels the same. If it does, there is no need to check any higher. If it feels different you should move to the <em>tibial epicondyle</em> and then to the [greater trochanter](http://en.wikipedia.org/wiki/Greater_trochanter) until it feels normal.

![Vibration test](../../../../../../media/learning/osce-skills/img/lower-limb-vibration-test.jpg)

#### Proprioception

Finally assess [proprioception](http://en.wikipedia.org/wiki/Proprioception). Hold the [distal phalanx](https://en.wikipedia.org/wiki/Phalanx_bone) of the great toe on either side so that you can flex the interphalangeal joint.

Show the patient that when you hold the joint extended, that represents *Up* whereas when you hold it flexed that represents *Down*. Ask the patient to close their eyes and, having moved the joint a few times hold it in one position – up or down. Ask the patient which position the joint is in.

![Flex the interphalangeal joint up](../../../../../../media/learning/osce-skills/img/lower-limb-proprioception-1.jpg)

![Flex the interphalangeal joint down](../../../../../../media/learning/osce-skills/img/lower-limb-proprioception-2.jpg)

## Conclusion

Allow the patient to dress and thank them. 

Wash your hands and report your findings to the examiner.