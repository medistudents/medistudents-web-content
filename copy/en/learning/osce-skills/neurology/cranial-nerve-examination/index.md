```metadata
{
  "title":"Cranial Nerve Examination",
  "description":"There are 12 pairs of nerves that come from the brain, one for each side of the brain. One or more of the nerves can be affected depending on what is the cause",
  "last_updated":"2018-05-26"
}
```
# Cranial Nerve Examination

## Foreword

There are 12 pairs of nerves that come from the brain, one for each side of the brain. One or more of the nerves can be affected depending on what is the cause.  Common conditions include space occupying lesions (tumours or [aneurysm](https://en.wikipedia.org/wiki/Aneurysm)), [myasthenia gravis](http://en.wikipedia.org/wiki/Myasthenia_gravis) and [multiple sclerosis](http://en.wikipedia.org/wiki/Multiple_sclerosis), although there are many more.

For a detailed list visit [this site](http://www.patient.co.uk/doctor/cranial-nerve-lesions).

The cranial nerve examination involves a number of steps as you are testing all 12 of the nerves in one station. Be certain to know which nerve is being tested next and what tests you must perform for each specific nerve.

This guide will take you through each nerve systematically, but personal techniques may be adopted for this station so that it flows best for you.  It can seem like a daunting station as there are many steps to it, but hopefully this guide will help.

## Procedure Steps

### Step 01

The following equipment is required for a cranial nerve examination:

* Handwash.
* Item with distinct odour (e.g. orange/lemon peel, coffee, vinegar, etc).
* Cotton ball.
* Pen torch.
* Fundoscope.
* Tuning fork.
* Neurological reflex hammer.
* Snellen charts.
* Ishihara plates.

![Cranial Nerve Examination equipment](../../../../../../media/learning/osce-skills/img/cranial-nerve-examination-equipment.jpg)

![Typical Snellen chart to estimate visual acuity](../../../../../../media/learning/osce-skills/img/481px-snellen_chart.svg_.png)

![Example of an Ishihara color test plate.](../../../../../../media/learning/osce-skills/img/600px-ishihara_9.png)

### Step 02

Wash your hands, introduce yourself to the patient and clarify their identity.  Explain the procedure and obtain consent.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 03 - The Olfactory Nerve

**[The Olfactory nerve (CN I)](http://en.wikipedia.org/wiki/Olfactory_nerve)** is simply tested by offering something familiar for the patient to smell and identify, for example orange/lemon peel, coffee, or vinegar.

![Test the olfactory nerve](../../../../../../media/learning/osce-skills/img/offering-something-familiar-for-the-patient-to-smell-and-identify.jpg)

### Step 04 - The Optic Nerve

**[The Optic nerve](http://en.wikipedia.org/wiki/Optic_nerve)** is tested in five ways:

* Acuity
* Colour
* Fields
* Reflexes
* Fundoscopy

#### Visual Acuity

[Visual acuity](https://en.wikipedia.org/wiki/Visual_acuity) is tested using [Snellen charts](http://en.wikipedia.org/wiki/Snellen_chart). If the patient normally wears glasses or contact lenses, then this test should be assessed both with and without their vision aids.

![Typical Snellen chart to estimate visual acuity](../../../../../../media/learning/osce-skills/img/481px-snellen_chart.svg_.png)

#### Colour

Colour vision is tested using [Ishihara plates](http://en.wikipedia.org/wiki/Ishihara_plate) which identify patients who are colour blind.

![Example of an Ishihara color test plate.](../../../../../../media/learning/osce-skills/img/600px-ishihara_9.png)

#### Visual Field

[Visual field](https://en.wikipedia.org/wiki/Visual_field) is tested by asking the patient to look directly at you whilst you wiggle one of your fingers in each of the four quadrants. Ask the patient to identify which finger is moving.

#### Visual Inattention

Visual inattention can be tested by moving both fingers at the same time and checking the patient identifies this.

![Visual fields test in one pair of quadrants](../../../../../../media/learning/osce-skills/img/visual-fields-test-1.jpg)

![Visual fields test in the alternative pair of quadrants](../../../../../../media/learning/osce-skills/img/visual-fields-test-2.jpg)

#### Visual Reflexes

Visual reflexes comprise direct and concentric reflexes.

Place one hand vertically along the patients nose to block any light from entering the eye which is not being tested. Shine a pen torch into one eye and check that the pupils on both sides constrict. This should be tested on both sides.

![Shine a pen torch into the patient's eye](../../../../../../media/learning/osce-skills/img/shine-a-pen-torch-into-one-eye.jpg)

#### Fundoscopy

Finally [fundoscopy](http://en.wikipedia.org/wiki/Fundoscopy) should be performed on both eyes.

![Perform fundoscopy on both eyes](../../../../../../media/learning/osce-skills/img/perform-fundoscopy-on-both-eyes.jpg)

### Step 10 - Oculomotor Nerve (CN III), Trochlear Nerve, Abducent Nerve (CN VI)

The [Oculomotor nerve (CN III)](http://en.wikipedia.org/wiki/Oculomotor_nerve), [Trochlear nerve](http://en.wikipedia.org/wiki/Trochlear_nerve) and [Abducent Nerve (CN VI)](https://en.wikipedia.org/wiki/Abducens_nerve) are involved in movements of the eye.

Asking the patient to keep their head perfectly still directly in front of you, you should draw two large joining H’s in front of them using your finger and ask them to follow your finger with their eyes. It is important the patient does not move their head.

Always ask if the patient experiences any double vision, and if so, when is it worse?

![Get the patient to follow your finger](../../../../../../media/learning/osce-skills/img/oculomotor-examination.jpg)

### Step 11 - Trigeminal Nerve (CN V)

The [Trigeminal nerve (CN V)](http://en.wikipedia.org/wiki/Trigeminal_nerve) is involved in sensory supply to the face and motor supply to the muscles of mastication. There are 3 sensory branches of the trigeminal nerve: [ophthalmic](https://en.wikipedia.org/wiki/Ophthalmic_nerve), [maxillary](https://en.wikipedia.org/wiki/Maxillary_nerve) and [mandibular](https://en.wikipedia.org/wiki/Mandible#Nerves).

Initially test the sensory branches by lightly touching the face with a piece of cotton wool followed by a blunt pin in three places on each side of the face:

1. Around the jawline.
2. On the cheek and.
3. On the forehead.

The [corneal reflex](http://en.wikipedia.org/wiki/Corneal_reflex) should also be examined as the sensory supply to the cornea is from this nerve. Do this by lightly touching the cornea with the cotton wool. This should cause the patient to shut their eyelids.

![Opthalmic](../../../../../../media/learning/osce-skills/img/sensory-branch-test-on-the-forehead.jpg)

![Maxillary](../../../../../../media/learning/osce-skills/img/sensory-branch-test-on-the-cheek.jpg)

![Mandibular](../../../../../../media/learning/osce-skills/img/sensory-branch-test-on-the-jawline.jpg)

![Corneal reflex test](../../../../../../media/learning/osce-skills/img/corneal-reflex-test.jpg)

#### Motor Supply

To test the motor supply, ask the patient to clench their teeth together, observing and feeling the bulk of the [masseter](http://en.wikipedia.org/wiki/Masseter) and [temporalis](http://en.wikipedia.org/wiki/Temporalis) muscles.

Ask the patient to then open their mouth against resistance.

Finally perform the jaw jerk on the patient by placing your left index finger on their chin and striking it with a tendon hammer. This should cause slight protrusion of the jaw.

![Muscles of the head and neck](../../../../../../media/learning/osce-skills/img/illu_head_neck_muscle.jpg)

![Feeling the masseter muscles](../../../../../../media/learning/osce-skills/img/feeling-the-masseter-muscles.jpg)

![Feeling the temporalis muscles](../../../../../../media/learning/osce-skills/img/feeling-the-temporalis-muscles.jpg)

![The jaw jerk](../../../../../../media/learning/osce-skills/img/the-jaw-jerk.jpg)

### Step 13 - Abducens Nerve

As previously mentioned, the [abducens nerve](http://en.wikipedia.org/wiki/Abducens_nerve) is tested in the same manner as the oculomotor and trochlear nerves, again in eye movements.

### Step 14 - The Facial Nerve

The [facial nerve](http://en.wikipedia.org/wiki/Facial_nerve) supplies motor branches to the muscles of facial expression.

This nerve is therefore tested by asking the patient to crease up their forehead (raise their eyebrows), close their eyes and keep them closed against resistance, puff out their cheeks and reveal their teeth.

![Crease up the forehead](../../../../../../media/learning/osce-skills/img/crease-up-the-patients-forehead.jpg)

![Keep eyes closed against resistance](../../../../../../media/learning/osce-skills/img/keep-eyes-closed-against-resistance.jpg)

![Puff out the cheeks](../../../../../../media/learning/osce-skills/img/puff-out-the-patients-cheeks.jpg)

![Reveal the teeth](../../../../../../media/learning/osce-skills/img/reveal-the-patients-teeth.jpg)

### Step 15 - The Vestibulocochlear Nerve

The [vestibulocochlear nerve](http://en.wikipedia.org/wiki/Vestibulocochlear_nerve) provides innervation to the hearing apparatus of the ear and can be used to differentiate conductive and sensori-neural hearing loss using the [Rinne](http://en.wikipedia.org/wiki/Rinne_test) and [Weber](http://en.wikipedia.org/wiki/Weber_test) tests.

To carry out the Rinne test, place a sounding tuning fork on the patient’s mastoid process and then next to their ear and ask which is louder. A normal patient will find the second position louder.

To carry out the Weber’s test, place the tuning fork base down in the centre of the patient’s forehead and ask if it is louder in either ear. Normally it should be heard equally in both ears.

![Rinne test - place tuning fork on the mastoid process](../../../../../../media/learning/osce-skills/img/rinne-test-place-tuning-fork-on-the-mastoid-process.jpg)

![Rinne test - place tuning fork beside the ear](../../../../../../media/learning/osce-skills/img/rinne-test-place-tuning-fork-beside-the-ear.jpg)

![Webers test - place the tuning fork base down in the centre of the forehead](../../../../../../media/learning/osce-skills/img/webers-test-place-the-tuning-fork-base-down-in-the-centre-of-the-forehead.jpg)

### Step 16 - The Glossopharyngeal Nerve

The [glossopharyngeal nerve](http://en.wikipedia.org/wiki/Glossopharyngeal_nerve) provides sensory supply to the palate. It can be tested with the gag reflex or by touching the arches of the [pharynx](http://en.wikipedia.org/wiki/Pharynx).

![Glossopharyngeal nerve examination](../../../../../../media/learning/osce-skills/img/glossopharyngeal-nerve-examination.jpg)


### Step 17 - The Vagus Nerve

The [vagus nerve](http://en.wikipedia.org/wiki/Vagus_nerve) provides motor supply to the [pharynx](http://en.wikipedia.org/wiki/Pharynx).

Asking the patient to speak gives a good indication to the efficacy of the muscles. The uvula should be observed before and during the patient saying “aah”. Check that it lies centrally and does not deviate on movement.

### Step 18 - The Accessory Nerve

The [accessory nerve](http://en.wikipedia.org/wiki/Accessory_nerve) gives motor supply to the [sternocleidomastoid](http://en.wikipedia.org/wiki/Sternocleidomastoid) and [trapezius](http://en.wikipedia.org/wiki/Trapezius) muscles. To test it, ask the patient to shrug their shoulders and turn their head against resistance.

![Sternocldeiomastoid muscle test against resistance](../../../../../../media/learning/osce-skills/img/sternocldeiomastoid-muscle-test-against-resistance.jpg)

![Sternocleidomastoideus](../../../../../../media/learning/osce-skills/img/sternocleidomastoideus.png)

![Trapezius muscle test against resistance](../../../../../../media/learning/osce-skills/img/trapezius_muscle-test-against-resistance.jpg)

![Trapezius](../../../../../../media/learning/osce-skills/img/trapezius.png)

### The Hypoglossal Nerve

The [hypoglossal nerve](http://en.wikipedia.org/wiki/Hypoglossal_nerve) provides motor supply to the muscles of the tongue.

Observe the tongue for any signs of wasting or [fasciculations](http://en.wikipedia.org/wiki/Fasciculations). Ask the patient to stick their tongue out. If the tongue deviates to either side, it suggests a weakening of the muscles on that side.

![Hypoglossal nerve examination](../../../../../../media/learning/osce-skills/img/hypoglossal-nerve-examination.jpg)

## Conclusion

Thank your patient and wash your hands. Report any findings to your examiner.

---

This guide is designed for students and doctors. If you are applying for medical school and would like more information on the UCAT please check out our [complete guide](/en/ucat/ucat-2020-complete-guide) and our [guide on how to practice for your exam](/en/ucat/ucat-practice).





