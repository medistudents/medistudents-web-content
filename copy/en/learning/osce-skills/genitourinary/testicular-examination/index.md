```metadata
{
  "title":"Testicular Examination",
  "description":"A testicular examination is mainly performed on male patients who present with testicular pathology e.g. pain, swelling, a lump. Although titled testicular examination it involves the examination of the penis, scrotum and testes.",
  "last_updated":"2018-05-26"
}
```
# Testicular Examination

## Foreword

A testicular examination is mainly performed on male patients who present with testicular pathology e.g. pain, swelling, a lump.  Although titled testicular examination it involves the examination of the penis, scrotum and testes.

As this is an intimate examination it is pertinent to gain a good rapport with your patient, maintain good communication and ensure the patient’s dignity at all times. Remember to offer a chaperone for this skill. For the purposes of your exam, you will most likely be examining a mannequin.

## Procedure Steps

### Step 01

Firstly, wash your hands. Introduce yourself to the patient, explain to them what the examination will entail and gain his consent.

### Step 02

The patient should be exposed from the waist down, and with his shirt pulled up.  Testicular examination is best performed with the patient stood up. You should kneel down in front of and to the side of the patient, NOT directly in front of the patient.

### Step 03

As always you should start with a thorough inspection of the area. Firstly, inspect the penis on all sides and note any obvious lumps, swellings, ulcers or scars.

### Step 04

After inspecting the penis you should inspect the scrotum.  This is easier to do if you move the penis out of the way; you could ask the patient to do this for you.  Again inspect all sides of the scrotum and not forgetting to lift it so that you can inspect the posterior aspect. Again noting any obvious lumps, swellings, ulcers or scars.

### Step 05

You should now individually palpate each testicle. If you have identified or been told about any abnormalities, examine the “normal” side first. This is a bimanual technique using the thumb and index finger of each hand. Using both hands gently feel the whole testicle noting any palpable abnormalities.  Ensure you ask the patient if he experiences any pain whilst doing this.

Note that if you cannot palpate one of the testicles then you should feel along the course of the [inguinal ligament](https://en.wikipedia.org/wiki/Inguinal_ligament) for an undescended testicle. Alternatively, the patient may have had it removed surgically.

### Step 06

You should also palpate the [epididymis](https://en.wikipedia.org/wiki/Epididymis) and [spermatic cord](https://en.wikipedia.org/wiki/Spermatic_cord) on both sides.

### Step 07

If you do feel any abnormalities then there are certain questions you should ask yourself:

* Can I get above the lump?
* Is the testicle separate from the lump?
* Does it have a cough impulse?
* Does the lump [transilluminate](https://en.wikipedia.org/wiki/Transillumination)?

What does the lump feel like? (hard, soft, craggy, regular/irregular, like a “bag of worms”, size, etc?)

### Step 08

When you have finished your examination, cover the patient and thank them. Dispose of your gloves in the clinical waste bin and wash your hands.

### Step 09

You should now present your findings to the examiner. They may ask you for a differential diagnosis and this will be based upon your findings.