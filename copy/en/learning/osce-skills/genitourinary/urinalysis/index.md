```metadata
{
  "title":"Urinalysis (UA)",
  "description":"Urinalysis (or dipstick analysis or UA) is where the urine of a patient is tested using urinalysis testing strips. It is performed for various reasons e.g. patients with urinary symptoms, abdominal pain and at times during routine checks such as in pregnancy.",
  "last_updated":"2018-05-26"
}
```
# Urinalysis (UA)

## Foreword

Urinalysis (or dipstick analysis or UA) is where the urine of a patient is tested using [urinalysis testing strips](https://en.wikipedia.org/wiki/Urine_test_strip). It is performed for various reasons e.g. patients with urinary symptoms, abdominal pain and at times during routine checks such as in pregnancy.

This station demonstrates urinalysis using standard UK [urine test strips](http://en.wikipedia.org/wiki/Urine_test_strips), in which the results can be read as colour changes.

## Procedure Steps

### Step 01

For this station you will need:

* Gloves.
* Apron.
* Hand sanitiser.
* Urine dipsticks.
* Urine sample.

### Step 02

Start by washing your hands, put on your gloves and apron.

![Wash your hands](../../../../../../media/learning/osce-skills/img/wash-your-hands-female.jpg)

### Step 03

Check you have the correct patient’s details on the sample bottle (name and DOB).

### Step 04

Check that the urine sample has been collected within the last 4 hours.

### Step 05

Comment on the colour and clarity of the sample.  Upon removing the cap also comment if there is any odour.

### Step 06

Move onto the urine dipsticks.

Ensure they are in date.

### Step 07

Remove the cap and remove a strip without touching any of the test areas.

Replace the cap.

### Step 08

Immerse the strip fully in the urine, ensuring all the test areas are covered. Tap off any residual urine against the side of the urine sample bottle.

### Step 09

Hold the strip horizontally to prevent any of the testing area’s running into each other, and against the dipstick analysis on the side of the dipstick tub.

### Step 10

Read the strip after the correct amount of time, for example: 30 seconds for glucose, 60 seconds for protein.

### Step 11

Discard the strip, your gloves and apron in the clinical waste bin. Wash your hands.

### Step 12

Record/report any findings.

### Step 13

An extension to this station could be what “to do next”.  Should there be any positive results you may want to send the sample for analysis, if you do send the sample for analysis then note whether it is a midstream specimen of urine (MSU) or catheter specimens of urine (CSU).  You may want also want to arrange further tests, for example: if positive for glucose consider further investigations for diabetes.