```metadata
{
  "title":"Bimanual Vaginal (PV) Examination",
  "description":"Bimanual or PV examinations are performed for a number of clinical reasons e.g. problems relating to menstruation, irregular bleeding, dyspareunia, abnormal vaginal discharge or pelvic pain",
  "last_updated":"2018-05-26"
}
```
# Bimanual Vaginal (PV) Examination

## Foreword

Bimanual or *PV* examinations are performed for a number of clinical reasons, for example with problems relating to menstruation, irregular bleeding, [dyspareunia](http://en.wikipedia.org/wiki/Dyspareunia), abnormal [vaginal discharge](https://en.wikipedia.org/wiki/Vaginal_discharge) or pelvic pain.

For the purpose of examinations you will be provided with a mannequin, however you should pretend it is a real patient and talk to it as such as this will form part of the marking scheme.

## Procedure Steps

### Step 01

Introduce yourself to the patient and clarify her identity.  Explain what you would like to do and obtain consent.

Explain she should feel little, if any, discomfort and that the examination should be over fairly quickly.

**A chaperone is required for this examination**

### Step 02

The patient should be exposed from the waist down. Ask her to lie on her back, ankles together and to let her knees fall apart as much as possible.  You should try and remain some of her modesty by putting a cover over her.

![Patient exposed from the waist down](../../../../../../media/learning/osce-skills/img/patient-exposed-from-the-waist-down.jpg)

### Step 03

Wash your hands, put on some gloves and inspect the outside of the vagina. Check the labia and clitoris looking for any obvious abnormalities such as erosions.

![Inspect the outside of the vagina](../../../../../../media/learning/osce-skills/img/inspect-the-outside-of-the-vagina.jpg)

### Step 04

Lubricate the index and middle finger of your right hand. Explain to the patient that you are about to start the procedure.

![Lubricate the index and middle finger](../../../../../../media/learning/osce-skills/img/lubricate-the-index-and-middle-finger.jpg)

### Step 05

Use the thumb and index finger of your left hand to separate the [labia majora](https://en.wikipedia.org/wiki/Labia_majora) and firstly insert your index finger, checking for any [cervical excitation](https://en.wikipedia.org/wiki/Cervical_motion_tenderness). If none is present, then insert your middle finger.

![Separate the labia majora and firstly insert your index finger](../../../../../../media/learning/osce-skills/img/separate-the-labia-minora-and-firstly-insert-your-index-finger.jpg)

### Step 06

Palpate all of the vaginal walls as you advance your fingers feeling for any obvious abnormalities.

### Step 07

Using your fingertips, palpate the [cervix](https://en.wikipedia.org/wiki/Cervix), feel for its size, shape and mobility – check with the patient if it is tender.

### Step 08

At this point palpate the [uterus](https://en.wikipedia.org/wiki/Uterus) by pressing it between your right middle and index fingers and your left hand placed on the lower abdomen. Feel for any masses

![Palpate the patients uterus](../../../../../../media/learning/osce-skills/img/palpate-the-patients-uterus.jpg)

### Step 09

You should also try to palpate each of the [ovaries](https://en.wikipedia.org/wiki/Ovary). This is done by placing your internal fingers in the right [fornix](https://en.wikipedia.org/wiki/Vaginal_fornix) and trying to press the ovary between them and your left hand placed in the right iliac fossa.

Do the same for the left ovary.

Note any tenderness or masses which you may feel.

![Palpate the patients right ovary](../../../../../../media/learning/osce-skills/img/palpate-the-patients-right-ovary.jpg)

![Palpate the left ovary](../../../../../../media/learning/osce-skills/img/palpate-the-left-ovary.jpg)

### Step 10

Once complete, remove your fingers, check your glove for any discharge or blood, and then discard your gloves in the clinical waste bin.

![Check your glove for any discharge or blood](../../../../../../media/learning/osce-skills/img/check-your-glove-for-any-discharge-or-blood.jpg)

### Step 11

Finish by offering the patient a tissue, cover them up and thank them. Report your findings to the examiner.