```metadata
{
  "title":"Suicide Risk Assessment",
  "description":"Patients with mental health problems may attempt suicide, this can be in various ways although most commonly involves overdose of medication or cutting. Attempted suicide is also known as self harm",
  "last_updated":"2018-05-26"
}
```
# Suicide Risk Assessment

## Foreword

Patients with mental health problems may attempt suicide, this can be in various ways although most commonly involves overdose of medication or cutting.  Attempted suicide is also known as self harm.

Over 6000 patients in the UK annually succeed in ending their own life (statistics from January 2013), however many more attempt to do so.

In this station you wish to assess the patient’s likelihood to attempt suicide, particularly if they have recently done so.  It is a psychiatric history station and in order for the patient to open up and be honest with you, you must gain their trust and establish a good rapport.

The assessment has **6** main components:

1. The history of the current episode of self harm.
2. Assess risk factors for suicide.
3. Assess the patients mood.
4. Will the patient be returning to the same situation?
5. What does the patient think about the future?
6. Ask about current suicide thoughts.

## Procedure Steps

### Step 01

Introduce yourself to the patient, clarify their identity and explain that you wish to talk to them about their recent attempt to harm themselves.

### Step 02 - The history of the current episode of self harm.

* What precipitated the attempt?
* Was it planned?
* What method did they use?
* Was a suicide note left?
* Was the patient intoxicated (drugs/alcohol)?
* Was the patient alone?
* Were there any precautions against discovery (e.g. waited until house empty)?
* Did the patient seek help after the attempt or were they found and brought in by someone else?
* How does the patient feel about the episode now? (regret? do they wish that they had succeeded?)

### Step 03 - Assess risk factors for suicide.

* Are they **male**?
* Is their age **greater than 45 years**?
* Are they **unemployed**?
* Are they **divorced, widowed or single**?
* Do they have a **physical illness**?
* Do they have a **psychiatric illness**?
* Do they have a history of **substance misuse**?
* Have they had **previous suicide attempts**?
* Do they have a **family history of depression, substance misuse or suicide**?


### Step 04

Assess the patients mood. Particulary note if they are depressed or angry.

### Step 05

Assess whether the patient will be returning to the same situation, such as issues at home?

### Step 06

Question what the patient thinks about the future?

### Step 07

Ask the patient about their current suicide thoughts.

### Step 08

Thank the patient for speaking to you.

## Conclusion

You should summarise your findings to the examiner stating the patients suicide risk. You should also suggest what to do next, for example: hospitalisation, outpatient follow-up, or GP follow-up.







