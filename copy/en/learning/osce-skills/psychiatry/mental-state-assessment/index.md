```metadata
{
  "title":"Mental State Assessment",
  "description":"Mental state examination is basically the psychiatric `physical examination` i.e. a patient with breathing problems needs a respiratory examination, in the same way; a psychiatric patient needs a mental state examination",
  "last_updated":"2018-05-26"
}
```
# Mental State Assessment

## Foreword

The mental state examination is basically the psychiatric “physical examination”, for example: a patient with breathing problems needs a respiratory examination, in the same way that a psychiatric patient requires a mental state examination.

The station involves assessing how the patient appears and/or behaves and any abnormal thoughts or beliefs they may hold. It does *not* involve a cognitive (memory) assessment.

It is a psychiatric history/physical station and in order for the patient to open up and be honest with you, you must gain their trust and establish a good rapport.

## Procedure Steps

### Step 01

Begin by introducing yourself to the patient, clarify their identity and explain that you would like to talk to them about their thoughts.

### Step 02

There are 8 components to this assessment.

To assess appearance, behaviour and speech begin by asking some general open questions.

#### Appearance and behaviour

* **Appearance** – dress, posture, facial expression, mannerisms.
* **Activity** – sitting still/fidgeting.
* **Social and emotional behaviour** – apathy, irritable, co-operative.

#### Speech

* **Rate**
* **Tone**
* **Quality**
* **Form** – thought blocking, loosening of associations, flight of ideas, [neologisms](http://en.wikipedia.org/wiki/Neologism).
* **Content** – depressive ideas, delusions.

Now ask more specific questions relating to the other parts of the assessment:

#### Mood

* **Symptoms of anxiety** – e.g. sweating, palpitations.
* **Current mood state** – both subjective and objective.
* **Any biological symptoms** – e.g. sleep, appetite, libido, or loss of concentration.
* **Suicidal ideation.**

#### Phobias and obsessions

* **Phobias** – determine the stimulus, its psychological and physiological effect and the nature of any avoidance behaviour.
* **Obsessions** – determine the underlying thoughts, the nature of the obsession, the effect on daily life and if it’s a senseless obsession?

#### Abnormal experiences

* **Illusions or misperceptions**
* **Hallucinations** – visual, ophthalmic, auditory (second or third person).

#### Abnormal beliefs

* **Delusions**

#### Insight

To determine this you should ask a few directed questions:

> What do you think is wrong with you?

> Do you think you need any treatment?

> What do you think the treatment will do for you?

#### Cognition

This should be done by the mini mental state examination, although this is generally not part of the OSCE.

### Step 03

Thank the patient for speaking to you.

Summarise your findings for the examiner, offering a differential diagnosis.